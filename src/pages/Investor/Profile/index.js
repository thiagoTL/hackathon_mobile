import React, { useState } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  Switch,
  ScrollView,
  Platform,
} from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/AntDesign';
import IconMa from 'react-native-vector-icons/MaterialIcons';
import IconSa from 'react-native-vector-icons/SimpleLineIcons';
import { isIphoneX } from 'react-native-iphone-x-helper';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import profileImage from '../../../assets/images/profileImage.png';
import { colors } from '../../../constants/colors';
import { signInOut } from '../../../store/modules/auth/actions';

Icon.loadFont();
IconMa.loadFont();
IconSa.loadFont();

export default function Profile() {
  const [isEnabled, setIsEnaled] = useState(false);
  const toggleSwitch = () => setIsEnaled((previousState) => !previousState);

  const navigation = useNavigation();
  const dispatch = useDispatch();
  const userAuth = useSelector((state) => state.auth.user);

  function handleLogout() {
    dispatch(signInOut());
  }

  function handleEditProfile() {
    navigation.navigate('EditProfile');
  }

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.titleAccount}>ACCOUNT</Text>
          <TouchableOpacity
            style={styles.btnProfileAbout}
            activeOpacity={0.6}
            onPress={handleEditProfile}>
            <Text style={styles.textProfile}>Profile</Text>
            <View style={styles.viewProfileType}>
              <Text>EDIT</Text>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>

          <Text style={[styles.titleAbout, { marginTop: 5 }]}>APP</Text>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Language</Text>
            <View style={styles.viewProfileType}>
              <Text>ENG</Text>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Currency</Text>
            <View style={styles.viewProfileType}>
              <Text>USD</Text>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Privacy Policy</Text>
            <View style={styles.viewProfileType}>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Terms of use</Text>
            <View style={styles.viewProfileType}>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>

          <Text style={styles.titleApp}>ABOUT</Text>
          <View style={styles.btnProfileAbout}>
            <Text style={styles.textProfile}>Touch ID</Text>
            <Switch
              trackColor={{ false: colors.yellowDark, true: colors.yellowDark }}
              thumbColor={isEnabled ? '#fff' : '#222'}
              ios_backgroundColor={colors.yellowDark}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Support</Text>
            <View style={styles.viewProfileType}>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>Report a Bug</Text>
            <View style={styles.viewProfileType}>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={styles.btnProfileAbout} activeOpacity={0.6}>
            <Text style={styles.textProfile}>App Version 1.0</Text>
            <View style={styles.viewProfileType}>
              <IconMa
                name="chevron-right"
                color="#000"
                size={20}
                style={styles.iconProfileArrow}
              />
            </View>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.btnLogout}
            activeOpacity={0.6}
            onPress={handleLogout}>
            <IconSa name="logout" color="red" size={20} />
            <Text style={styles.btnLogoutText}> Logout </Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </View>
  );
}
