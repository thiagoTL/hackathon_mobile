import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.grayPrimary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    height: Platform.OS === 'ios' ? scale(550) : scale(560),
    width: scale(315),
    borderRadius: 30,
    backgroundColor: colors.white,
    shadowColor: colors.gray,
    shadowOffset: {
      height: Platform.OS === 'ios' ? 4 : 8,
      width: Platform.OS === 'ios' ? 4 : 8,
    },
    shadowOpacity: Platform.OS === 'ios' ? 4 : 7,
    shadowRadius: Platform.OS === 'ios' ? 4 : 7,
    elevation: Platform.OS === 'ios' ? 0 : 7,

    marginTop: scale(10),
  },
  header: {
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnEdit: {
    height: Platform.OS === 'ios' ? scale(30) : 40,
    width: Platform.OS === 'ios' ? scale(30) : 40,
    borderRadius: 30,
    backgroundColor: colors.yellowDark,
    justifyContent: 'center',
    alignItems: 'center',
    shadowColor: colors.black,
    shadowOffset: {
      height: Platform.OS === 'ios' ? 2 : 6,
      width: Platform.OS === 'ios' ? 2 : 6,
    },
    shadowOpacity: Platform.OS === 'ios' ? 10 : 8,
    shadowRadius: Platform.OS === 'ios' ? 5 : 8,
    elevation: Platform.OS === 'ios' ? 0 : 10,
    marginTop: Platform.OS === 'ios' ? scale(34) : 28,
    marginLeft: scale(250),
  },
  viewProfileImage: {
    height: scale(90),
    width: scale(90),
    borderRadius: 50,
    backgroundColor: '#000',
    alignSelf: 'center',
    justifyContent: 'center',
    // position: 'absolute',
    // top: 20,
    bottom: scale(50),
    borderWidth: 3,
    borderColor: colors.white,
    shadowColor: colors.black,
    shadowOffset: {
      height: Platform.OS === 'ios' ? 2 : 10,
      width: Platform.OS === 'ios' ? 2 : 10,
    },
    shadowOpacity: Platform.OS === 'ios' ? 2 : 10,
    shadowRadius: Platform.OS === 'ios' ? 2 : 15,
    elevation: Platform.OS === 'ios' ? 0 : 10,
  },
  profileContent: {
    flexDirection: 'column',
    position: 'absolute',
    top: 0,
    marginBottom: scale(100),
  },
  profileText: {
    fontSize: scale(16),
    color: colors.black,
    fontWeight: 'bold',
    bottom: Platform.OS === 'ios' ? scale(48) : 50,
  },
  imageProfile: {
    height: scale(85),
    width: scale(85),
    borderRadius: 50,
  },
  titleAbout: {
    fontSize: scale(14),
    fontWeight: 'bold',
    marginLeft: scale(10),
    color: '#ccc',
  },
  titleAccount: {
    fontSize: scale(14),
    fontWeight: 'bold',
    marginLeft: scale(10),
    color: '#ccc',
    marginTop: 10,
  },
  btnProfileAbout: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginTop: scale(15),
    width: scale(300),
    alignSelf: 'center',
    paddingBottom: scale(10),
  },
  textProfile: {
    fontSize: scale(16),
    fontWeight: '700',
  },
  viewProfileType: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconProfileArrow: {
    marginLeft: scale(3),
  },
  titleApp: {
    fontSize: scale(14),
    fontWeight: 'bold',
    marginLeft: scale(10),
    color: '#ccc',
    marginTop: scale(10),
  },
  btnLogout: {
    marginTop: scale(20),
    marginLeft: scale(10),
    flexDirection: 'row',
    // ...ifIphoneX(
    //   {
    //     marginBottom: 0,
    //   },
    //   {
    //     marginBottom: 20,
    //   },
    // ),
    marginBottom: scale(20),
  },
  btnLogoutText: {
    color: 'red',
    fontSize: scale(14),
    fontWeight: 'bold',
    marginLeft: scale(5),
  },
});

export default styles;
