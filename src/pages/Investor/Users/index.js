import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  FlatList,
  ActivityIndicator,
  SafeAreaView,
  TextInput,
  Image,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import styles from './styles';
import avatarDefault from '../../../assets/images/imageDefault.jpg';
import { UserListRequest } from '../../../store/modules/user/actions';
import { colors } from '../../../constants/colors';
import DashboardStar from '../components/Stars/DashboardStar';

// bagdes
import sms from '../../../assets/images/bagdes/smsb.png';
import idImage from '../../../assets/images/bagdes/id.png';
import money from '../../../assets/images/bagdes/05mm.png';
import ba from '../../../assets/images/bagdes/ba.png';
import mba from '../../../assets/images/bagdes/mba.png';
import cfa from '../../../assets/images/bagdes/cfa.png';
import linkedin from '../../../assets/images/bagdes/linkedinBad.png';

Icon.loadFont();

export default function Users() {
  const dataBages = [
    { id: 1, name: 'SMS', badges: sms },
    { id: 2, name: 'ID', badges: idImage },
    { id: 3, name: '5MM', badges: money },
    { id: 4, name: 'B.A', badges: ba },
    { id: 5, name: 'MBA', badges: mba },
    { id: 6, name: 'CFA', badges: cfa },
    { id: 7, name: 'Linkedin', badges: linkedin },
  ];

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const userList = useSelector((state) => state.user.userList);
  const loading = useSelector((state) => state.user.loading);

  useEffect(() => {
    userList.map((item) => console.log('ITEM', item.user.firstName));

    dispatch(UserListRequest());
  }, [dispatch]);

  function handleProfileComplete(id) {
    navigation.navigate('ProfileComplete', { id });
  }

  return (
    <SafeAreaView style={styles.container}>
      {!loading ? (
        <>
          {userList.length === 0 ? (
            <View />
          ) : (
            <>
              <View style={styles.viewInputSearch}>
                <Icon name="ios-search" size={25} color={colors.black} />
                <TextInput style={styles.inputSearch} placeholder="Pesquisa" />
              </View>
              <FlatList
                data={userList}
                keyExtractor={(item) => String(item._id)}
                renderItem={({ item }) => (
                  <View style={styles.viewContentList} key={item._id}>
                    <View style={styles.viewContentHeader}>
                      <View style={styles.viewImage}>
                        <Image
                          style={styles.imageAvatar}
                          source={{ uri: item.user.url_image }}
                        />
                      </View>
                      <View style={styles.viewContentData}>
                        <DashboardStar
                          rating={item.avaliacao.rating}
                          size={20}
                        />
                        <Text style={styles.textNameContent}>
                          {item.user.firstName + ' ' + item.user.lastName}
                        </Text>
                        <Text style={styles.profissionalType}>
                          {item.corretora}
                        </Text>
                      </View>
                    </View>
                    <View style={styles.viewBadges}>
                      {dataBages.map((dataBadges) => (
                        <Image
                          key={dataBadges.id}
                          style={styles.imageBadges}
                          source={dataBadges.badges}
                        />
                      ))}
                    </View>
                    <TouchableOpacity
                      style={styles.buttonViewProfile}
                      onPress={() => handleProfileComplete(item.user._id)}
                      activeOpacity={0.8}>
                      <Text style={styles.buttonTextProfile}>
                        View Full Profile
                      </Text>
                    </TouchableOpacity>
                  </View>
                )}
              />
            </>
          )}
        </>
      ) : (
        <View style={styles.loadingIndicator}>
          <ActivityIndicator size="large" color={colors.black} />
        </View>
      )}
    </SafeAreaView>
  );
}
