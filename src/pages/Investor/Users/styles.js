import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.grayPrimary,
  },
  viewInputSearch: {
    height: scale(35),
    width: scale(315),
    backgroundColor: '#eee',
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: scale(8),
    marginBottom: scale(10),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: scale(8),
  },
  inputSearch: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
  },
  viewContentList: {
    height: scale(200),
    borderRadius: 25,
    width: scale(325),
    backgroundColor: colors.white,
    marginBottom: scale(20),
    alignSelf: 'center',
  },
  viewContentHeader: {
    height: scale(80),
    width: scale(315),
    backgroundColor: colors.black,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    alignSelf: 'center',
    marginTop: scale(10),
    flexDirection: 'row',
  },
  viewImage: {
    borderRadius: 25,
    borderWidth: 2,
    borderColor: colors.white,
    height: scale(70),
    width: scale(70),
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: scale(10),
    marginLeft: scale(10),
  },
  imageAvatar: {
    height: scale(60),
    width: scale(60),
    borderRadius: 25,
  },
  viewContentData: {
    flexDirection: 'column',
  },
  textNameContent: {
    fontSize: scale(10),
    color: colors.white,
    fontWeight: 'bold',
    marginTop: scale(4),
    marginLeft: scale(8),
  },
  profissionalType: {
    fontSize: scale(10),
    color: colors.white,
    marginLeft: scale(8),
    fontWeight: 'bold',
  },
  cityTextContent: {
    fontSize: scale(10),
    fontWeight: 'bold',
    color: colors.grayPrimary,
    marginTop: scale(8),
    marginLeft: scale(8),
  },
  Stars: {
    height: scale(25),
    width: scale(25),
    backgroundColor: '#ccc',
    marginLeft: scale(10),
    marginTop: scale(8),
    borderRadius: 12,
  },
  viewStart: {
    flexDirection: 'row',
    marginTop: scale(5),
  },
  viewBadges: {
    flexDirection: 'row',
    marginTop: scale(15),
    marginLeft: scale(10),
  },
  imageBadges: {
    marginLeft: 7,
    height: scale(35),
    width: scale(35),
  },
  buttonViewProfile: {
    height: scale(36),
    width: scale(310),
    marginTop: scale(13),
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: colors.black,
  },
  buttonTextProfile: {
    color: colors.white,
    fontSize: scale(11),
    fontWeight: 'bold',
  },
  loadingIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
