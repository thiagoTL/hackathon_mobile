import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.grayPrimary,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    // ...ifIphoneX(
    //   {
    //     height: hp('68%'),
    //   },
    //   {
    //     height: Platform.OS === 'ios' ? hp('80%') : hp('75%'),
    //   },
    // ),

    height: scale(600),
    width: scale(315),
    borderRadius: 30,
    backgroundColor: colors.white,
    shadowColor: colors.gray,
    shadowOffset: {
      height: Platform.OS === 'ios' ? 4 : 8,
      width: Platform.OS === 'ios' ? 4 : 8,
    },
    shadowOpacity: Platform.OS === 'ios' ? 4 : 7,
    shadowRadius: Platform.OS === 'ios' ? 4 : 7,
    elevation: Platform.OS === 'ios' ? 0 : 7,
    // ...ifIphoneX(
    //   {
    //     marginTop: 60,
    //   },
    //   {
    //     marginTop: 30,
    //   },
    // ),
    marginTop: scale(20),
  },
  contentLinear: {
    flex: 1,
    borderRadius: 30,
  },
  title: {
    alignSelf: 'center',
    fontSize: scale(14),
    fontWeight: '600',
    marginTop: scale(10),
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnEditPhoto: {
    height: scale(30),
    width: scale(30),
    borderRadius: 20,
    shadowColor: '#222',
    shadowOffset: {
      height: 2,
      width: 2,
    },
    shadowOpacity: 1,
    shadowRadius: 2,
  },
  viewUploadPhoto: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginRight: scale(10),
    marginLeft: scale(10),
  },
  labelPhoto: {
    fontSize: scale(10),
    color: colors.grayPrimary,
    fontWeight: 'bold',
    marginLeft: scale(10),
    marginTop: scale(20),
    marginBottom: scale(5),
  },
  viewImage: {
    height: scale(60),
    width: scale(60),
    borderRadius: 30,
    backgroundColor: '#000',
    borderWidth: 1,
    borderColor: colors.white,
    shadowColor: '#ccc',
    shadowOffset: {
      height: 4,
      width: 4,
    },
    shadowOpacity: 4,
    shadowRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewEditTextInput: {
    flexDirection: 'row',
    height: scale(40),
    justifyContent: 'space-between',
    marginLeft: scale(10),
    marginRight: scale(10),
    borderBottomWidth: 1,
    borderBottomColor: colors.grayPrimary,
    marginTop: scale(10),
  },
  input: {
    flex: 1,
  },
  btnSave: {
    height: scale(40),
    width: '90%',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: scale(15),
    marginBottom: 15,
  },
  btnSaveText: {
    fontSize: scale(14),
    color: colors.white,
    fontWeight: 'bold',
  },
  btnGradientSave: {
    flex: 1,
    width: '100%',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imagePhotoUser: {
    height: scale(50),
    width: scale(50),
    borderRadius: 25,
  },
  btnBack: {
    height: 50,
    width: '90%',
    alignSelf: 'center',
    marginTop: 15,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 19,
  },
  btnBackText: {
    fontSize: scale(14),
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
