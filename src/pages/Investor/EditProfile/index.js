import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import IconAnt from 'react-native-vector-icons/AntDesign';
import LinearGradient from 'react-native-linear-gradient';
import { useSelector, useDispatch } from 'react-redux';
import { useNavigation } from '@react-navigation/native';
import { showMessage } from 'react-native-flash-message';
import { heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { scale } from 'react-native-size-matters';
import { launchImageLibrary } from 'react-native-image-picker';

import styles from './styles';
import { colors } from '../../../constants/colors';
import profileImage from '../../../assets/images/profileImage.png';
import { UserUpdateRequest } from '../../../store/modules/user/actions';
import api from '../../../services/api';

Icon.loadFont();
IconAnt.loadFont();

export default function EditProfile() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobile, setMobile] = useState('');
  const [imageUri, setImageUri] = useState('');
  const [file, setFile] = useState('');
  const [password, setPassword] = useState('');
  const [updatePassword, setUpdatePassword] = useState(false);
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmPassword] = useState('');

  // editables
  const [editable, setEditable] = useState(false);
  const [editableLastName, setEditableLastName] = useState(false);
  const [editableEmail, setEditableEmail] = useState(false);
  const [editableMobile, setEditableMobile] = useState(false);

  const userAuth = useSelector((state) => state.auth.user);
  const loadingEdit = useSelector((state) => state.user.loadingEdit);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const options = {
    title: 'Select Avatar edit',
    customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  useEffect(() => {
    setFirstName(userAuth.firstName);
    setLastName(userAuth.lastName);
    setEmail(userAuth.email);
    setMobile(userAuth.mobile);
    setImageUri(userAuth.filename);
  }, [userAuth]);

  async function handletakePictureSelf() {
    launchImageLibrary(options, (response) => {
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const sourceSelf = { uri: response.uri };

        setImageUri(sourceSelf.uri);
        setFile(sourceSelf);

        let prefix;
        let ext;

        if (response.fileName) {
          [prefix, ext] = response.fileName.split('.');
          ext = ext.toLocaleLowerCase() === 'heic' ? 'jpg' : ext;
        } else {
          prefix = new Date().getTime();
          ext = 'jpg';
        }

        const image = {
          uri: response.uri,
          type: response.type,
          name: `${prefix}.${ext}`,
        };
      }
    });
  }

  async function handleUpdateProfile() {
    // const [newPassword, setNewPassword] = useState('');
    // const [confirmNewPassword, setConfirmPassword] = useState('');

    if (newPassword !== confirmNewPassword) {
      showMessage({
        message: 'new passwords are not the same',
        type: 'warning',
      });
    }

    dispatch(
      UserUpdateRequest(
        imageUri,
        firstName,
        lastName,
        email,
        mobile,
        password,
        newPassword,
        navigation,
      ),
    );
  }

  console.log(
    'imageUri',
    `https://investexpert.sfo3.cdn.digitaloceanspaces.com/${imageUri}`,
  );

  return (
    <View style={styles.container}>
      <View
        style={[
          styles.content,
          { height: updatePassword ? scale(560) : scale(550) },
        ]}>
        <LinearGradient
          style={styles.contentLinear}
          colors={[
            'rgb(255,255,255)',
            'rgb(239, 242, 248)',
            'rgb(222, 228, 241)',
          ]}>
          <Text style={styles.title}>Account</Text>

          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.labelPhoto}>PERSONAL</Text>
            <View style={styles.viewUploadPhoto}>
              <View style={styles.viewImage}>
                {imageUri === '' ? (
                  <Image source={profileImage} style={styles.imagePhotoUser} />
                ) : (
                  <Image
                    source={{
                      uri: `https://investexpert.sfo3.cdn.digitaloceanspaces.com/${imageUri}`,
                    }}
                    style={styles.imagePhotoUser}
                  />
                )}
              </View>
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={handletakePictureSelf}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <Icon name="camera" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <View style={styles.viewEditTextInput}>
              <TextInput
                value={firstName}
                onChangeText={setFirstName}
                editable={editable}
                style={styles.input}
              />
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={() => setEditable(!editable)}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <IconAnt name="edit" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <View style={styles.viewEditTextInput}>
              <TextInput
                value={lastName}
                // defaultValue={}
                onChangeText={setLastName}
                editable={editableLastName}
                style={styles.input}
              />
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={() => setEditableLastName(!editableLastName)}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <IconAnt name="edit" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>

            <Text style={styles.labelPhoto}>CONTACT INFORMATION</Text>
            <View style={styles.viewEditTextInput}>
              <TextInput
                value={email}
                // defaultValue={}
                onChangeText={setEmail}
                editable={editableEmail}
                style={styles.input}
              />
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={() => setEditableEmail(!editableEmail)}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <IconAnt name="edit" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <View style={styles.viewEditTextInput}>
              <TextInput
                value={mobile}
                // defaultValue={}
                onChangeText={setMobile}
                editable={editableMobile}
                style={styles.input}
              />
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={() => setEditableMobile(!editableMobile)}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <IconAnt name="edit" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>

            <View style={styles.viewEditTextInput}>
              <TextInput
                value={password}
                placeholder="current password"
                placeholderTextColor={colors.black}
                // defaultValue={}
                onChangeText={setPassword}
                secureTextEntry={true}
                editable={true}
                style={styles.input}
              />
              <TouchableOpacity
                style={styles.btnEditPhoto}
                activeOpacity={0.7}
                onPress={() => setUpdatePassword(!updatePassword)}>
                <LinearGradient
                  colors={[colors.yellowT, colors.yellowDark]}
                  style={styles.btnGradient}>
                  <IconAnt name="edit" color={colors.white} size={15} />
                </LinearGradient>
              </TouchableOpacity>
            </View>

            {updatePassword && (
              <>
                <View style={styles.viewEditTextInput}>
                  <TextInput
                    value={newPassword}
                    // defaultValue={}
                    onChangeText={setNewPassword}
                    editable={true}
                    placeholder="New Password"
                    placeholderTextColor={colors.black}
                    style={styles.input}
                    secureTextEntry={true}
                  />
                  <TouchableOpacity
                    style={styles.btnEditPhoto}
                    activeOpacity={0.7}>
                    <LinearGradient
                      colors={[colors.yellowT, colors.yellowDark]}
                      style={styles.btnGradient}>
                      <IconAnt name="edit" color={colors.white} size={15} />
                    </LinearGradient>
                  </TouchableOpacity>
                </View>

                <View style={styles.viewEditTextInput}>
                  <TextInput
                    value={confirmNewPassword}
                    // defaultValue={}
                    placeholder="Confirm New Password"
                    placeholderTextColor={colors.black}
                    onChangeText={setConfirmPassword}
                    editable={true}
                    secureTextEntry={true}
                    style={styles.input}
                  />
                  <TouchableOpacity
                    style={styles.btnEditPhoto}
                    activeOpacity={0.7}>
                    <LinearGradient
                      colors={[colors.yellowT, colors.yellowDark]}
                      style={styles.btnGradient}>
                      <IconAnt name="edit" color={colors.white} size={15} />
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </>
            )}

            <TouchableOpacity
              style={styles.btnBack}
              onPress={() => navigation.goBack()}
              activeOpacity={0.7}>
              <Text style={styles.btnBackText}>Back</Text>
            </TouchableOpacity>

            <TouchableOpacity
              style={styles.btnSave}
              onPress={handleUpdateProfile}
              activeOpacity={0.7}>
              <LinearGradient
                colors={[colors.yellowT, colors.yellowDark]}
                style={styles.btnGradientSave}>
                {loadingEdit ? (
                  <ActivityIndicator size="small" color={colors.white} />
                ) : (
                  <Text style={styles.btnSaveText}>Save</Text>
                )}
              </LinearGradient>
            </TouchableOpacity>
          </ScrollView>
        </LinearGradient>
      </View>
    </View>
  );
}
