import { StyleSheet } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { scale } from 'react-native-size-matters';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#212121',
  },
  content: {
    // height: '100%',
    width: '100%',
    // flex: 1,
    padding: 15,
  },
  contentTitle: {
    fontSize: 26,
    fontWeight: '400',
    color: colors.white,
    lineHeight: 40,
  },
  contentFooter: {
    flexDirection: 'row',
    marginTop: 15,
    alignItems: 'center',
  },
  contentUserImage: {
    height: 40,
    width: 40,
    backgroundColor: colors.gray,
    borderRadius: 20,
  },
  contentText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: '400',
    marginLeft: 10,
  },
  contentTextDate: {
    fontSize: 14,
    color: colors.white,
    fontWeight: '400',
    marginLeft: 25,
  },
  imageArticle: {
    backgroundColor: colors.gray,
    height: 278,
    width: '100%',
    marginTop: 20,
    marginBottom: 20,
  },
  textBody: {
    fontSize: 18,
    color: '#DBDBDB',
    fontWeight: '400',
    textAlign: 'justify',
    margin: 15,
  },
  viewTags: {
    flexDirection: 'row',
    margin: 15,
  },
  textTags: {
    fontSize: 18,
    color: '#DBDBDB',
    fontWeight: '400',
    paddingLeft: 5,
  },
  viewLine: {
    borderBottomColor: '#979797',
    borderBottomWidth: 1,
    width: '95%',
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: 10,
  },
  viewRecommendationData: {
    flexDirection: 'row',
    marginLeft: scale(20),
    marginRight: scale(8),
    // marginTop: 20,
    marginBottom: scale(20),
    // marginTop: scale(8),
    alignItems: 'center',
  },
  imageViewRecommendation: {
    height: scale(60),
    width: scale(60),
    borderRadius: 30,
    marginTop: scale(20),
    marginRight: scale(10),
    backgroundColor: '#ccc',
  },
  dateRecommendation: {
    fontSize: 14,
    fontWeight: '500',
  },
  textRecommendationDescription: {
    // ...ifIphoneX({width: 280}, {width: 220}),
    fontSize: 14,
    width: scale(250),
  },
  viewTextRecommendation: {
    flexDirection: 'column',
  },
  textArticles: {
    fontSize: scale(18),
    fontWeight: '600',
    marginLeft: scale(20),
    color: colors.white,
    marginTop: 10,
  },
  imageArticles: {
    height: scale(80),
    width: scale(80),
    borderRadius: 1,
    marginTop: scale(20),
    marginRight: scale(10),
  },
  viewTextArticles: {
    flexDirection: 'column',
  },
  dateArticles: {
    fontSize: scale(12),
    fontWeight: '500',
  },
  textArticlesDescription: {
    ...ifIphoneX(
      {
        width: wp('60%'),
      },
      {
        width: wp('60%'),
      },
    ),
    fontSize: 14,
    textAlign: 'justify',
    color: colors.white,
  },
  titleArticle: {
    fontSize: scale(16),
    fontWeight: '600',
    marginTop: scale(10),
    color: colors.white,
    width: wp('60%'),
  },
  textReadMore: {
    fontSize: scale(14),
    color: colors.white,
    fontWeight: '600',
  },
});

export default styles;
