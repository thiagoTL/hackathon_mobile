import React, { useEffect } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ScrollView,
  FlatList,
} from 'react-native';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { format, compareAsc } from 'date-fns';

import styles from './styles';
import advisor from '../../../assets/images/advisior.png';
import {
  articleDetailsRequest,
  articleListRequest,
} from '../../../store/modules/articles/actions';
import Header from '../components/Header';

export default function ArticleDetails() {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();

  const articleDetails = useSelector((state) => state.articles.articleDetails);
  const loading = useSelector((state) => state.articles.loading);
  const articles = useSelector((state) => state.articles.articleList);

  const ids = route.params.ids;
  const id = route.params.id;

  useEffect(() => {
    dispatch(articleDetailsRequest(ids));
    dispatch(articleListRequest(id));
  }, [id, ids]);

  function handleViewArticle(itemId) {
    dispatch(articleDetailsRequest(itemId));
  }

  const name =
    articleDetails === null
      ? ' '
      : articleDetails.firstName + ' ' + articleDetails.lastName;

  const tag = articleDetails === null ? [] : articleDetails.tags;

  return (
    <View style={styles.container}>
      <Header
        title={name}
        iconLeft="left"
        iconRight=""
        onPress={() => navigation.goBack()}
      />
      <ScrollView>
        <View style={styles.content}>
          <Text style={styles.contentTitle}>
            Article Title: {articleDetails === null ? '' : articleDetails.title}
          </Text>
          <View style={styles.contentFooter}>
            <Image
              style={styles.contentUserImage}
              source={articleDetails === null ? '' : articleDetails.url_image}
            />
            <Text style={styles.contentText}>{name}</Text>
            <Text style={styles.contentTextDate}>
              {/* {format(new Date(2014, 1, 11), 'MMM, dd, yyyy')} */}
              {articleDetails === null
                ? ''
                : format(
                    new Date(articleDetails.createdAt),
                    'MMM, dd, yyyy',
                  )}{' '}
            </Text>
          </View>
        </View>
        <View style={styles.imageArticle} />
        {/* format(new Date(2014, 1, 11), 'MM/dd/yyyy') */}

        <Text style={styles.textBody}>
          {articleDetails === null ? '' : articleDetails.text}{' '}
        </Text>

        <View style={styles.viewTags}>
          <Text style={styles.textTags}> Tags: </Text>
          {tag.map((item) => (
            <Text key={item} style={styles.textTags}>
              {item},
            </Text>
          ))}
        </View>

        <View style={styles.viewLine} />
        <Text style={styles.textArticles}>ARTICLES - POSTS</Text>

        {articles.length > 0 ? (
          <FlatList
            data={articles}
            renderItem={({ item }) => (
              <View style={styles.viewRecommendationData}>
                <Image style={styles.imageArticles} source={advisor} />
                <View style={styles.viewTextArticles}>
                  {/* <DateFormated data={item.CreatedAt} /> */}
                  <Text style={styles.titleArticle}>{item.title}</Text>
                  {/* <Text style={styles.textArticlesDescription}>
                    {item.text}
                    <Text
                      style={styles.textReadMore}
                      onPress={() => handleViewArticle(item._id)}>
                      Read More
                    </Text> */}
                  <Text
                    numberOfLines={4}
                    style={styles.textArticlesDescription}>
                    {item.text.length < 80
                      ? item.text
                      : item.text.substring(0, 80)}
                    <Text
                      style={styles.textReadMore}
                      onPress={() => handleViewArticle(item._id)}>
                      Read More
                    </Text>
                  </Text>
                </View>
              </View>
            )}
          />
        ) : (
          <View />
        )}
      </ScrollView>
    </View>
  );
}
