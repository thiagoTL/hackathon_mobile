import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconsM from 'react-native-vector-icons/MaterialIcons';
import IconIo from 'react-native-vector-icons/Ionicons';
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';

import Home from './Home';
import Users from './Users';
import Chat from './Chat';
import Profile from './Profile';
import { colors } from '../../constants/colors';

const InvestorTab = createBottomTabNavigator();

IconF.loadFont();
IconsM.loadFont();
IconIo.loadFont();
IconMat.loadFont();

const icons = {
  DashboardGeral: {
    lib: IconF,
    name: 'home',
  },
  Users: {
    lib: IconF,
    name: 'users',
  },
  Chat: {
    lib: IconsM,
    name: 'chat-bubble',
  },
  Profile: {
    lib: IconF,
    name: 'user-circle',
  },
};

export default function InvestorBottom() {
  return (
    <InvestorTab.Navigator
      initialRouteName="DashboardGeral"
      screenOptions={({ route, navigation }) => ({
        tabBarIcon: ({ color, size, focused }) => {
          const { lib: Icon, name } = icons[route.name];
          return <Icon name={name} color={color} size={size} />;
        },
      })}
      tabBarOptions={{
        style: {
          backgroundColor: colors.black,
        },
        activeTintColor: colors.yellow,
        inactiveTintColor: colors.gray,
      }}>
      <InvestorTab.Screen
        name="DashboardGeral"
        component={Home}
        options={{
          title: 'Home',
        }}
      />
      <InvestorTab.Screen
        name="Users"
        component={Users}
        options={{
          title: 'Users',
        }}
      />
      <InvestorTab.Screen
        name="Chat"
        component={Chat}
        options={{
          title: 'Chat',
        }}
      />
      <InvestorTab.Screen
        name="Profile"
        component={Profile}
        options={{
          title: 'Profile',
        }}
      />
    </InvestorTab.Navigator>
  );
}
