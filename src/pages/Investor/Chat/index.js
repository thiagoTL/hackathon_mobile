import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableOpacity,
  SafeAreaView,
  Image,
  ActivityIndicator,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';

// import {
//   Container,
//   Title,
//   ListViewChat,
//   ChatView,
//   ImageProfileChat,
//   ProfileNameChat,
// } from './styles';
import styles from './styles';

// actions
import { ChatRoomListRequest } from '../../../store/modules/chatroom/actions';

export default function Chat() {
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const userAuth = useSelector((state) => state.auth.user);
  const chatRoom = useSelector((state) => state.chatroom.chatRoom);
  const loading = useSelector((state) => state.chatroom.loading);

  console.log('CHAAT ROOM', chatRoom);

  useEffect(() => {
    dispatch(ChatRoomListRequest(userAuth._id));
  }, []);

  function handleModalChat(itens) {
    navigation.navigate('ChatMessage', { itens });
  }

  console.log('LOADING  => ', loading);
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Chat</Text>
      {loading ? (
        <View style={styles.indicator}>
          <ActivityIndicator size="large" color="#000" />
        </View>
      ) : (
        <View style={styles.listViewChat}>
          {chatRoom.length > 0 ? (
            <ScrollView>
              {chatRoom.map((item) => (
                <TouchableOpacity
                  style={styles.chatView}
                  key={item}
                  onPress={() => handleModalChat(item)}>
                  <Image
                    style={styles.imageProfileChat}
                    source={{ uri: item.photoPrimary }}
                  />
                  <Text style={styles.profileNamChat}>{item.name}</Text>
                </TouchableOpacity>
              ))}
            </ScrollView>
          ) : (
            <View />
          )}
        </View>
      )}
    </SafeAreaView>
  );
}
