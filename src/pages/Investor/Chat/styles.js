import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.grayPrimary,
  },
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: scale(22),
    color: colors.white,
    fontWeight: 'bold',
    marginTop: scale(10),
    marginLeft: scale(10),
  },
  listViewChat: {
    height: hp('100%'),
    flex: 1,
    backgroundColor: colors.white,
    marginTop: scale(5),
  },
  chatView: {
    height: scale(50),
    width: wp('100%'),
    borderBottomWidth: 0.3,
    borderBottomColor: '#ccc',
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageProfileChat: {
    height: scale(30),
    width: scale(30),
    borderRadius: 15,
    backgroundColor: '#ccc',
    marginLeft: scale(10),
  },
  profileNamChat: {
    fontSize: scale(16),
    color: colors.black,
    fontWeight: 'bold',
    marginLeft: scale(10),
  },
});

export default styles;
