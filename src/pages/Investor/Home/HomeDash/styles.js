import {StyleSheet, Platform} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  headerContent: {
    height: scale(75),
    width: '100%',
    // flex: 1,
    // backgroundColor: colors.black,
  },
  headerTitle: {
    fontSize: scale(28),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: scale(25),
    marginLeft: scale(9),
  },
  ViewBorderLine: {
    borderBottomWidth: 1.5,
    borderBottomColor: colors.white,
    marginTop: scale(10),
  },
  textValue: {
    fontSize: scale(35),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: scale(15),
  },
  textValueMoeda: {
    fontSize: scale(12),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  valueDolar: {
    fontSize: scale(14),
    marginTop: scale(30),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  textDolar: {
    fontSize: scale(12),
    color: colors.white,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  viewList: {
    flex: 1,
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
  },

  viewContentList: {
    flexGrow: 1,
    marginTop: 10,
  },
  cartList: {
    margin: 10,
    width: scale(120),
    height: scale(120),
    shadowColor: '#ccc',
    shadowOffset: {height: 4, width: 4},
    backgroundColor: colors.white,
    shadowOpacity: 1,
    shadowRadius: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: Platform.OS === 'ios' ? null : 'hidden',
    elevation: Platform.OS === 'ios' ? 0 : 4,
  },
  buttonCard: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageCard: {
    height: scale(40),
    width: scale(40),
  },
  textCard: {
    fontSize: scale(12),
    color: colors.black,
    marginTop: 10,
  },
});

export default styles;
