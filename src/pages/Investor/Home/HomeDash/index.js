import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  Image,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useSelector, useDispatch} from 'react-redux';

import styles from './styles';
import gorila from '../../../../assets/images/gorila1.png';
import xp from '../../../../assets/images/xpinvestiments.png';
import bitcoin from '../../../../assets/images/bitcoin.png';
import invest from '../../../../assets/images/investiments.png';
import questTrade from '../../../../assets/images/questTrade.png';
import coinbase from '../../../../assets/images/coinbase.png';
import {modalClose} from '../../../../store/modules/codeVerification/actions';
import ModalWelcome from '../../components/ModalWelcome';
import Header from '../../components/Header';

export default function Home() {
  const data = [
    {id: 1, image: gorila, title: 'Gorila'},
    {id: 2, image: questTrade, title: 'Quest Trade'},
    {id: 3, image: coinbase, title: 'Coinbase'},
    {id: 4, image: invest, title: 'Manually Input'},
    {id: 5, image: xp, title: 'XP Investment'},
    {id: 6, image: bitcoin, title: 'MercadoBitcoin'},
    {id: 7, image: bitcoin, title: 'Ágora'},
    {id: 8, image: invest, title: 'Clear'},
  ];

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const modalWelcome = useSelector((state) => state.codeVerification.modal);

  function handleInvest() {
    navigation.navigate('Wallet');
  }

  return (
    <SafeAreaView style={styles.container}>
      {/* <View style={styles.headerContent}>
        <Text style={styles.headerTitle}>Build Your Portfolio</Text>
      </View> */}
      <Header title="Build Your Portfolio" iconLeft="" iconRight="" />

      <View style={styles.viewList}>
        <FlatList
          style={styles.viewContentList}
          showsVerticalScrollIndicator={false}
          data={data}
          numColumns={2}
          renderItem={({item}) => (
            <TouchableOpacity
              style={styles.cartList}
              activeOpacity={0.7}
              onPress={handleInvest}>
              <Image source={item.image} style={styles.imageCard} />
              <Text style={styles.textCard}>{item.title}</Text>
            </TouchableOpacity>
          )}
        />
      </View>

      <ModalWelcome
        visible={modalWelcome}
        onRequestClose={() => dispatch(modalClose())}
      />
    </SafeAreaView>
  );
}
