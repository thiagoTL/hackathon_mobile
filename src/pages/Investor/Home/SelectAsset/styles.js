import {StyleSheet, Platform} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  headerContent: {
    // ...ifIphoneX(
    //   {
    //     height: hp('10%'),
    //   },
    //   {
    //     height: hp('12%'),
    //   },
    // ),
    height: scale(75),
    width: '100%',
    // flex: 1,
    backgroundColor: colors.black,
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    marginLeft: scale(10),
    color: colors.yellowDark,
  },
  headerTitle: {
    fontSize: Platform.OS === 'ios' ? scale(23) : 25,
    color: colors.white,
    fontWeight: 'bold',
    // marginTop: 25,
    // ...ifIphoneX(
    //   {
    //     marginLeft: '16%',
    //   },
    //   {
    //     marginLeft: Platform.OS === 'ios' ? '10%' : '13%',
    //   },
    // ),
    marginLeft: scale(35),
    alignSelf: 'center',
  },
  viewExample: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: colors.white,
  },
  viewRadio: {
    flexDirection: 'row',
    marginTop: scale(30),
    alignSelf: 'center',
  },
  viewRadioContent: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  gradient: {
    borderRadius: 8,
    height: scale(30),
    width: scale(150),
    justifyContent: 'center',
    alignItems: 'center',
  },
  gradientText: {
    fontSize: scale(14),
    fontWeight: 'bold',
  },
  viewSell: {
    height: scale(30),
    width: scale(150),
    backgroundColor: colors.gray,
    borderTopRightRadius: 8,
    borderBottomRightRadius: 8,

    justifyContent: 'center',
    alignItems: 'center',
  },
  lineView: {
    borderTopColor: '#ccc',
    borderTopWidth: 1,
    width: '80%',
    marginTop: scale(15),
  },
  btnLogin: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLoginText: {
    fontSize: scale(14),
    color: colors.white,
    fontWeight: 'bold',
  },
  inputView: {
    height: scale(45),
    width: scale(140),
    borderWidth: 1,
    borderColor: '#ccc',
    paddingLeft: 5,
  },
  viewRowContent: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  inputAmount: {
    width: scale(315),
    height: scale(45),
    backgroundColor: 'rgba(33,33,33,0.8)',
    borderWidth: 2,
    borderColor: colors.black,
    borderRadius: 20,
    paddingLeft: 10,
  },
  viewDate: {
    flexDirection: 'column',
    marginLeft: scale(20),
    marginTop: scale(40),
  },
  viewText: {
    fontSize: scale(14),
    fontWeight: 'bold',
    marginBottom: 5,
  },
  viewContent: {
    flexDirection: 'column',
    marginLeft: scale(20),
    marginTop: scale(10),
  },
  textViewRow: {
    marginRight: 5,
  },
  viewSelect: {
    flexDirection: 'column',
    marginLeft: scale(20),
  },
  btnDone: {
    height: Platform.OS === 'ios' ? scale(45) : 48,
    width: scale(315),
    backgroundColor: colors.black,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: scale(50),
  },
});

export default styles;
