import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

export default function Render() {
  const item = {
    id: 1,
    valor: '$ 7,265.50',
    description: 'in USD * US dollar',
    valorMoeda: '$10,201.31',
    text: 'total in CAD equivalent',
    graph: false,
  };

  return (
    <View style={styles.viewData}>
      <Text style={styles.textValue}>{item.valor}</Text>
      <Text style={styles.textValueMoeda}>{item.description}</Text>
      <Text style={styles.valueDolar}>{item.valorMoeda}</Text>
      <Text style={styles.textDolar}>{item.text}</Text>
    </View>
  );
}
