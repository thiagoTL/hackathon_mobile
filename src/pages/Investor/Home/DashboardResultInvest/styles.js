import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../../constants/colors';

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    // backgroundColor: colors.black,
  },
  header: {
    // flex: 2,
    backgroundColor: colors.black,
    // ...ifIphoneX(
    //   {
    //     height: hp('35%'),
    //   },
    //   {height: Platform.OS === 'ios' ? hp('50%') : 360},
    // ),
    height: scale(400),
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  image: {
    width: scale(50),
    height: scale(50),
  },
  headerBit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: scale(10),
    marginLeft: scale(25),
    marginRight: scale(15),
  },
  textMoeda: {
    color: colors.white,
    fontSize: scale(30),
    fontWeight: 'bold',
    marginTop: scale(8),
  },
  textValue: {
    color: 'green',
    fontSize: scale(20),
    fontWeight: '600',
  },
  btnClose: {
    height: scale(25),
    width: scale(25),
    borderRadius: 13,
    backgroundColor: '#222',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    marginLeft: scale(5),
    marginTop: scale(3),
  },
  viewGroupBtn: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginLeft: scale(20),
    marginRight: scale(15),
  },
  btnBuy: {
    height: scale(25),
    width: scale(55),
    backgroundColor: colors.green,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  btnBuyText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  btnSell: {
    height: scale(25),
    width: scale(55),
    backgroundColor: colors.red,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
  },
  btnSellText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  btnPerc: {
    height: scale(25),
    width: scale(70),
    backgroundColor: colors.purple,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnPercText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  viewToday: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: scale(25),
  },
  viewDay: {
    height: scale(25),
    width: scale(35),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? scale(5) : 10,
  },
  viewDayText: {
    fontSize: scale(12),
    // marginLeft: 20,
    fontWeight: '500',
  },
  btnBuySell: {
    flexDirection: 'row',
  },
  btnAdd: {
    width: scale(32),
    height: scale(32),
    borderRadius: 12,
    shadowColor: '#ccc',
    shadowOffset: { height: 8, width: 8 },
    shadowOpacity: 8,
    shadowRadius: 8,
    elevation: Platform.OS === 'ios' ? 0 : 5,
    backgroundColor: colors.burntyellow,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewBtnAdd: {
    flexDirection: 'row',
    // justifyContent: 'center',
    alignItems: 'center',
    marginLeft: scale(15),
    marginTop: scale(15),
  },
  btnAddText: {
    fontSize: scale(14),
    fontWeight: '500',
    marginLeft: scale(10),
  },
  viewLineArticles: {
    borderBottomWidth: 0.4,
    width: '95%',
    alignSelf: 'center',
    marginTop: scale(14),
    borderBottomColor: colors.black,
  },
  viewArticleTitle: {
    fontSize: scale(14),
    marginTop: scale(10),
    marginLeft: scale(10),
    color: colors.black,
  },
  viewImage: {
    height: scale(80),
    width: scale(80),
    marginTop: scale(20),
    borderWidth: 1,
    borderColor: colors.black,
    backgroundColor: '#ccc',
  },
  viewArticle: {
    // flexDirection: 'row',
    // marginLeft: scale(10),
    // marginTop: scale(10),
    flexDirection: 'row',
    width: '70%',
    marginLeft: 15,
    marginRight: 20,
    height: 140,
    marginBottom: 20,
  },
  viewArticleText: {
    flexDirection: 'column',
    marginLeft: scale(10),
  },
  textArticleContent: {
    fontSize: 12,
    textAlign: 'justify',
    // ...ifIphoneX(
    //   {
    //     width: 260,
    //   },
    //   {
    //     width: Platform.OS === 'ios' ? 200 : 270,
    //   },
    // ),
    width: scale(230),
    marginTop: scale(1),
  },
  articleTitle: {
    fontSize: scale(15),
    fontWeight: 'bold',
    marginTop: scale(5),
    width: wp('60%'),
    marginBottom: 10,
  },
  textReadMore: {
    marginTop: 5,
    color: '#0A32FB',
    fontWeight: '400',
    // marginTop: 5,
    //                 color: '#0A32FB',
    //                 fontWeight: '400',
  },
  viewArrowUp: {
    height: scale(15),
    width: scale(15),
    backgroundColor: colors.white,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  viewIndicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewUndefined: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewUndefinedText: {
    fontSize: 14,
    color: colors.grayPrimary,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  viewReadMore: {
    alignItems: 'center',
  },
});

export default styles;
