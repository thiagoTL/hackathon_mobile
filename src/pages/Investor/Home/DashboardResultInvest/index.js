import React, { useEffect } from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconF from 'react-native-vector-icons/AntDesign';
import { useNavigation, useRoute } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import { format } from 'date-fns';
import { ScrollView } from 'react-native-gesture-handler';

import styles from './styles';
import { colors } from '../../../../constants/colors';
import { articleTagsRequest } from '../../../../store/modules/articles/actions';
import Graph from '../../components/Graph';

Icon.loadFont();
IconF.loadFont();

export default function DashboardResultInvest() {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const routes = useRoute();

  const nameTag = routes.params;

  const articless = useSelector((state) => state.articles.articleTag);
  const loading = useSelector((state) => state.articles.loading);

  useEffect(() => {
    dispatch(articleTagsRequest(nameTag));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nameTag]);

  function handleDashboardClose() {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={styles.conatiner}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.header}>
          <Graph onPressGoBack={() => console.log('OK')} />
        </View>
        <View style={styles.content}>
          <View style={styles.viewBtnAdd}>
            <TouchableOpacity
              activeOpacity={0.6}
              style={styles.btnAdd}
              onPress={() => navigation.navigate('')}>
              <Icon name="add" color={colors.white} size={20} />
            </TouchableOpacity>
            <Text style={styles.btnAddText}>Invite Advisor</Text>
          </View>
          <View style={styles.viewLineArticles} />
          <Text style={styles.viewArticleTitle}>ARTICLES</Text>
          {loading ? (
            <View style={styles.viewIndicator}>
              <ActivityIndicator size="large" color={colors.black} />
            </View>
          ) : (
            <>
              {articless === undefined ? (
                <View style={styles.viewUndefined}>
                  <Text style={styles.viewUndefinedText}>
                    There are no articles for this subject at the moment.
                  </Text>
                </View>
              ) : (
                <FlatList
                  data={articless}
                  keyExtractor={(item) => String(item._id)}
                  renderItem={({ item }) => (
                    <View style={styles.viewArticle}>
                      <View>
                        <Text>
                          {format(new Date(item.createdAt), 'yyyy-MM-dd')}
                        </Text>
                        <Text style={styles.articleTitle}>{item.title}</Text>
                        <Text>
                          {item.text.length < 160
                            ? item.text
                            : item.text.substring(0, 160)}
                        </Text>
                      </View>
                      <View style={styles.viewReadMore}>
                        <View style={styles.viewImage} />
                        <Text style={styles.textReadMore}>Read More</Text>
                      </View>
                    </View>
                  )}
                />
              )}
            </>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
