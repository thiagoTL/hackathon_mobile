import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  viewHeader: {
    height: scale(50),
    width: '100%',
    backgroundColor: colors.black,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    backgroundColor: '#fff',
    height: '90%',
    marginTop: scale(25),
    borderTopEndRadius: 30,
    borderTopLeftRadius: 30,
  },
  infoProfile: {
    // ...ifIphoneX(
    //   {
    //     height: hp('27'),
    //   },
    //   {height: Platform.OS === 'ios' ? hp('42%') : 225},
    // ),
    height: scale(240),
    width: scale(320),
    alignSelf: 'center',
    marginTop: scale(20),
    backgroundColor: colors.black,
    borderRadius: 20,
  },
  imageProfile: {
    // ...ifIphoneX({height: 90}, {height: 80}),
    // ...ifIphoneX({width: 90}, {width: 80}),
    height: scale(90),
    width: scale(90),
    borderWidth: 3,
    borderColor: colors.white,
    backgroundColor: '#faf',
    alignSelf: 'center',
    borderRadius: 22,
    position: 'absolute',
    // ...ifIphoneX({bottom: 125}, {bottom: Platform.OS === 'ios' ? 152 : 138}),
    bottom: scale(140),
    marginBottom: 25,
  },
  stars: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  viewInfoUser: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: scale(10),
  },
  textViewInfoUserAdvisor: {
    fontSize: scale(12),
    color: '#fff',
  },
  viewScore: {
    height: scale(15),
    width: scale(15),
    borderRadius: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: scale(10),
    marginLeft: scale(10),
  },
  score: {
    height: scale(5),
    width: scale(5),
    borderRadius: 10,
    backgroundColor: '#000',
  },
  textTypeProfession: {
    fontSize: scale(12),
    color: '#fff',
  },
  textExperience: {
    fontSize: scale(12),
    color: '#fff',
    alignSelf: 'center',
    marginTop: scale(2),
  },
  viewLine: {
    borderBottomWidth: 1,
    borderBottomColor: '#fff',
    width: scale(100),
    alignSelf: 'center',
    marginTop: scale(10),
  },
  textInfoDescription: {
    fontSize: scale(12),
    textAlign: 'justify',
    color: '#fff',
    marginTop: scale(5),
    marginLeft: scale(10),
    marginRight: scale(10),
  },
  textProfileCheck: {
    fontSize: scale(14),
    fontWeight: 'bold',
    marginTop: scale(10),
    marginLeft: scale(20),
  },
  btnChat: {
    height: scale(45),
    width: scale(315),
    backgroundColor: colors.yellowDark,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 30,
    marginTop: scale(15),
    shadowColor: '#ccc',
    shadowOffset: {
      height: 5,
      width: 5,
    },
    shadowOpacity: 8,
    shadowRadius: 3,
  },
  btnChatText: {
    fontSize: scale(14),
    color: colors.white,
  },
  btnAddInvestments: {
    width: scale(30),
    height: scale(25),
    // marginTop: 30,
    borderRadius: 5,
    marginLeft: scale(15),
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewRecommendation: {
    flexDirection: 'row',
    marginTop: scale(20),
    marginLeft: scale(15),
    alignItems: 'center',
  },
  textRecommendation: {
    fontSize: scale(18),
    fontWeight: '600',
  },
  viewRecommendationData: {
    flexDirection: 'row',
    marginLeft: scale(20),
    marginRight: scale(50),
    // marginTop: 20,
    marginBottom: scale(20),
    marginTop: scale(8),
  },
  imageViewRecommendation: {
    height: scale(60),
    width: scale(60),
    borderRadius: 30,
    marginTop: scale(20),
    marginRight: scale(10),
    backgroundColor: '#ccc',
  },
  dateRecommendation: {
    fontSize: 14,
    fontWeight: '500',
  },
  textRecommendationDescription: {
    // ...ifIphoneX({width: 280}, {width: 220}),
    fontSize: 14,
    width: scale(250),
  },
  viewTextRecommendation: {
    flexDirection: 'column',
  },
  textArticles: {
    fontSize: scale(18),
    fontWeight: '600',
    marginLeft: scale(20),
  },
  imageArticle: {
    height: scale(100),
    width: scale(100),
    borderRadius: 1,
    marginTop: scale(20),
    marginRight: scale(10),
  },
  viewTextArticles: {
    flexDirection: 'column',
  },
  dateArticles: {
    fontSize: scale(12),
    fontWeight: '500',
  },
  textArticlesDescription: {
    // ...ifIphoneX(
    //   {
    //     width: 240,
    //   },
    //   {
    //     width: 180,
    //   },
    // ),
    width: scale(200),
    fontSize: scale(14),
    textAlign: 'justify',
  },
  titleArticle: {
    fontSize: scale(16),
    fontWeight: '600',
    marginTop: scale(10),
  },
  textReadMore: {
    fontSize: scale(14),
    color: 'blue',
    fontWeight: '600',
  },
  viewLineRecommendations: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    width: '95%',
    alignSelf: 'center',
    // ...ifIphoneX({marginTop: 30}, {marginTop: 10}),
    marginTop: scale(30),
    marginBottom: 20,
  },
  imageBadges: {
    height: scale(40),
    width: scale(40),
    marginRight: scale(8),
  },
  viewExperience: {
    flexDirection: 'row',
    marginLeft: scale(17),
    marginTop: scale(8),
  },
  viewProfileCheck: {
    flexDirection: 'row',
    marginLeft: scale(17),
    marginTop: scale(8),
  },
  imageBadgesE: {
    height: scale(48),
    width: scale(35),
    marginRight: scale(8),
    marginTop: scale(5),
  },
  viewBadgesImages: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textDescBadges: {
    fontSize: scale(12),
    color: '#ccc',
    marginTop: scale(3),
  },
  header: {
    width: '100%',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  headerTitle: {
    color: '#fff',
    fontWeight: 'bold',
    marginRight: scale(30),
  },
  btnGoBack: {
    marginLeft: scale(10),
  },
});

export default styles;
