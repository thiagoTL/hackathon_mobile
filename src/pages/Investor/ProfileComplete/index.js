import React, { useEffect, useState, useMemo } from 'react';
import {
  View,
  Text,
  ScrollView,
  TouchableNativeFeedback,
  Animated,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';
import { useRoute, useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconAt from 'react-native-vector-icons/AntDesign';
import socketio from 'socket.io-client';

import avatarDefault from '../../../assets/images/imageDefault.jpg';
import advisor from '../../../assets/images/advisior.png';
import profile from '../../../assets/images/pessoa.png';
// bagdes
import sms from '../../../assets/images/bagdes/smsb.png';
import idImage from '../../../assets/images/bagdes/id.png';
import cincomm from '../../../assets/images/bagdes/05mm.png';
import ba from '../../../assets/images/bagdes/ba.png';
import mba from '../../../assets/images/bagdes/mba.png';
import cfa from '../../../assets/images/bagdes/cfa.png';
import canada from '../../../assets/images/bagdes/canada.png';
import brazil from '../../../assets/images/bagdes/brazil.png';
import linkedin from '../../../assets/images/bagdes/linkedinBad.png';

import styles from './styles';
import Star from '../components/Star';

//actions
import { UserListIdRequest } from '../../../store/modules/user/actions';
import { articleListRequest } from '../../../store/modules/articles/actions';
import { evaluationsListRequest } from '../../../store/modules/evaluations/actions';
import { ChatRoomRequest } from '../../../store/modules/chatroom/actions';

import ModalRecommendations from '../components/ModalRecommendations';
import ModalBadges from '../components/ModalBadges';
import DateFormated from '../components/DateFormated';
import { colors } from '../../../constants/colors';

const numStars = 5;

Icon.loadFont();
IconAt.loadFont();

export default function ProfileComplete() {
  const [visible, setVisible] = useState(false);
  const [modalBadges, setModalBadges] = useState(false);

  const navigation = useNavigation();
  const routes = useRoute();
  const dispatch = useDispatch();

  const profileName = useSelector((state) => state.user.name);
  const profileLastName = useSelector((state) => state.user.lastName);
  const ratingSt = useSelector((state) => state.user.stars);
  const urlImage = useSelector((state) => state.user.urlImage);
  const articles = useSelector((state) => state.articles.articleList);
  const evaluations = useSelector((state) => state.evaluations.evaluations);
  const userAuth = useSelector((state) => state.auth.user);

  let stars = [];
  const id = routes.params.id;

  useEffect(() => {
    dispatch(UserListIdRequest(id));
  }, [dispatch]);

  useEffect(() => {
    dispatch(articleListRequest(id));
    dispatch(evaluationsListRequest());
  }, [dispatch]);

  function handleOpenChatMessage() {
    const name = profileName + ' ' + profileLastName;
    const subName = userAuth.firstName + ' ' + userAuth.lastName;

    dispatch(
      ChatRoomRequest(
        name,
        subName,
        urlImage,
        userAuth.url_image,
        id,
        userAuth._id,
        navigation,
      ),
    );
    // navigation.navigate('ChatMessage');
  }

  function handleOpenModalRecommendation() {
    setVisible(true);
  }

  function handleGoBackUsers() {
    navigation.goBack();
  }

  function handleModalBadges() {
    setModalBadges(true);
  }

  for (let x = 1; x <= numStars; x++) {
    stars.push(
      <TouchableNativeFeedback key={x}>
        <Animated.View>
          <Star filled={x <= ratingSt ? true : false} />
        </Animated.View>
      </TouchableNativeFeedback>,
    );
  }

  function handleNavigateArticleDetails(ids) {
    navigation.navigate('ArticleDetails', { ids, id });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.viewHeader}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={handleGoBackUsers}
            style={styles.btnGoBack}>
            <IconAt name="left" size={25} color={colors.yellowDark} />
          </TouchableOpacity>
          <Text style={styles.headerTitle}>
            {profileName + ' ' + profileLastName}
          </Text>

          <View />
        </View>
      </View>
      <View style={{ flex: 1, backgroundColor: '#ccc' }}>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}>
          <View style={styles.content}>
            <View style={styles.infoProfile}>
              <Image style={styles.imageProfile} source={{ uri: urlImage }} />
              <View style={styles.stars}>{stars}</View>
              <View style={styles.viewInfoUser}>
                <Text style={styles.textViewInfoUserAdvisor}>Advisor</Text>
                <View style={styles.viewScore}>
                  <View style={styles.score} />
                </View>
                <Text style={styles.textTypeProfession}>Independent</Text>
              </View>
              <Text style={styles.textExperience}>20 years of Experience</Text>
              <View style={styles.viewLine} />
              <Text style={styles.textInfoDescription}>
                Managing your investments can be complicated. You may not be
                comfotable investing on your own. A professional financial
                advisor or planner can help. Choosing the right advisor depends
                on what help you need. If you need specialized (250 characters)
              </Text>
            </View>
            <Text style={styles.textProfileCheck}>Profile Check</Text>
            <View style={styles.viewProfileCheck}>
              <View style={styles.viewBadgesImages}>
                <Image style={styles.imageBadges} source={idImage} />
                <Text style={styles.textDescBadges}>ID</Text>
              </View>
              <View style={styles.viewBadgesImages}>
                <Image style={styles.imageBadges} source={sms} />
                <Text style={styles.textDescBadges}>SMS</Text>
              </View>
              <Image style={styles.imageBadgesE} source={canada} />
              <Image style={styles.imageBadgesE} source={brazil} />
              <Image style={styles.imageBadgesE} source={linkedin} />
            </View>
            <Text style={styles.textProfileCheck}>
              Experiencie & Academic Education
            </Text>
            <View style={styles.viewExperience}>
              <View style={styles.viewBadgesImages}>
                <Image style={styles.imageBadges} source={cincomm} />
                <Text style={styles.textDescBadges}> 5MM </Text>
              </View>
              <View style={styles.viewBadgesImages}>
                <Image style={styles.imageBadges} source={ba} />
                <Text style={styles.textDescBadges}> B.A. </Text>
              </View>
              <View style={styles.viewBadgesImages}>
                <Image style={styles.imageBadges} source={mba} />
                <Text style={styles.textDescBadges}>MBA </Text>
              </View>
              <TouchableOpacity
                style={styles.viewBadgesImages}
                onPress={handleModalBadges}
                activeOpacity={0.7}>
                <Image style={styles.imageBadges} source={cfa} />
                <Text style={styles.textDescBadges}>CFA </Text>
              </TouchableOpacity>
            </View>

            <TouchableOpacity
              style={styles.btnChat}
              activeOpacity={0.7}
              onPress={handleOpenChatMessage}>
              <Text style={styles.btnChatText}>CHAT WITH ME</Text>
            </TouchableOpacity>
            <View style={styles.viewRecommendation}>
              <Text style={styles.textRecommendation}>RECOMMENDATION</Text>
              <Text>{articles.evaluations}</Text>
              <TouchableOpacity
                style={[
                  styles.btnAddInvestments,
                  {
                    backgroundColor:
                      userAuth.verifiedIdDocument === false
                        ? '#ccc'
                        : colors.yellowDark,
                  },
                ]}
                onPress={
                  userAuth.verifiedIdDocument === false
                    ? null
                    : handleOpenModalRecommendation
                }
                activeOpacity={0.7}>
                <Icon name="add" color="#fff" size={20} />
              </TouchableOpacity>
              {/** userAuth.verifiedIdDocument === false
                    ? null
                    : */}
            </View>
            {evaluations.length > 0 ? (
              <FlatList
                data={evaluations}
                style={{ height: 300 }}
                renderItem={({ item }) => (
                  <View style={styles.viewRecommendationData}>
                    <View style={styles.imageViewRecommendation} />
                    <View style={styles.viewTextRecommendation}>
                      {/* <DateFormated data={item.CreatedAt} /> */}
                      <Text style={styles.textRecommendationDescription}>
                        {item.content}
                      </Text>
                    </View>
                  </View>
                )}
              />
            ) : (
              <View />
            )}

            <View style={styles.viewLineRecommendations} />
            <Text style={styles.textArticles}>ARTICLES - POSTS</Text>

            {articles.length > 0 ? (
              <FlatList
                data={articles}
                renderItem={({ item }) => (
                  <View style={styles.viewRecommendationData}>
                    <Image style={styles.imageArticle} source={advisor} />
                    <View style={styles.viewTextArticles}>
                      {/* <DateFormated data={item.CreatedAt} /> */}
                      <Text style={styles.titleArticle}>{item.title}</Text>
                      <Text style={styles.textArticlesDescription}>
                        {item.text.length < 120
                          ? item.text
                          : item.text.substring(0, 120)}
                        <Text
                          style={styles.textReadMore}
                          onPress={() =>
                            handleNavigateArticleDetails(item._id)
                          }>
                          {' '}
                          Read More
                        </Text>
                      </Text>
                    </View>
                  </View>
                )}
              />
            ) : (
              <View />
            )}
          </View>
        </ScrollView>
        <ModalRecommendations
          visible={visible}
          onRequestClose={() => setVisible(false)}
          idUser={id}
        />

        <ModalBadges
          visible={modalBadges}
          onRequestClose={() => setModalBadges(false)}
        />
      </View>
    </SafeAreaView>
  );
}
