import { StyleSheet } from 'react-native';

import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  chatTitle: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600',
  },
  icon: {
    marginLeft: 5,
    color: colors.white,
  },
  chatContentHeader: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    marginTop: 10,
    paddingBottom: 5,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  viewChatImage: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
  chatImage: {
    height: 30,
    width: 30,
    borderRadius: 15,
    backgroundColor: '#000',
  },
  chatTextName: {
    fontSize: 14,
    fontWeight: '600',
    marginLeft: 10,
  },
  chatTextInfoActive: {
    fontSize: 12,
    color: '#ddd',
    marginRight: 10,
  },
  balloon: {
    maxWidth: 250,
    padding: 15,
    borderRadius: 20,
  },
  itemIn: {
    alignSelf: 'flex-start',
  },
  itemOut: {
    alignSelf: 'flex-end',
  },
  time: {
    alignSelf: 'flex-end',
    margin: 15,
    fontSize: 12,
    color: '#808080',
  },
  item: {
    marginVertical: 5,
    flex: 1,
    flexDirection: 'row',
    borderBottomLeftRadius: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    marginLeft: 5,
    marginRight: 5,
  },
  textChat: {
    fontSize: 12,
  },
  viewSendInputMessage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderTopWidth: 1,
    borderTopColor: '#ccc',
    height: 45,
    position: 'absolute',
    bottom: 0,
    width: '100%',
  },
  inputMessage: {
    marginLeft: 20,
    flex: 1,
  },
  btnSend: {
    height: 30,
    width: 40,
    borderRadius: 10,
    backgroundColor: 'blue',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 10,
  },
  header: {
    height: 50,
    width: '100%',
    backgroundColor: colors.black,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textTitleHeader: {
    color: colors.white,
    marginRight: 15,
  },
  list: {
    paddingHorizontal: 5,
    height: 200,
  },
});

export default styles;
