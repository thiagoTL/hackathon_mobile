import React, { useState, useMemo, useEffect } from 'react';
import {
  Modal,
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
  Image,
} from 'react-native';
import socketio from 'socket.io-client';
import { useSelector, useDispatch } from 'react-redux';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import Icon from 'react-native-vector-icons/MaterialIcons';
import { useNavigation, useRoute } from '@react-navigation/native';
import IconA from 'react-native-vector-icons/AntDesign';
import IconIo from 'react-native-vector-icons/Ionicons';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';
import api from '../../../services/api';
import { ChatMessageRequest } from '../../../store/modules/chatmessage/actions';
import InputMessage from '../components/InputMessage';

Icon.loadFont();

export default function ModalChat() {
  const routes = useRoute();

  const itemsChat = routes.params.itens;

  const { _id: id } = itemsChat;

  const socket = useMemo(
    () => socketio('https://app.investexpert.co', { query: { message: id } }),
    [id],
  );
  // const socket = useMemo(
  //   () => socketio('http://localhost:5000', {query: {message: id}}),
  //   [id],
  // );
  // http://localhost:5000/
  const [newMessage, setNewMessage] = useState([]);
  const [message, setMessage] = useState('');
  const [arrayMessage, setArrayMessage] = useState([]);

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const userAuth = useSelector((state) => state.auth.user);

  useEffect(() => {
    socket.on('messages', (messageNew) => {
      setNewMessage([...newMessage, messageNew]);
    });
  }, [socket, newMessage]);

  useEffect(() => {
    async function loadMessage() {
      const response = await api.get(`/message/${id}`);

      setNewMessage(response.data);
      setArrayMessage(response.data);
    }

    loadMessage();
  }, []);

  function handleMessage() {
    const idUser = userAuth._id;

    dispatch(ChatMessageRequest(message, id, idUser));

    setMessage('');
  }

  const idUser = userAuth._id;

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.goBack()}>
          <IconA name="left" color={colors.yellowDark} size={25} />
        </TouchableOpacity>

        <Text style={styles.textTitleHeader}>Chat With</Text>
        <View />
      </View>
      <View style={styles.chatContent}>
        <View style={styles.chatContentHeader}>
          <View style={styles.viewChatImage}>
            {/* <View  style={styles.chatImage}> */}
            <Image
              style={styles.chatImage}
              source={{ uri: itemsChat.photoPrimary }}
            />
            {/* </View> */}
            <Text style={styles.chatTextName}>{itemsChat.name}</Text>
          </View>
          <Text style={styles.chatTextInfoActive}>Active 52m ago</Text>
        </View>
      </View>
      <View style={{ flex: 1 }}>
        <FlatList
          style={styles.list}
          data={newMessage}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => (
            <View
              style={[
                styles.item,
                {
                  backgroundColor:
                    item.user === userAuth._id ? 'blue' : colors.gray,
                  borderBottomRightRadius: item.user === userAuth._id ? 5 : 30,
                  borderTopLeftRadius: item.user === userAuth._id ? 30 : 5,
                  alignSelf:
                    item.user === userAuth._id ? 'flex-end' : 'flex-start',
                },
              ]}>
              <View style={[styles.balloon]}>
                <Text
                  style={[
                    styles.textChat,
                    {
                      color: item.user === userAuth._id ? '#fff' : '#000',
                    },
                  ]}>
                  {item.message}
                </Text>
              </View>
            </View>
          )}
        />
      </View>

      <InputMessage idUser={idUser} id={id} />
    </SafeAreaView>
  );
}
