import React, {useState} from 'react';
import {
  View,
  Text,
  TextInput,
  SafeAreaView,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';

import styles from './styles';
import {colors} from '../../../constants/colors';

Icon.loadFont();

export default function CheckInfoUser() {
  const [name, setName] = useState('Charlene');

  const navigation = useNavigation();

  function handleNavigate() {
    navigation.goBack();
  }

  function handleNavigateNext() {
    navigation.navigate('SelectDocumentCheck');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity activeOpacity={0.7} onPress={handleNavigate}>
          <Icon name="left" color={colors.white} size={20} />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
      <ScrollView>
        <View style={styles.viewContent}>
          <Text style={styles.title}>Welcome</Text>
          <Text style={styles.subTitle}>
            To verify your identity, please fill in personal information
          </Text>
          <Text style={styles.label}>First Name</Text>
          <TextInput
            value={name}
            onChangeText={setName}
            returnKeyType="next"
            style={styles.input}
          />
          <Text style={styles.label}>Surname</Text>
          <TextInput
            value={name}
            onChangeText={setName}
            returnKeyType="next"
            style={styles.input}
          />
          <Text style={styles.label}>Birthdate</Text>
          <TextInput
            value={name}
            onChangeText={setName}
            returnKeyType="next"
            style={styles.input}
          />
          <Text style={styles.label}>Email</Text>
          <TextInput
            value={name}
            onChangeText={setName}
            returnKeyType="next"
            style={styles.input}
          />
          <Text style={styles.label}>Country</Text>
          <TextInput
            value={name}
            onChangeText={setName}
            returnKeyType="next"
            style={styles.input}
          />

          <TouchableOpacity
            style={styles.btnLogin}
            activeOpacity={0.6}
            onPress={handleNavigateNext}>
            <Text style={styles.btnLoginText}>Next</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
