import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  header: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: 'blue',
    width: 200,
    marginLeft: 20,
  },
  viewContent: {
    marginTop: 30,
    marginLeft: 15,
  },
  title: {
    fontSize: 20,
    color: colors.white,
    fontWeight: 'bold',
  },
  subTitle: {
    fontSize: 15,
    marginTop: 10,
    width: 250,
    color: colors.white,
    marginBottom: 10,
  },
  label: {
    fontSize: 15,
    color: colors.white,
    marginTop: 10,
  },
  input: {
    width: '92%',
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
    color: colors.white,
    // paddingLeft: 5,
    paddingTop: 10,
    paddingBottom: 5,
    fontWeight: '600',
    marginBottom: 10,
  },
  btnLogin: {
    height: 38,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 10,
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
