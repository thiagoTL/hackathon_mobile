import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ChatMessage from './ChatMessage';
import EditProfile from './EditProfile';
import ProfileComplete from './ProfileComplete';
import ArticleDetails from './ArticleDetails';

import RoutesBottom from './routesBottom';

const InvestorStack = createStackNavigator();

export default function Investor() {
  return (
    <InvestorStack.Navigator initialRouteName="RoutesBottom">
      <InvestorStack.Screen
        name="RoutesBottom"
        component={RoutesBottom}
        options={{ headerShown: false }}
      />
      <InvestorStack.Screen
        name="ChatMessage"
        component={ChatMessage}
        options={{ headerShown: false }}
      />
      <InvestorStack.Screen
        name="EditProfile"
        component={EditProfile}
        options={{ headerShown: false }}
      />
      <InvestorStack.Screen
        name="ProfileComplete"
        component={ProfileComplete}
        options={{ headerShown: false }}
      />
      <InvestorStack.Screen
        name="ArticleDetails"
        component={ArticleDetails}
        options={{ headerShown: false }}
      />
    </InvestorStack.Navigator>
  );
}
