import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {scale} from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconF from 'react-native-vector-icons/AntDesign';

import {colors} from '../../../../constants/colors';

Icon.loadFont();
IconF.loadFont();

export default function Footer() {
  return (
    <View style={styles.containerFooter}>
      <TouchableOpacity style={styles.btnPerc}>
        <Text style={styles.btnPercText}>7.12%</Text>
        <IconF name="caretup" size={10} style={styles.icon} color="#fff" />
      </TouchableOpacity>
      <View style={styles.btnBuySell}>
        <View style={styles.btnBuy}>
          <View style={styles.viewArrowUp}>
            <IconF name="arrowup" size={8} color={colors.green} />
          </View>
        </View>
        <View style={styles.btnSell}>
          <View style={styles.viewArrowUp}>
            <IconF name="arrowdown" size={8} color={colors.red} />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerFooter: {
    height: 40,
    width: '100%',
    position: 'absolute',
    bottom: 50,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  btnPerc: {
    height: scale(25),
    width: scale(70),
    backgroundColor: colors.purple,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  btnPercText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  btnBuySell: {
    flexDirection: 'row',
  },
  btnBuy: {
    height: scale(25),
    width: scale(55),
    backgroundColor: colors.green,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
  },
  btnBuyText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  btnSell: {
    height: scale(25),
    width: scale(55),
    backgroundColor: colors.red,
    justifyContent: 'center',
    alignItems: 'center',
    borderTopRightRadius: 15,
    borderBottomRightRadius: 15,
  },
  btnSellText: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: 'bold',
  },
  viewArrowUp: {
    height: scale(15),
    width: scale(15),
    backgroundColor: colors.white,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
