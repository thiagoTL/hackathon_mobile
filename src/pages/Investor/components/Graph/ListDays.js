import React from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Platform} from 'react-native';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

export default function ListDays({onPress, label}) {
  return (
    <TouchableOpacity {...{onPress}} style={[styles.viewDay, {}]}>
      <Text style={[styles.viewDayText]}>{label}</Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  viewDay: {
    height: scale(25),
    width: scale(40),
    borderRadius: 10,
    backgroundColor: '#222',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? scale(5) : 10,
  },
  viewDayText: {
    fontSize: 12,
    // marginLeft: 20,
    fontWeight: '500',
    color: colors.white,
  },
});
