import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import Graph from './Graph';

export default function Rainbow({onPressGoBack}) {
  return (
    <View style={styles.container}>
      <Graph onPressGoBack={onPressGoBack} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});
