import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import bitcoin from '../../../../assets/images/bitcoinIcon.png';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

Icon.loadFont();

export default function Header({onPress}) {
  return (
    <View style={styles.container}>
      <View style={styles.headerBit}>
        <View>
          <Image source={bitcoin} style={styles.image} />
          <Text style={styles.textMoeda}>Bitcoin</Text>
          <Text style={styles.textValue}>$6.564,30</Text>
        </View>
        <TouchableOpacity
          // activeOpacity={0.8}
          style={styles.btnClose}
          onPress={() => console.log('Ok')}>
          <Icon name="close" size={18} color={colors.white} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    ...StyleSheet.absoluteFill,
  },
  title: {
    fontSize: 16,
    color: '#fff',
    fontWeight: 'bold',
  },
  viewClose: {
    height: 40,
    width: 40,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
  },
  textClose: {
    fontSize: 14,
    fontWeight: 'bold',
    color: '#212121',
  },
  headerBit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: scale(10),
    marginLeft: scale(25),
    marginRight: scale(15),
    position: 'absolute',
    width: '90%',
    // backgroundColor: '#fff',
  },
  textMoeda: {
    color: colors.white,
    fontSize: scale(30),
    fontWeight: 'bold',
    marginTop: scale(8),
  },
  textValue: {
    color: 'green',
    fontSize: scale(20),
    fontWeight: '600',
  },
  btnClose: {
    height: scale(25),
    width: scale(25),
    borderRadius: 13,
    backgroundColor: '#222',
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    marginLeft: scale(5),
    marginTop: scale(3),
  },
  image: {
    width: scale(50),
    height: scale(50),
  },
});
