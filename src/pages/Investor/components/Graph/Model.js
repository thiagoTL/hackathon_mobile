import {Dimensions} from 'react-native';

export const SIZE = Dimensions.get('window').width;

export const Amount = {
  amount: '',
  currency: '',
  scale: '',
};

export const PercentChange = {
  hour: 0,
  day: 0,
  week: 0,
  month: 0,
  year: 0,
};

export const LatestPrice = {
  amount: Amount,
  timestamp: '',
  percent_change: PercentChange,
};

export const Price = [];
export const PriceList = [];

export const DataPoints = {
  percent_change: 0,
  prices: PriceList,
};

export const Prices = {
  latest: '',
  latest_price: LatestPrice,
  hour: DataPoints,
  day: DataPoints,
  week: DataPoints,
  month: DataPoints,
  year: DataPoints,
  all: DataPoints,
};

export const Data = {
  base: '',
  base_id: '',
  unit_price_scale: 0,
  currency: 0,
  prices: Prices,
};
