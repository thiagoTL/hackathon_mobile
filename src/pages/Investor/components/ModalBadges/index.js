import React from 'react';
import {View, Text, Modal, Image, TouchableOpacity} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';

import styles from './styles';
import {colors} from '../../../../constants/colors';
import {colorsClient} from '../../../../constants/colorsClient';
import imageCfa from '../../../../assets/images/bagdes/cfa.png';

export default function ModalBadges({visible, onRequestClose}) {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => onRequestClose()}
      transparent={true}>
      <View style={styles.container}>
        <View style={styles.content}>
          <View style={styles.imageBadges}>
            <Image source={imageCfa} style={styles.image} />
          </View>
          <Text style={styles.title}>CFA</Text>
          <Text style={styles.subTitle}>Chart Financial Analyst</Text>
          <Text style={styles.textLevel}>Level 01</Text>

          <View style={styles.lineView} />

          <Text style={styles.textDescription}>
            A CFA charterholder has the technical knowledge to make invest
            decisions and is held to the highest ethical standards.
          </Text>
          <TouchableOpacity
            style={styles.btnOk}
            onPress={() => onRequestClose()}
            activeOpacity={0.7}>
            <LinearGradient
              colors={[colors.yellowT, colors.burntyellow]}
              style={styles.btnGradient}>
              <Text style={styles.btnOkText}>OK</Text>
            </LinearGradient>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
}
