import {StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ifIphoneX} from 'react-native-iphone-x-helper';

import {colors} from '../../../../constants/colors';
import {colorsClient} from '../../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.4)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    height: Platform.OS === 'ios' ? 300 : 320,
    width: wp('80%'),
    backgroundColor: '#fff',
    borderRadius: 10,
    borderColor: '#000',
    shadowColor: '#000',
    borderWidth: 1,
    shadowOffset: {
      height: 20,
      width: 20,
    },
    shadowOpacity: 50,
    shadowRadius: 50,
    elevation: 20,
    alignItems: 'center',
  },
  title: {
    fontSize: Platform.OS === 'ios' ? 23 : 25,
    fontWeight: 'bold',
    marginTop: 45,
  },
  subTitle: {
    fontSize: Platform.OS === 'ios' ? 18 : 20,
    fontWeight: 'bold',
  },
  textLevel: {
    fontSize: Platform.OS === 'ios' ? 18 : 20,
    fontWeight: 'bold',
  },
  lineView: {
    borderBottomWidth: 2,
    borderBottomColor: colors.yellowDark,
    width: wp('70%'),
    marginTop: 10,
  },
  textDescription: {
    fontSize: Platform.OS === 'ios' ? 14 : 17,
    fontWeight: 'bold',
    textAlign: 'center',
    marginTop: 10,
    maxWidth: 300,
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: Platform.OS === 'ios' ? 20 : 10,
  },
  btnOk: {
    height: 60,
    width: wp('70%'),
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnOkText: {
    fontSize: 18,
    color: colors.white,
    fontWeight: 'bold',
  },
  imageBadges: {
    ...ifIphoneX({height: 90}, {height: Platform.OS === 'ios' ? 60 : 80}),
    ...ifIphoneX({width: 90}, {width: Platform.OS === 'ios' ? 60 : 80}),
    borderWidth: 2,
    borderColor: colors.yellowT,
    backgroundColor: '#ccc',
    alignSelf: 'center',
    borderRadius: 40,
    position: 'absolute',
    ...ifIphoneX({bottom: 125}, {bottom: Platform.OS === 'ios' ? 230 : 250}),
    marginBottom: 25,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: Platform.OS === 'ios' ? 45 : 70,
    width: Platform.OS === 'ios' ? 45 : 70,
  },
});

export default styles;
