import {StyleSheet} from 'react-native'

import { colors } from '../../../../constants/colors'
import { colorsClient } from '../../../../constants/colorsClient'

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // height: 20,
        borderTopColor: colors.gray,
        borderTopWidth: 1,
        flexDirection: "row",
        alignItems: "center"
    },
    mainContainer: {
        flexDirection: "row",
        flex: 1,
        alignItems: "center"
    },
    input: {
        flex: 1,
        paddingLeft: 10
    }, 
    buttonContainer: {
        marginHorizontal: 15,
        borderLeftColor: colors.gray,
        borderLeftWidth: 1
    },
    btnSend: {
        height: 40,
        width: 40,
        borderRadius: 20,
        backgroundColor: colorsClient.blue,
        justifyContent: "center",
        alignItems: "center",
        marginLeft: 10
    }
})

export default styles