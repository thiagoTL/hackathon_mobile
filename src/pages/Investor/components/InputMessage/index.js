import React, { useState, useEffect } from 'react'
import {View, Text, TextInput, TouchableOpacity} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'
import IconIonicons from 'react-native-vector-icons/Ionicons'
import { useDispatch, useSelector } from 'react-redux'

import styles from './styles'
import { colors } from '../../../../constants/colors'
import { ChatMessageRequest } from '../../../../store/modules/chatmessage/actions'

Icon.loadFont();
IconIonicons.loadFont();

export default function InputMessage({ idUser, id }) {
    const [ message, setMessage ] = useState("");

    const dispatch = useDispatch();

    function handleSendMesssage() {
        dispatch(ChatMessageRequest(message, id, idUser))

        setMessage("")
    }

    return (
        <View style={styles.container}>

            <View style={styles.mainContainer}>
                <TextInput 
                    placeholder="Type message to ssend"
                    style={styles.input}
                    multiline={true}
                    value={message}
                    onChangeText={setMessage}
                />

                <IconIonicons name="image-outline" color={colors.blackweak} size={28}/>
                <Icon name="emoji-emotions" size={28} color={colors.blackweak}/>
            </View>
            
           <View style={styles.buttonContainer}>
               <TouchableOpacity style={styles.btnSend} activeOpacity={0.7} onPress={handleSendMesssage}>
                    <Icon name="send" color={colors.white} size={20}/>
               </TouchableOpacity>
           </View>
        </View>
    )
}