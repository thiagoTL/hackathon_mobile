import React from 'react';
import {View, StyleSheet} from 'react-native';

import StarDashboard from '../StarDashboard';

const numStars = 5;

export default function DashboardStar({rating, size}) {
  let stars = [];

  for (let x = 1; x <= numStars; x++) {
    stars.push(
      <View key={x}>
        <StarDashboard filled={x <= rating ? true : false} size={size} />
      </View>,
    );
  }

  return <View style={styles.container}>{stars}</View>;
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },
});
