import React from 'react';
import { View, Text, ScrollView } from 'react-native';
// import {ScrollView} from 'react-native-gesture-handler';

import styles from './styles';

const formatter = Intl.DateTimeFormat('en', { month: 'short' });

const lerp = (v0, v1, t) => {
  return (1 - t) * v0 + t * v1;
};

export default function Underlay({ dates, minY, maxY, step }) {
  return (
    <View style={styles.containerUnderlay}>
      <View style={styles.barValues}>
        {[1, 0.66, 0.33, 0].map((progress) => {
          return (
            <View key={progress} style={styles.viewValues}>
              <View style={styles.values}>
                <Text style={styles.valuesText}>
                  {Math.round(lerp(minY, maxY, progress))}
                </Text>
              </View>
              {/* <View style={{flex: 1, height: 1, backgroundColor: 'grey'}} /> */}
            </View>
          );
        })}
      </View>
      <View style={styles.viewsMonths}>
        {dates.map((date, index) => (
          <View style={{ width: step }}>
            <Text key={index} style={styles.monthsText}>
              {formatter.format(new Date(date))}
            </Text>
          </View>
        ))}
      </View>
    </View>
  );
}
