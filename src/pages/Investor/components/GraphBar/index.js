import React from 'react';
import { View, Dimensions, ScrollView } from 'react-native';
// import {ScrollView} from 'react-native-gesture-handler';

import styles from './styles';
import Underlay from './Underlay';

const { width: wWidth } = Dimensions.get('window');

const lerp = (v0, v1, t) => {
  return (1 - t) * v0 + t * v1;
};

export default function Graph({ data }) {
  const values = data.map((p) => p.value);
  const dates = data.map((p) => p.date);
  const minY = Math.min(...values);
  const maxY = Math.max(...values);
  const step = wWidth / data.length;

  const canvasHeight = 180;
  const canvasWidth = 391 - 60;

  const width = canvasWidth + 60;
  const height = canvasHeight;

  return (
    <View style={styles.containerView}>
      <Underlay minY={minY} maxY={maxY} dates={dates} step={step} />
      <View style={[styles.container, { width: width, height: height }]}>
        {data.map((point, i) => {
          if (point.value === 0) {
            return null;
          }

          return (
            <View
              style={[
                styles.contentBar,
                {
                  width: step,
                  left: i * step,
                  height: lerp(0, height, point.value / maxY),
                },
              ]}>
              <View style={styles.bar} />
            </View>
          );
        })}
      </View>
    </View>
  );
}
