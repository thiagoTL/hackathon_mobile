import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  containerView: {
    paddingBottom: 40,
    paddingLeft: 45,
    marginTop: 30,
  },
  container: {
    // alignSelf: 'center',
    // marginTop: 30,
  },
  contentBar: {
    position: 'absolute',
    bottom: 0,
  },
  bar: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 15,
    right: 15,
    backgroundColor: '#76DDFB',
  },
  containerUnderlay: {
    ...StyleSheet.absoluteFill,
    paddingLeft: 20,
  },
  barValues: {
    flex: 0.93,
    justifyContent: 'space-between',
  },
  viewValues: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  values: {
    width: 40,
    paddingRight: 10,
  },
  valuesText: {
    color: '#ffffff',
    textAlign: 'right',
  },
  viewsMonths: {
    marginLeft: 23,
    flexDirection: 'row',
    alignItems: 'center',
  },
  monthsText: {
    color: '#ffffff',
    textAlign: 'center',
  },
});

export default styles;
