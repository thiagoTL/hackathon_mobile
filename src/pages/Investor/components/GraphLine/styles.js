import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  labelTitle: {
    fontSize: 40,
    fontWeight: '500',
    color: '#fff',
  },
  labelSubTitle: {
    fontSize: 14,
    color: '#fff',
    fontWeight: '600',
  },
  header: {
    alignItems: 'center',
  },
  footer: {
    alignItems: 'center',
    marginTop: 40,
  },
  footerLabel: {
    fontSize: 20,
    fontWeight: '500',
    color: '#fff',
  },
  footerSubLabel: {
    fontSize: 14,
    fontWeight: '600',
    color: '#fff',
  },
});

export default styles;
