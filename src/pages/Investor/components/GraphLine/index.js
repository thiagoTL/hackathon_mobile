import React from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import Svg, { Path } from 'react-native-svg';
import * as shape from 'd3-shape';
import {
  useSharedValue,
  useDerivedValue,
  interpolate,
  Extrapolate,
} from 'react-native-reanimated';
import { scale as scales } from 'react-native-size-matters';

import { parsePath, getPointAtLength } from '../AnimatedHelpers';

import Label from './Label';
import { colors } from '../../../../constants/colors';

const { width } = Dimensions.get('window');
const height = width;

const data = [
  { x: new Date(2020, 5, 1), y: 12412 },
  { x: new Date(2020, 5, 2), y: 34650 },
  { x: new Date(2020, 5, 3), y: 12412 },
  { x: new Date(2020, 5, 4), y: 5060 },
  { x: new Date(2020, 5, 5), y: 25999 },
  { x: new Date(2020, 5, 6), y: 12412 },
  { x: new Date(2020, 5, 7), y: 34 },
  { x: new Date(2020, 5, 8), y: 23425 },
  { x: new Date(2020, 5, 9), y: 12412 },
  { x: new Date(2020, 5, 10), y: 28000 },
  { x: new Date(2020, 5, 11), y: 25000 },
  { x: new Date(2020, 5, 12), y: 15999 },
].map((p) => [p.x.getTime(), p.y]);

const domain = {
  x: [Math.min(...data.map(([x]) => x)), Math.max(...data.map(([x]) => x))],
  y: [Math.min(...data.map(([, y]) => y)), Math.max(...data.map(([, y]) => y))],
};

const range = {
  x: [0, width],
  y: [height, 290],
};

const scale = (v, d, r) => {
  'worklet';
  return interpolate(v, d, r, Extrapolate.CLAMP);
};

const scaleInvert = (y, d, r) => {
  'worklet';
  return interpolate(y, r, d, Extrapolate.CLAMP);
};

const d = shape
  .line()
  .x(([x]) => scale(x, domain.x, range.x))
  .y(([, y]) => scale(y, domain.y, range.y))
  .curve(shape.curveBasis)(data);
const path = parsePath(d);

const styles = StyleSheet.create({
  container: {
    height: scales(225),
    width: scales(350),
    backgroundColor: colors.black,
  },
});

const Graph = () => {
  const length = useSharedValue(0);
  const point = useDerivedValue(() => {
    const p = getPointAtLength(path, length.value);
    return {
      coord: {
        x: p.x,
        y: p.y,
      },
      data: {
        x: scaleInvert(p.x, domain.x, range.x),
        y: scaleInvert(p.y, domain.y, range.y),
      },
    };
  });

  return (
    <View style={styles.container}>
      <Label {...{ data, point }} />
      <View style={{ position: 'absolute', bottom: 0 }}>
        <Svg {...{ width, height }}>
          {/* <Defs>
            <LinearGradient x1="50%" y1="0%" x2="50%" y2="100%" id="gradient">
              <Stop stopColor="#CDE3F8" offset="0%" />
              <Stop stopColor="#eef6fd" offset="80%" />
              <Stop stopColor="#FEFFFF" offset="100%" />
            </LinearGradient>
          </Defs> */}
          {/* <Path
            fill="transparent"
            // stroke="#ccc"
            strokeWidth={4}
            {...{d}}
          /> */}
          <Path d={`${d}  L ${width} ${height} L 0 ${height}`} fill="#FFD60A" />
        </Svg>
      </View>
    </View>
  );
};

export default Graph;
