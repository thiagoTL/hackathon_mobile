import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

export default function Label() {
  return (
    <View style={styles.label}>
      <View style={styles.header}>
        <Text style={styles.labelTitle}>$7,265.55</Text>
        <Text style={styles.labelSubTitle}>in USD • US dollar</Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.footerLabel}>$10,201.31</Text>
        <Text style={styles.footerSubLabel}>total in CAD equivalent</Text>
      </View>
    </View>
  );
}
