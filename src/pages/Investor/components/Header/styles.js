import { StyleSheet } from 'react-native';

import { colors } from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.black,
    width: '100%',
  },
  header: {
    height: 50,
    width: '100%',
    backgroundColor: colors.black,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingLeft: 10,
    paddingRight: 10,
  },
  headerTitle: {
    fontSize: 18,
    color: colors.white,
    fontWeight: 'bold',
  },
});

export default styles;
