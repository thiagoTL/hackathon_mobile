import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import { colors } from '../../../../constants/colors';

Icon.loadFont();

export default function Header({ title, iconLeft, iconRight, onPress }) {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        {iconLeft === '' ? (
          <View style={{ marginRight: 10 }} />
        ) : (
          <TouchableOpacity {...{ onPress }} activeOpacity={0.7}>
            <Icon name={iconLeft} color={colors.yellowDark} size={25} />
          </TouchableOpacity>
        )}

        <Text style={styles.headerTitle}>{title}</Text>
        {iconRight === '' ? (
          <View style={{ marginRight: 10 }} />
        ) : (
          <Icon name={iconRight} color={colors.yellowDark} size={25} />
        )}
      </View>
    </View>
  );
}
