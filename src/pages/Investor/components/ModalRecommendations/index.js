import React, {useState} from 'react';
import {
  View,
  Text,
  Modal,
  KeyboardAvoidingView,
  Platform,
  TouchableNativeFeedback,
  Animated,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {useDispatch, useSelector} from 'react-redux';

import Stars from '../StarRecommendation';
import {evaluationsStoreRequest} from '../../../../store/modules/evaluations/actions';
import styles from './styles';

const numStars = 5;

Icon.loadFont();

export default function ModalRecommendations({
  visible,
  onRequestClose,
  idUser,
}) {
  const [rating, setRating] = useState(0);
  const [recommendationText, setRecommendationText] = useState('');

  const dispatch = useDispatch();

  const loading = useSelector((state) => state.evaluations.loading);
  const user = useSelector((state) => state.auth.user);

  const stars = [];

  function handleSubmitRecommendation() {
    dispatch(evaluationsStoreRequest(recommendationText, rating, idUser));

    if (!loading) {
      onRequestClose();
    }
  }

  function rate(star) {
    setRating(star);
  }

  for (let x = 1; x <= numStars; x++) {
    stars.push(
      <TouchableNativeFeedback onPress={() => rate(x)} key={x}>
        <Animated.View>
          <Stars filled={x <= rating ? true : false} profile="reco" />
        </Animated.View>
      </TouchableNativeFeedback>,
    );
  }

  return (
    <Modal
      visible={visible}
      onRequestClose={() => onRequestClose()}
      transparent={true}
      animationType="fade">
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
        style={styles.container}>
        <View style={styles.content}>
          <TouchableOpacity
            style={styles.btnClose}
            onPress={() => onRequestClose()}
            activeOpacity={0.7}>
            <Icon name="close" color="#ccc" size={18} />
          </TouchableOpacity>
          <View style={styles.headerContent}>
            <View style={styles.viewRowContent}>
              <View style={styles.viewImage} />
              <View style={styles.viewColumn}>
                <View style={styles.stars}>{stars}</View>
                <Text style={styles.textNameRecommendation}>
                  {user.firstName + ' ' + user.lastName}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.viewRecommendation}>
            <Text style={styles.textRecommendation}>RECOMMENDATION</Text>
            <View style={styles.viewCaracteres}>
              <View style={styles.viewBorderCaracteres}>
                <Text style={styles.viewBorderCaracteresText}>250</Text>
              </View>
              <Text style={styles.textCaracteres}>CARACTERES</Text>
            </View>
          </View>

          <TextInput
            placeholderTextColor="#ccc"
            multiline={true}
            placeholder="Write your recommendation here..."
            style={styles.input}
            value={recommendationText}
            onChangeText={setRecommendationText}
          />

          <View style={styles.viewRadioButton}>
            <View style={styles.viewRadio} />
            <Text style={styles.textDon}>Don't show my picture</Text>
          </View>

          <TouchableOpacity
            style={styles.btnChat}
            activeOpacity={0.7}
            onPress={handleSubmitRecommendation}>
            <Text style={styles.btnChatText}>SHARE MY RECOMMENDATION</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAvoidingView>
    </Modal>
  );
}
