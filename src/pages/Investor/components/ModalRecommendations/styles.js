import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ifIphoneX} from 'react-native-iphone-x-helper';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
  content: {
    // flex: 3,
    height: hp('80%'),
    backgroundColor: colors.white,
    width: wp('100%'),
    position: 'absolute',
    bottom: 0,
    borderRadius: 30,
  },
  headerContent: {
    height: 80,
    width: '90%',
    backgroundColor: '#000',
    alignSelf: 'center',
    marginTop: 20,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  viewRowContent: {
    flexDirection: 'row',
  },
  viewImage: {
    height: 80,
    width: 80,
    borderRadius: 20,
    borderColor: '#fff',
    borderWidth: 2,
    backgroundColor: '#eee',
    position: 'absolute',
    marginTop: 10,
    marginLeft: 10,
  },
  viewColumn: {
    flexDirection: 'column',
    marginLeft: 100,
  },
  stars: {
    flexDirection: 'row',
    // marginBottom: 80,
  },
  textNameRecommendation: {
    fontSize: 14,
    color: '#fff',
    marginTop: 8,
    marginLeft: 8,
  },
  viewRecommendation: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginLeft: 20,
    marginRight: 20,
    marginTop: 20,
  },
  viewCaracteres: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  viewBorderCaracteres: {
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 10,
    height: 20,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewBorderCaracteresText: {
    fontSize: 10,
    color: '#222',
  },
  textCaracteres: {
    fontSize: 8,
    color: '#222',
    marginLeft: 5,
  },
  textRecommendation: {
    fontSize: 12,
    fontWeight: '600',
  },
  input: {
    height: 100,
    width: '88%',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ccc',
    alignSelf: 'center',
    marginTop: 10,
    paddingLeft: 10,
    paddingTop: 10,
  },
  viewRadioButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
    marginTop: 15,
  },
  viewRadio: {
    height: 20,
    width: 20,
    borderRadius: 10,
    backgroundColor: '#ccc',
  },
  textDon: {
    marginLeft: 10,
    fontSize: 12,
    fontWeight: '500',
  },
  btnChat: {
    height: 45,
    width: wp('90%'),
    backgroundColor: colors.yellowDark,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 30,
    position: 'absolute',
    bottom: 0,
    shadowColor: '#ccc',
    shadowOffset: {
      height: 5,
      width: 5,
    },
    shadowOpacity: 8,
    shadowRadius: 3,
    ...ifIphoneX({marginBottom: 40}, {marginBottom: 10}),
  },
  btnChatText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: 'bold',
  },
  btnClose: {
    alignSelf: 'flex-end',
    marginRight: 20,
    marginTop: 10,
    backgroundColor: '#000',
    height: 30,
    width: 30,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
