import {StyleSheet} from 'react-native';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  starView: {
    height: 30,
    width: 30,
    backgroundColor: colors.yellowDark,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginTop: 52,
  },
});

export default styles;
