import React, {useMemo} from 'react';
import {TouchableOpacity, View, Text, DatePickerAndroid} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {format} from 'date-fns';
import pt from 'date-fns/locale/pt';

import styles from './styles';

export default function DatePicker({date, onChange}) {
  const dateFormated = useMemo(
    () => format(date, "dd 'de' MMMM 'de' yyyy", {locale: pt}),
    [date],
  );

  async function handleOpenPicker() {
    const {action, year, month, day} = await DatePickerAndroid.open({
      mode: 'spinner',
      date,
    });

    if (action === DatePickerAndroid.dateSetAction) {
      const selectedDate = new Date(year, month, day);

      onChange(selectedDate);
    }
  }

  return (
    <View>
      <TouchableOpacity onPress={handleOpenPicker}>
        <Text style={styles.dateText}>{dateFormated}</Text>
        <Icon
          name="caretdown"
          color="#000"
          size={12}
          style={{marginLeft: 20}}
        />
      </TouchableOpacity>
    </View>
  );
}
