import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  container: {
    // margin: 60,
  },
  dateButton: {
    paddingLeft: 10,
    width: 180,
    borderWidth: 1,
    borderColor: '#ccc',
    height: 30,
    // backgroundColor: 'rgba(0,0,0,0.1)',
    borderRadius: 4,
    flexDirection: 'row',
    alignItems: 'center',
  },
  dateText: {
    fontSize: 12,
    color: '#000',
    marginRight: 8,
  },
  picker: {
    // width: '100%',
    // flex: 1,
    width: '100%',
    position: 'absolute',
    // bottom: 0,
    backgroundColor: '#222',
    padding: 30,
    marginTop: 30,
  },
});

export default styles;
