import React, {useState, useMemo} from 'react';
import {TouchableOpacity, View, Text, DatePickerIOS} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import {format} from 'date-fns';
import pt from 'date-fns/locale/pt';
// import DateTimePicker from '@react-native-community/datetimepicker';

import styles from './styles';

Icon.loadFont();

export default function DatePicker({date, onChange}) {
  const [opened, setOpened] = useState(false);

  const dateFormated = useMemo(
    () => format(date, "dd 'de' MMMM 'de' yyyy", {locale: pt}),
    [date],
  );

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => setOpened(!opened)}
        style={styles.dateButton}>
        <Text style={styles.dateText}>{dateFormated}</Text>
        <Icon
          name="caretdown"
          color="#000"
          size={12}
          style={{marginLeft: 20}}
        />
      </TouchableOpacity>

      {opened && (
        <View style={styles.picker}>
          <DatePickerIOS
            date={date}
            onDateChange={onChange}
            minimumDate={new Date()}
            minuteInterval={60}
            style={{color: '#fff'}}
            locale="pt"
            mode="date"
          />
          {/* <DateTimePicker
            testID="dateTimePicker"
            timeZoneOffsetInMinutes={0}
            value={date}
            mode="date"
            is24Hour={true}
            display="default"
            onChange={onChange}
          /> */}
        </View>
      )}
    </View>
  );
}
