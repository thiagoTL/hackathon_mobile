import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import Header from '../../components/Header';

import cardIcon from '../../../../assets/images/cardIcon.png';
import crypto from '../../../../assets/images/crypto.png';
import money from '../../../../assets/images/money.png';
import invest from '../../../../assets/images/investiments.png';
import bank from '../../../../assets/images/bank.png';
import pink from '../../../../assets/images/ping.png';

Icon.loadFont();

export default function Wallet() {
  const navigation = useNavigation();

  const data = [
    { id: 1, image: invest, title: 'Stocks' },
    { id: 2, image: cardIcon, title: 'Mutual Fund' },
    { id: 3, image: crypto, title: 'Cryptocoins' },
    { id: 4, image: money, title: 'Debentures' },
    { id: 5, image: bank, title: 'Goverment Bonds' },
    { id: 6, image: pink, title: 'Savings' },
  ];

  function handleDashboardWallet() {
    navigation.navigate('SelectAsset');
  }

  return (
    <SafeAreaView style={styles.container}>
      <Header
        title="Select Investments"
        iconLeft="left"
        iconRight=""
        onPress={() => navigation.goBack()}
      />

      <View style={styles.viewExample}>
        <FlatList
          style={styles.viewContentList}
          data={data}
          showsVerticalScrollIndicator={false}
          numColumns={2}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.cardList}
              activeOpacity={0.7}
              onPress={() => navigation.navigate('SelectAsset')}>
              <Image source={item.image} style={styles.imageCard} />
              <Text style={styles.textCard}>{item.title}</Text>
            </TouchableOpacity>
          )}
        />
      </View>
    </SafeAreaView>
  );
}
