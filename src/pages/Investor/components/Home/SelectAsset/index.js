import React, {useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import Icon from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
import LinearGradient from 'react-native-linear-gradient';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import styles from './styles';
import {colors} from '../../../../constants/colors';
import Header from '../../components/Header';

Icon.loadFont();

export default function SelectAsset() {
  const [date, setDate] = useState(new Date());

  const navigation = useNavigation();

  function navigationDone() {
    navigation.navigate('DashboardWallet');
  }

  return (
    <SafeAreaView style={styles.container}>
      {/* <View style={styles.headerContent}>
        <TouchableOpacity
          onPress={() => navigation.goBack()}
          activeOpacity={0.7}>
          <Icon
            name="left"
            size={Platform.OS === 'ios' ? 28 : 32}
            style={styles.icon}
          />
        </TouchableOpacity>
        <Text style={styles.headerTitle}>Select Investments</Text>
      </View> */}
      <Header
        title="Select Investments"
        iconLeft="left"
        iconRight=""
        onPress={() => navigation.goBack()}
      />
      <View style={styles.viewExample}>
        <View style={styles.viewRadio}>
          <View style={styles.viewRadioContent}>
            <LinearGradient
              colors={[colors.yellow, colors.yellowDark]}
              style={styles.gradient}>
              <Text style={styles.gradientText}>Buy</Text>
            </LinearGradient>
          </View>

          <View style={styles.viewRadioContent}>
            <View style={styles.viewSell}>
              <Text style={styles.gradientText}>Sell</Text>
            </View>
          </View>
        </View>
        <View style={styles.viewSelect}>
          <RNPickerSelect
            placeholder={{
              label: 'where did you buy ?',
              value: null,
              color: colors.white,
            }}
            useNativeAndroidPickerStyle={false}
            onValueChange={(value) => console.log(value)}
            style={{
              ...pickerSelectStyles,
              iconContainer: {
                top: scale(45),
                right: scale(40),
                height: Platform.OS === 'ios' ? scale(15) : 20,
                width: Platform.OS === 'ios' ? scale(15) : 20,
                backgroundColor: colors.white,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              },
              placeholder: {
                color: colors.white,
                fontSize: scale(10),
                fontWeight: 'bold',
              },
            }}
            items={[
              {label: 'Football', value: 'football'},
              {label: 'Baseball', value: 'baseball'},
              {label: 'Hockey', value: 'hockey'},
            ]}
            Icon={() => (
              <Icon
                name="down"
                color="#000"
                size={Platform.OS === 'ios' ? 8 : 12}
              />
            )}
          />
          <RNPickerSelect
            placeholder={{
              label: 'Select cryptocurrency ?',
              value: null,
              color: colors.white,
            }}
            onValueChange={(value) => console.log(value)}
            useNativeAndroidPickerStyle={false}
            style={{
              ...pickerSelectStylesCurrecy,
              iconContainer: {
                top: scale(25),
                right: scale(40),
                height: Platform.OS === 'ios' ? scale(15) : 20,
                width: Platform.OS === 'ios' ? scale(15) : 20,
                backgroundColor: colors.white,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              },
              placeholder: {
                color: colors.white,
                fontSize: scale(10),
                fontWeight: 'bold',
              },
            }}
            items={[
              {label: 'Football', value: 'football'},
              {label: 'Baseball', value: 'baseball'},
              {label: 'Hockey', value: 'hockey'},
            ]}
            Icon={() => (
              <Icon
                name="down"
                color="#000"
                size={Platform.OS === 'ios' ? 8 : 12}
              />
            )}
          />
        </View>
        <View style={styles.viewContent}>
          <TextInput
            placeholder="How much did you buy ?"
            placeholderTextColor={colors.white}
            style={styles.inputAmount}
          />
        </View>
        <View style={styles.viewContent}>
          <TextInput
            placeholder="How much did you pay ?"
            placeholderTextColor={colors.white}
            style={styles.inputAmount}
          />
        </View>
        <View style={styles.viewContent}>
          <RNPickerSelect
            placeholder={{
              label: 'When did you buy / sell?',
              value: null,
              color: colors.white,
            }}
            onValueChange={(value) => console.log(value)}
            useNativeAndroidPickerStyle={false}
            style={{
              ...pickerSelectStylesBuySell,
              iconContainer: {
                top: scale(14),
                right: scale(40),
                height: Platform.OS === 'ios' ? scale(15) : 20,
                width: Platform.OS === 'ios' ? scale(15) : 20,
                backgroundColor: colors.white,
                borderRadius: 15,
                justifyContent: 'center',
                alignItems: 'center',
              },
              placeholder: {
                color: colors.white,
                fontSize: scale(10),
                fontWeight: 'bold',
              },
            }}
            items={[
              {label: 'Football', value: 'football'},
              {label: 'Baseball', value: 'baseball'},
              {label: 'Hockey', value: 'hockey'},
            ]}
            Icon={() => (
              <Icon
                name="down"
                color="#000"
                size={Platform.OS === 'ios' ? 8 : 12}
              />
            )}
          />
        </View>
        <TouchableOpacity
          style={styles.btnDone}
          activeOpacity={0.7}
          onPress={navigationDone}>
          <LinearGradient
            colors={[colors.yellow, colors.yellowDark]}
            style={styles.btnLogin}>
            <Text style={styles.btnLoginText}>DONE</Text>
          </LinearGradient>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: scale(16),
    width: scale(315),
    marginTop: 30,
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',

    paddingLeft: 10,
  },
  inputAndroid: {
    fontSize: scale(16),
    width: scale(315),
    marginTop: 30,
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',

    paddingLeft: 10,
  },
});

const pickerSelectStylesCurrecy = StyleSheet.create({
  inputIOS: {
    fontSize: scale(16),
    width: scale(315),
    marginTop: 10,
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',
    paddingLeft: 10,
  },
  inputAndroid: {
    fontSize: scale(16),
    width: scale(315),
    marginTop: 10,
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',
    paddingLeft: 10,
  },
});

const pickerSelectStylesBuySell = StyleSheet.create({
  inputIOS: {
    fontSize: scale(16),
    width: scale(315),
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',
    paddingLeft: 10,
  },
  inputAndroid: {
    fontSize: scale(16),
    width: scale(315),
    height: scale(45),
    borderRadius: 40,
    borderWidth: 2,
    borderColor: colors.black,
    color: colors.white,
    backgroundColor: 'rgba(33,33,33,0.8)',
    paddingLeft: 10,
  },
});
