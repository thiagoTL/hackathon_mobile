import {StyleSheet, Platform} from 'react-native';
import {heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  headerContent: {
    // ...ifIphoneX(
    //   {
    //     height: hp('35%'),
    //   },
    //   {
    //     height: hp('50%'),
    //   },
    // ),
    height: scale(380),
    width: '100%',
    flex: 1,
    backgroundColor: colors.black,
    borderBottomLeftRadius: 40,
    borderBottomEndRadius: 40,
    borderBottomRightRadius: 40,
    borderBottomStartRadius: 40,
  },
  headerTitle: {
    fontSize: scale(28),
    color: colors.white,
    fontWeight: 'bold',
    marginTop: scale(25),
    marginLeft: scale(9),
  },
  ViewBorderLine: {
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
    marginTop: scale(10),
  },
  textValue: {
    fontSize: scale(35),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: scale(15),
  },
  textValueMoeda: {
    fontSize: scale(12),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  valueDolar: {
    fontSize: scale(14),
    marginTop: scale(30),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  textDolar: {
    fontSize: scale(12),
    color: colors.white,
    alignSelf: 'center',
    fontWeight: 'bold',
  },
  viewExample: {
    flex: 1,
    height: '100%',
    width: '100%',
    backgroundColor: colors.white,
    // borderTopLeftRadius: 10,
  },

  viewContentList: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // margin: 15,
    marginRight: scale(20),
  },
  viewInvestAdd: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: scale(15),
    marginTop: scale(15),
  },
  textViewInvest: {
    fontSize: scale(16),
    fontWeight: '500',
  },
  btnAdd: {
    borderWidth: 1,
    height: scale(20),
    width: scale(20),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: scale(10),
    borderColor: '#52B0E4',
  },
  viewLine: {
    width: '95%',
    borderBottomWidth: 4,
    borderBottomColor: '#539F6D',
    alignSelf: 'center',
    marginTop: scale(4),
  },
  viewActions: {
    height: scale(60),
    // backgroundColor: '#ccc',
    marginTop: scale(10),
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewActionsHeader: {
    flexDirection: 'row',
    marginLeft: scale(10),
  },
  viewActionsImage: {
    height: scale(30),
    width: scale(30),
    borderRadius: 5,
    // backgroundColor: '#222',
    marginRight: scale(5),
    marginTop: scale(4),
  },
  viewActionsText: {
    flexDirection: 'column',
    marginLeft: scale(5),
  },
  textActionsTitle: {
    fontSize: scale(16),
    fontWeight: '500',
  },
  textActionsSunTitle: {
    fontSize: scale(14),
    color: '#ccc',
  },
  viewValues: {
    flexDirection: 'column',
    marginRight: scale(10),
  },
  textValuer: {
    fontSize: scale(16),
    fontWeight: '500',
  },
  textValuePor: {
    fontSize: scale(12),
    alignSelf: 'flex-end',
    marginTop: scale(3),
    // color: '#2ecc71',
  },
  lineBottomView: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    width: '95%',
    alignSelf: 'center',
    marginTop: scale(5),
  },
  viewVirante: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  icon: {
    marginLeft: scale(5),
    marginTop: scale(3),
  },
  btnAddInvestments: {
    width: scale(30),
    height: scale(25),
    backgroundColor: colors.yellowDark,
    marginTop: scale(30),
    borderRadius: 5,
    // ...ifIphoneX(
    //   {
    //     marginLeft: '30%',
    //   },
    //   {
    //     marginLeft: Platform.OS === 'ios' ? '20%' : 150,
    //   },
    // ),
    position: 'absolute',
    top: 5,
    left: scale(315),
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerContentT: {
    flexDirection: 'row',
    alignItems: 'center',
  },
});

export default styles;
