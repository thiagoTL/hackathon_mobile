import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconF from 'react-native-vector-icons/AntDesign';
import {useNavigation} from '@react-navigation/native';
// import {PieChart} from 'react-native-svg-charts';
import {Text as Examplo} from 'react-native-svg';
import {scale} from 'react-native-size-matters';

import bitcoin from '../../../../assets/images/bitcoinIcon.png';
import ethereum from '../../../../assets/images/ethe.png';
import bitcoincash from '../../../../assets/images/bitcoincash.png';
import ripple from '../../../../assets/images/ripple.png';
import litecoin from '../../../../assets/images/litecoin.png';
// import BarChart from '../../../../components/CarouselGraphics/BarChart';
// import PieChart from '../../../../components/CarouselGraphics/PieChart';

import styles from './styles';

Icon.loadFont();
IconF.loadFont();

export default function DashboardWallet() {
  const navigation = useNavigation();

  const graph = [
    {
      id: 1,
      valor: '$ 7,265.50',
      description: 'in USD * US dollar',
      valorMoeda: '$10,201.31',
      text: 'total in CAD equivalent',
      graph: false,
    },
    {
      id: 2,
      valor: '$ 7,265.50',
      description: 'in USD * US dollar',
      valorMoeda: '$10,201.31',
      text: 'total in CAD equivalent',
      graph: false,
    },
    {
      id: 3,
      valor: '$ 7,265.50',
      description: 'in USD * US dollar',
      valorMoeda: '$10,201.31',
      text: 'total in CAD equivalent',
      graph: false,
    },
  ];
  const data = [
    {
      id: 1,
      name: 'Bitcoin',
      subTitle: 'RTC',
      value: '5750,70',
      porc: '7,12',
      status: false,
      image: bitcoin,
    },
    {
      id: 2,
      name: 'Ethereum',
      subTitle: 'ETH',
      value: '315,86',
      porc: '4,05',
      status: true,
      image: ethereum,
    },
    {
      id: 3,
      name: 'Bitcoin Cash',
      subTitle: 'BCH',
      value: '1181,54',
      porc: '21,12',
      status: true,
      image: bitcoincash,
    },
    {
      id: 4,
      name: 'Ripple',
      subTitle: 'XRP',
      value: '0,20034',
      porc: '14,92',
      status: false,
      image: ripple,
    },
    {
      id: 5,
      name: 'Litecoin',
      subTitle: 'LTC',
      value: '61,13',
      porc: '1,69',
      status: true,
      image: litecoin,
    },
  ];

  const dataGraph = [
    {
      key: 1,
      amount: 50,
      svg: {fill: '#600080'},
    },
    {
      key: 2,
      amount: 50,
      svg: {fill: '#9900cc'},
    },
    {
      key: 3,
      amount: 40,
      svg: {fill: '#c61aff'},
    },
  ];

  function handleNavigateInvest(name) {
    navigation.navigate('DashboardResultInvest', name);
  }

  function handleAddInvestments() {
    navigation.navigate('Home');
  }

  const Labels = ({slices, height, width}) => {
    return slices.map((slice, index) => {
      const {labelCentroid, pieCentroid, data} = slice;
      return (
        <Examplo
          key={index}
          x={pieCentroid[0]}
          y={pieCentroid[1]}
          fill={'white'}
          textAnchor={'middle'}
          alignmentBaseline={'middle'}
          fontSize={24}
          stroke={'black'}
          strokeWidth={0.2}>
          {data.amount}
        </Examplo>
      );
    });
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.headerContent}>
        <View style={styles.headerContentT}>
          <Text style={styles.headerTitle}>My Investments</Text>
          <TouchableOpacity
            style={styles.btnAddInvestments}
            onPress={handleAddInvestments}
            activeOpacity={0.7}>
            <Icon name="add" color="#000" size={20} />
          </TouchableOpacity>
        </View>
        <View style={styles.ViewBorderLine} />

        <FlatList
          data={graph}
          horizontal={true}
          renderItem={({item}) => (
            <>
              <View
                style={{
                  flexDirection: 'column',
                  flex: 1,
                  width: scale(350),
                  // ...ifIphoneX(
                  //   {
                  //     width: 380,
                  //   },
                  //   {
                  //     width: Platform.OS === 'ios' ? 330 : 380,
                  //   },
                  // ),

                  alignItems: 'center',
                }}>
                <Text style={styles.textValue}>{item.valor}</Text>
                <Text style={styles.textValueMoeda}>{item.description}</Text>
                <Text style={styles.valueDolar}>{item.valorMoeda}</Text>
                <Text style={styles.textDolar}>{item.text}</Text>
              </View>
            </>
          )}
        />
      </View>

      <View style={styles.viewExample}>
        <View style={styles.viewContentList}>
          <View style={styles.viewInvestAdd}>
            <Text style={styles.textViewInvest}>My Portfolio</Text>
          </View>
          <Text style={{marginTop: 15}}>Edit</Text>
        </View>
        <View style={styles.viewLine} />

        <FlatList
          data={data}
          keyExtractor={(item) => String(item)}
          renderItem={({item}) => (
            <>
              <TouchableOpacity
                onPress={() => handleNavigateInvest(item.name)}
                style={styles.viewActions}
                key={item.id}
                activeOpacity={0.7}>
                <View style={styles.viewActionsHeader}>
                  <Image source={item.image} style={styles.viewActionsImage} />
                  <View style={styles.viewActionsText}>
                    <Text style={styles.textActionsTitle}>{item.name}</Text>
                    <Text style={styles.textActionsSunTitle}>
                      {item.subTitle}
                    </Text>
                  </View>
                </View>

                <View style={styles.viewValues}>
                  <Text style={styles.textValuer}>${item.value}</Text>
                  <View style={styles.viewVirante}>
                    <Text
                      style={[
                        styles.textValuePor,
                        {color: item.status ? 'red' : '#2ecc71'},
                      ]}>
                      {item.porc}%
                    </Text>
                    <IconF
                      name={item.status ? 'caretdown' : 'caretup'}
                      size={10}
                      style={styles.icon}
                      color={item.status ? 'red' : '#2ecc71'}
                    />
                  </View>
                </View>
              </TouchableOpacity>
              <View style={styles.lineBottomView} />
            </>
          )}
        />
      </View>
    </SafeAreaView>
  );
}
