import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Home from './HomeDash';
import Wallet from './Wallet';
import SelectAsset from './SelectAsset';
import DashboardWallet from './DashboardWallet';
import DashboardResultInvest from './DashboardResultInvest';

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator initialRouteName="Home">
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="Wallet"
        component={Wallet}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="SelectAsset"
        component={SelectAsset}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DashboardWallet"
        component={DashboardWallet}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="DashboardResultInvest"
        component={DashboardResultInvest}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
