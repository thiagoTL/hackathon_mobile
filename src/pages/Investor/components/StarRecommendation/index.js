import React from 'react';
import {Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import {colors} from '../../../../constants/colors';
import styles from './styles';

export default function StarRecommendation({filled, profile}) {
  return (
    <View style={[styles.starView]}>
      <Icon
        name={filled === true ? 'star' : 'star-o'}
        size={20}
        color={colors.white}
        style={{marginHorizontal: 6}}
      />
    </View>
  );
}
