import {StyleSheet} from 'react-native';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  starView: {
    height: 28,
    width: 28,
    backgroundColor: colors.yellowDark,
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    marginTop: 20,
  },
});

export default styles;
