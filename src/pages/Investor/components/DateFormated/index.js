import React, {useMemo} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {format} from 'date-fns';
import pt from 'date-fns/locale/pt';

import styles from './styles';

export default function DateFormated({data}) {
  const dataFormated = useMemo(
    () => format(new Date(data), 'yyyy-MM-dd', {locale: pt}),
    [data],
  );
  return (
    <View>
      <Text style={styles.textDate}>{dataFormated}</Text>
    </View>
  );
}
