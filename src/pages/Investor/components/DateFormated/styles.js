import {StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  textDate: {
    fontSize: 14,
    fontWeight: '500',
  },
});

export default styles;
