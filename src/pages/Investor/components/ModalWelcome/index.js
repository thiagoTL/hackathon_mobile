import React from 'react';
import { View, Modal, Text, Image, TouchableOpacity } from 'react-native';

import check from '../../../../assets/images/check.png';
import styles from './styles';

export default function ModalWelcome({ visible, onRequestClose }) {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => onRequestClose()}
      animationType="slide">
      <View style={styles.container}>
        <Image source={check} style={styles.imageCheck} />
        <Text style={styles.textAccount}>Account Verify</Text>
        <TouchableOpacity
          style={styles.btnLogin}
          activeOpacity={0.7}
          onPress={() => onRequestClose()}>
          <Text style={styles.btnLoginText}>Welcome</Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}
