import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styles from './styles';

export default function WarningDocument() {
  const navigation = useNavigation();

  function handleCheckDoc() {
    navigation.navigate('CheckDocument');
  }

  return (
    <View style={styles.container}>
      <Text style={styles.text}>
        To add a new recommendation, confirm your details.
      </Text>

      <TouchableOpacity
        style={styles.btn}
        activeOpacity={0.7}
        onPress={handleCheckDoc}>
        <Text style={styles.btnText}>check documents</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.btnGoBack}
        activeOpacity={0.7}
        onPress={() => navigation.goBack()}>
        <Text style={styles.btnGoBackText}>Go Back</Text>
      </TouchableOpacity>
    </View>
  );
}
