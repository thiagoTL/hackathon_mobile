import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 16,
    color: colors.white,
    fontWeight: '600',
    textAlign: 'center',
  },
  btn: {
    height: 40,
    width: wp('80%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.white,
    marginTop: 50,
  },
  btnText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: '600',
  },

  btnGoBack: {
    height: 40,
    width: wp('80%'),
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: colors.white,
    borderWidth: 1,
    borderColor: colors.white,
    marginTop: 50,
  },
  btnGoBackText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: '600',
  },
});

export default styles;
