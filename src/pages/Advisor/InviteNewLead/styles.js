import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorsClient.green,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  header: {
    width: wp('100%'),
    height: 120,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  gradient: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'space-between',
    // alignItems: 'center',
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginTop: 20,
  },
  title: {
    fontSize: 30,
    color: colors.white,
    fontWeight: '600',
  },
  btnClose: {
    height: 30,
    width: 30,
    backgroundColor: 'rgba(255,255,255,0.9)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: '87%',
  },
  textInvite: {
    fontSize: 30,
    fontWeight: 'bold',
    color: colors.white,
    alignSelf: 'center',
    marginBottom: 40,
  },
  inputView: {
    backgroundColor: colors.white,
    height: 45,
    width: wp('90%'),
    borderRadius: 15,
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingLeft: 8,
    fontSize: 16,
    borderWidth: 1,
    borderColor: colors.black,
  },
  input: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
    color: '#000',
  },
  btnSendInvite: {
    height: 48,
    width: '90%',
    borderRadius: 20,
    backgroundColor: colorsClient.purple,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 140,
  },
  btnSendInviteText: {
    fontSize: 16,
    color: colors.white,
    fontWeight: '600',
  },
});

export default styles;
