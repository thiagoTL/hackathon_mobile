import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  SafeAreaView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconEnty from 'react-native-vector-icons/Entypo';
import IconFO from 'react-native-vector-icons/Fontisto';
import IconAt from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';

Icon.loadFont();
IconEnty.loadFont();
IconFO.loadFont();
IconAt.loadFont();

export default function InviteNewLead() {
  const [country, setCountry] = useState([]);
  const [selectCountry, setSelectCountry] = useState();

  const navigation = useNavigation();

  useEffect(() => {
    async function loadCountry() {
      await axios
        .get('https://restcountries.eu/rest/v2/all')
        .then((res) => {
          let country = res.data.map((item, index) => {
            return { label: item.name, value: item };
          });
          setCountry(country);
        })
        .catch((err) => console.log('ERROR', err));
    }

    loadCountry();
  }, []);

  function handleLeadList() {
    navigation.navigate('LeadsList');
  }

  function handleGoBack() {
    navigation.goBack();
  }

  function handleSendInvite() {
    navigation.navigate('InviteRegisterLead');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <View style={styles.viewHeader}>
              <Text style={styles.title}>Taric,</Text>
              <TouchableOpacity
                style={styles.btnClose}
                activeOpacity={0.7}
                onPress={handleGoBack}>
                <Icon name="close" color={colors.black} size={20} />
              </TouchableOpacity>
            </View>

            <Text style={styles.textInvite}>Invite a new Lead!,</Text>
          </LinearGradient>
        </View>

        <View style={styles.inputView}>
          <Icon
            name="person"
            size={15}
            style={{ marginTop: 3 }}
            color={colors.black}
          />
          <TextInput
            placeholder="first name"
            // value={firstName}
            // onChangeText={setFirstName}
            style={styles.input}
            placeholderTextColor={colors.black}
            // onSubmitEditing={() => {
            //   lastNameRef.current.focus();
            // }}
          />
        </View>
        <View style={styles.inputView}>
          <Icon
            name="person"
            size={15}
            style={{ marginTop: 3 }}
            color={colors.black}
          />
          <TextInput
            placeholder="last name"
            // ref={lastNameRef}
            style={styles.input}
            // value={lastName}
            // onChangeText={setLastName}
            placeholderTextColor={colors.black}
            // onSubmitEditing={() => {
            //   emailRef.current.focus();
            // }}
          />
        </View>
        <View style={styles.inputView}>
          <IconEnty
            name="email"
            size={12}
            style={{ marginTop: 3 }}
            color={colors.black}
          />
          <TextInput
            placeholder="email"
            // ref={emailRef}
            // value={email}
            autoCapitalize="none"
            // onChangeText={setEmail}
            style={styles.input}
            placeholderTextColor={colors.black}
            keyboardType="email-address"
            // onSubmitEditing={() => {
            //   emailConfirmRef.current.focus();
            // }}
          />
        </View>
        <TouchableOpacity
          style={styles.btnSendInvite}
          activeOpacity={0.7}
          onPress={handleSendInvite}>
          <Text style={styles.btnSendInviteText}>SEND INVITE</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
}

const pickerSelectCountry = StyleSheet.create({
  inputIOS: {
    backgroundColor: 'transparent',
    height: 45,
    width: wp('73%'),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    fontSize: 16,
    color: '#000',
  },
  inputAndroid: {
    backgroundColor: 'transparent',
    height: 45,
    width: wp('73%'),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    fontSize: 16,
    color: '#000',
  },
});
