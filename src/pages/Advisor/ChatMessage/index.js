import React, { useState, useEffect, useMemo } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  FlatList,
  KeyboardAvoidingView,
  TextInput,
  SafeAreaView,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { useNavigation, useRoute } from '@react-navigation/native';
import IconA from 'react-native-vector-icons/AntDesign';
import IconIo from 'react-native-vector-icons/Ionicons';
import { useSelector, useDispatch } from 'react-redux';
import socketio from 'socket.io-client';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';
import api from '../../../services/api';
import { ChatMessageRequest } from '../../../store/modules/chatmessage/actions';

Icon.loadFont();
IconA.loadFont();
IconIo.loadFont();

export default function ChatMessage() {
  const socket = useMemo(
    () => socketio('https://app.investexpert.co', { query: { message: id } }),
    [id],
  );

  const [message, setMessage] = useState('');
  const [newMessage, setNewMessage] = useState([]);

  const routes = useRoute();
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const userAuthAdvisor = useSelector((state) => state.auth.user);

  const itemMessage = routes.params.itemMessage;

  const { _id: id } = itemMessage;

  useEffect(() => {
    async function loadMessage() {
      const response = await api.get(`/message/${id}`);

      setNewMessage(response.data);
    }

    loadMessage();
  }, []);

  useEffect(() => {
    socket.on('messages', (messageNew) => {
      setNewMessage([...newMessage, messageNew]);
    });
  }, [socket, newMessage]);

  function handleAddInvite() {
    navigation.navigate('InviteRegisterLead');
  }

  function handleMessage() {
    const idUser = userAuthAdvisor._id;

    dispatch(ChatMessageRequest(message, id, idUser));

    setMessage('');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <TouchableOpacity
              onPress={() => navigation.goBack()}
              activeOpacity={0.7}>
              <IconA
                name="left"
                size={Platform.OS === 'ios' ? 20 : 20}
                style={styles.icon}
              />
            </TouchableOpacity>
            <Text style={styles.chatTitle}>Chat With</Text>
            <TouchableOpacity
              style={styles.btnAddInvestments}
              onPress={handleAddInvite}
              activeOpacity={0.7}>
              <Icon name="add" color={colorsClient.lightGreen} size={20} />
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <View style={styles.chatContent}>
          <View style={styles.chatContentHeader}>
            <View style={styles.viewChatImage}>
              <Image
                style={styles.chatImage}
                source={{ uri: itemMessage.photoSecondary }}
              />
              <Text style={styles.chatTextName}>{itemMessage.subName}</Text>
            </View>
            <Text style={styles.chatTextInfoActive}>Active 52m ago</Text>
          </View>
        </View>
        <FlatList
          style={styles.list}
          data={newMessage}
          keyExtractor={(item) => {
            return item.id;
          }}
          renderItem={({ item }) => (
            <View
              style={[
                styles.item,
                {
                  backgroundColor:
                    item.user === userAuthAdvisor._id
                      ? colorsClient.purple
                      : colors.grayPrimary,
                  borderBottomRightRadius:
                    item.user === userAuthAdvisor._id ? 5 : 30,
                  borderTopLeftRadius:
                    item.user === userAuthAdvisor._id ? 30 : 5,
                  alignSelf:
                    item.user === userAuthAdvisor._id
                      ? 'flex-end'
                      : 'flex-start',
                },
              ]}>
              <View style={styles.balloon}>
                <Text
                  style={[
                    styles.textChat,
                    {
                      color:
                        item.user === userAuthAdvisor._id ? '#fff' : '#000',
                    },
                  ]}>
                  {item.message}
                </Text>
              </View>
            </View>
          )}
        />

        <View style={styles.viewSendInputMessage}>
          <TextInput
            placeholder="Type Message to send"
            style={styles.inputMessage}
            value={message}
            onChangeText={setMessage}
          />
          <TouchableOpacity
            style={styles.btnSend}
            activeOpacity={0.7}
            onPress={handleMessage}>
            <IconIo name="ios-send" color={colors.white} size={20} />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}
