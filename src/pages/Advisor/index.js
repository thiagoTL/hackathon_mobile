import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Bottom from './bottom';
import ChatMessage from './ChatMessage';
import InviteNewLead from './InviteNewLead';
import Leads from './Leads';
import InviteRegisterLead from './InviteRegisterLead';

const Stack = createStackNavigator();

export default function Routes() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Bottom"
        component={Bottom}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="ChatMessageAdvisor"
        component={ChatMessage}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="InviteNewLead"
        component={InviteNewLead}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="LeadsAvisor"
        component={Leads}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="InviteRegisterLead"
        component={InviteRegisterLead}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
}
