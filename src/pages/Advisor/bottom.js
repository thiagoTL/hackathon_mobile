import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconsM from 'react-native-vector-icons/MaterialIcons';
import IconIo from 'react-native-vector-icons/Ionicons';
import IconMat from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeAdvisor from './Home';
import ChatList from './Chat';
import LeadsList from './LeadList';
import Profile from './Profile';

import { colorsClient } from '../../constants/colorsClient';
import { colors } from '../../constants/colors';

IconF.loadFont();
IconsM.loadFont();
IconIo.loadFont();
IconMat.loadFont();

const AdvisorBottom = createBottomTabNavigator();

const iconsClient = {
  HomeAdvisor: {
    lib: IconF,
    name: 'home',
  },
  LeadsList: {
    lib: IconMat,
    name: 'account-box-multiple',
  },
  ChatList: {
    lib: IconsM,
    name: 'chat-bubble',
  },
  Profile: {
    lib: IconF,
    name: 'user-circle',
  },
};

export default function AdvisorBottomNavigator() {
  return (
    <AdvisorBottom.Navigator
      initialRouteName="HomeAdvisor"
      screenOptions={({ route, navigation }) => ({
        tabBarIcon: ({ color, size, focused }) => {
          const { lib: Icon, name } = iconsClient[route.name];
          return <Icon name={name} color={color} size={size} />;
        },
      })}
      tabBarOptions={{
        style: {
          backgroundColor: colors.white,
        },
        activeTintColor: colorsClient.lightGreen,
        inactiveTintColor: colors.grayPrimary,
      }}>
      <AdvisorBottom.Screen
        name="HomeAdvisor"
        component={HomeAdvisor}
        options={{
          title: 'Clients',
        }}
      />
      <AdvisorBottom.Screen
        name="LeadsList"
        component={LeadsList}
        options={{
          title: 'Leads',
        }}
      />
      <AdvisorBottom.Screen
        name="ChatList"
        component={ChatList}
        options={{
          title: 'Chat',
        }}
      />
      <AdvisorBottom.Screen
        name="Profile"
        component={Profile}
        options={{
          title: 'Profile',
        }}
      />
    </AdvisorBottom.Navigator>
  );
}
