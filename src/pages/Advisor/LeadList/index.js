import React, { useState, useEffect } from 'react';
import {
  Text,
  View,
  TextInput,
  TouchableOpacity,
  FlatList,
  SafeAreaView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import IconM from 'react-native-vector-icons/MaterialIcons';
import IconAnt from 'react-native-vector-icons/AntDesign';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';

import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';
import styles from './styles';

Icon.loadFont();
IconM.loadFont();
IconAnt.loadFont();

export default function LeadList() {
  const [date, setDate] = useState(new Date());
  const [dayType, setDayType] = useState('');

  const data = [1, 2, 3];
  const navigation = useNavigation();

  const userAuth = useSelector((state) => state.auth.user);

  useEffect(() => {
    const hour = date.getHours();

    if (hour < 5) {
      setDayType('Good evening');
    } else if (hour < 8) {
      setDayType('Good Morning');
    } else if (hour < 12) {
      setDayType('Good Morning');
    } else if (hour < 18) {
      setDayType('Good afternoon');
    } else {
      setDayType('Good evening');
    }
  }, [date]);

  function handleAddInvite() {
    navigation.navigate('InviteNewLead');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <Text style={styles.title}>{dayType},</Text>
            <Text style={styles.nameTitle}>{userAuth.firstName}!</Text>
          </LinearGradient>
        </View>
        <View style={styles.viewInputSearch}>
          <Icon name="ios-search" size={25} color={colors.black} />
          <TextInput style={styles.inputSearch} placeholder="Search" />
        </View>
        <View style={styles.headerName}>
          <Text style={styles.titleClients}>Leads</Text>
          <TouchableOpacity
            style={styles.btnAddInvestments}
            onPress={handleAddInvite}
            activeOpacity={0.7}>
            <IconM name="add" color={colors.white} size={20} />
          </TouchableOpacity>
        </View>

        <FlatList
          data={data}
          renderItem={({ item }) => (
            <View style={styles.viewList}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[
                  colorsClient.red,
                  colorsClient.orange,
                  colorsClient.yellow,
                ]}
                style={styles.gradientList}>
                <View style={styles.imageProfileList} />
                <View style={styles.contentText}>
                  <Text style={styles.textName}>Martha Brown</Text>
                  <View style={styles.viewPortfolio}>
                    <Text style={styles.textName}>Portfolio: US$ 50MM</Text>
                    <View style={styles.viewIconArrowRight}>
                      <IconAnt name="right" color={colorsClient.orange} />
                    </View>
                  </View>
                  <View style={styles.viewCountry}>
                    <Text style={styles.textCountry}>Canada</Text>
                    <View style={styles.viewIconTag}>
                      <IconAnt
                        name="tag"
                        color={colorsClient.lightGreen}
                        size={10}
                      />
                    </View>
                  </View>
                </View>
              </LinearGradient>
            </View>
          )}
        />
      </View>
    </SafeAreaView>
  );
}
