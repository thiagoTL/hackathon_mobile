import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorsClient.green,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  header: {
    height: 100,
    width: wp('100%'),
  },
  gradient: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 30,
    color: colors.white,
  },
  nameTitle: {
    fontSize: 30,
    color: colors.white,
    fontWeight: 'bold',
  },
  viewInputSearch: {
    height: 35,
    width: wp('90%'),
    backgroundColor: '#ccc',
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 10,
    flexDirection: 'row',
    // justifyContent: 'fle',
    alignItems: 'center',
    paddingLeft: 8,
  },
  inputSearch: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
  },
  titleClients: {
    fontSize: 35,
    fontWeight: 'bold',
    marginLeft: 20,
  },
  viewList: {
    height: 85,
    alignSelf: 'center',
    width: '98%',
    backgroundColor: '#000',
    marginTop: 20,
  },
  gradientList: {
    flex: 1,
    flexDirection: 'row',
  },

  imageProfileList: {
    borderWidth: 2,
    borderColor: '#fff',
    height: 70,
    width: 70,
    marginLeft: 5,
    borderRadius: 10,
    marginTop: 8,
  },
  contentText: {
    flexDirection: 'column',
    marginTop: 10,
    marginLeft: 10,
  },
  textName: {
    fontSize: 16,
    fontWeight: '600',
    color: '#fff',
  },
  viewPortfolio: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '65 %',
  },
  viewIconArrowRight: {
    height: 20,
    width: 20,
    backgroundColor: colors.white,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewCountry: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '65%',
  },
  textCountry: {
    fontSize: 12,
    color: '#fff',
    marginTop: 5,
  },
  viewIconTag: {
    height: 15,
    width: 15,
    backgroundColor: colors.black,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 7,
  },
});

export default styles;
