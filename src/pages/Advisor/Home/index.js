import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TextInput,
  FlatList,
  TouchableOpacity,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import IconAnt from 'react-native-vector-icons/AntDesign';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';
import { modalClose } from '../../../store/modules/codeVerification/actions';
import ModalWelcome from '../../Investor/components/ModalWelcome';

Icon.loadFont();
IconAnt.loadFont();

export default function HomeAdvisor() {
  const [date, setDate] = useState(new Date());
  const [dayType, setDayType] = useState('');

  const data = [1, 2, 3];
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const modalWelcome = useSelector((state) => state.codeVerification.modal);
  const userAuth = useSelector((state) => state.auth.user);

  useEffect(() => {
    const hour = date.getHours();

    if (hour < 5) {
      setDayType('Good evening');
    } else if (hour < 8) {
      setDayType('Good Morning');
    } else if (hour < 12) {
      setDayType('Good Morning');
    } else if (hour < 18) {
      setDayType('Good afternoon');
    } else {
      setDayType('Good evening');
    }
  }, [date]);

  function handleNavigateLead() {
    navigation.navigate('LeadsAvisor');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <Text style={styles.title}>{dayType},</Text>
            <Text style={styles.nameTitle}>{userAuth.firstName}!</Text>
          </LinearGradient>
        </View>
        <View style={styles.viewInputSearch}>
          <Icon name="ios-search" size={25} color={colors.black} />
          <TextInput style={styles.inputSearch} placeholder="Search" />
        </View>

        <Text style={styles.titleClients}>Clients</Text>

        {/* <View style={{height: 100, width: 100}}>
          <Hexagono>
            <View style={{flex: 1, backgroundColor: '#ccc'}} />
            <Text>Hello</Text>
          </Hexagono>
        </View> */}

        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.viewList}
              onPress={handleNavigateLead}
              activeOpacity={0.7}>
              <LinearGradient
                start={{ x: 0, y: 0 }}
                end={{ x: 1, y: 0 }}
                colors={[
                  colorsClient.purple,
                  colorsClient.green,
                  colorsClient.lightGreen,
                  colorsClient.mediumGreen,
                ]}
                style={styles.gradientList}>
                <View style={styles.imageProfileList} />
                <View style={styles.contentText}>
                  <Text style={styles.textName}>Martha Brown</Text>
                  <View style={styles.viewPortfolio}>
                    <Text style={styles.textName}>Portfolio: US$ 500MM</Text>
                    <View style={styles.viewIconArrowRight}>
                      <IconAnt name="right" color={colorsClient.lightGreen} />
                    </View>
                  </View>
                  <View style={styles.viewCountry}>
                    <Text style={styles.textCountry}>Canada</Text>
                    <View style={styles.viewIconTag}>
                      <IconAnt
                        name="tag"
                        color={colorsClient.lightGreen}
                        size={10}
                      />
                    </View>
                  </View>
                </View>
              </LinearGradient>
            </TouchableOpacity>
          )}
        />
        <ModalWelcome
          visible={modalWelcome}
          onRequestClose={() => dispatch(modalClose())}
        />
      </View>
    </SafeAreaView>
  );
}
