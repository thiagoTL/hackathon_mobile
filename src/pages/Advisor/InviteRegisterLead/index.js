import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  StyleSheet,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import IconEnty from 'react-native-vector-icons/Entypo';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconFO from 'react-native-vector-icons/Fontisto';
import IconAt from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import RNPickerSelect from 'react-native-picker-select';

import styles from './styles';
import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

Icon.loadFont();
IconEnty.loadFont();
IconFO.loadFont();
IconAt.loadFont();

export default function InviteRegisterLead() {
  const [name, setName] = useState('Martha');
  const [lastName, setLastName] = useState('Brown');
  const [email, setEmail] = useState('martha.brown@gmail.com');
  const [country, setCountry] = useState([]);
  const [selectCountry, setSelectCountry] = useState();

  const navigation = useNavigation();
  useEffect(() => {
    async function loadCountry() {
      await axios
        .get('https://restcountries.eu/rest/v2/all')
        .then((res) => {
          let country = res.data.map((item, index) => {
            return { label: item.name, value: item };
          });
          setCountry(country);
        })
        .catch((err) => console.log('ERROR', err));
    }

    loadCountry();
  }, []);
  function handleGoBack() {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <View style={styles.viewHeader}>
              <View style={styles.viewImage} />
              <TouchableOpacity
                style={styles.btnClose}
                activeOpacity={0.7}
                onPress={handleGoBack}>
                <Icon name="close" color={colors.black} size={20} />
              </TouchableOpacity>
            </View>
          </LinearGradient>
        </View>
        <ScrollView>
          <View style={styles.inputView}>
            <Icon
              name="person"
              size={15}
              style={{ marginTop: 3 }}
              color={colors.black}
            />
            <TextInput
              placeholder="first name"
              value={name}
              // onChangeText={setFirstName}
              style={styles.input}
              placeholderTextColor={colors.black}
              // onSubmitEditing={() => {
              //   lastNameRef.current.focus();
              // }}
            />
          </View>
          <View style={styles.inputView}>
            <Icon
              name="person"
              size={15}
              style={{ marginTop: 3 }}
              color={colors.black}
            />
            <TextInput
              placeholder="last name"
              // ref={lastNameRef}
              style={styles.input}
              value={lastName}
              // onChangeText={setLastName}
              placeholderTextColor={colors.black}
              // onSubmitEditing={() => {
              //   emailRef.current.focus();
              // }}
            />
          </View>
          <View style={styles.inputView}>
            <IconEnty
              name="email"
              size={12}
              style={{ marginTop: 3 }}
              color={colors.black}
            />
            <TextInput
              placeholder="email"
              // ref={emailRef}
              value={email}
              autoCapitalize="none"
              // onChangeText={setEmail}
              style={styles.input}
              placeholderTextColor={colors.black}
              keyboardType="email-address"
              // onSubmitEditing={() => {
              //   emailConfirmRef.current.focus();
              // }}
            />
          </View>

          <View style={styles.inputView}>
            <IconFO
              name="world-o"
              size={15}
              color={colors.black}
              style={{ marginTop: 3 }}
            />
            <RNPickerSelect
              placeholder={{
                label: 'country',
                value: null,
                color: '#000',
              }}
              onValueChange={(value) => setSelectCountry(value)}
              style={{
                ...pickerSelectCountry,
                iconContainer: { top: 15, marginRight: 10 },
                placeholder: {
                  color: '#000',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              items={country}
              Icon={() => <IconAt name="down" color="#000" size={15} />}
            />
          </View>

          <View style={styles.inputView}>
            <Icon
              name="phone"
              size={15}
              color={colors.black}
              style={{ marginTop: 3 }}
            />

            <TextInput
              placeholder="mobile"
              // ref={phoneRef}
              style={styles.input}
              placeholderTextColor={colors.black}
              // value={phone}
              // onChangeText={setPhone}
              keyboardType="phone-pad"
            />
            <Icon
              name="info"
              color={colors.black}
              size={15}
              style={{ marginRight: 10 }}
            />
          </View>

          <TouchableOpacity
            style={styles.btnSendInvite}
            activeOpacity={1}
            onPress={() => {}}>
            <Text style={styles.btnSendInviteText}>SEND INVITE</Text>
          </TouchableOpacity>

          <View style={styles.viewLineGradient}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1.5, y: 0 }}
              colors={[colorsClient.green, colorsClient.lightGreen]}
              style={styles.gradientLine}
            />
          </View>

          <Text style={styles.textTitlePipeline}>Pipeline Status</Text>
          <View style={styles.inputView}>
            <Icon
              name="info"
              color="#ccc"
              size={20}
              style={{ marginRight: 5 }}
            />
            <RNPickerSelect
              placeholder={{
                label: 'Invitation Sent',
                value: null,
                color: '#ccc',
              }}
              onValueChange={(value) => setSelectCountry(value)}
              style={{
                ...pickerSelectCountry,
                iconContainer: { top: 15, marginRight: 10 },
                placeholder: {
                  color: '#ccc',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              items={country}
              Icon={() => <IconAt name="down" color="#ccc" size={15} />}
            />
          </View>
          <View style={styles.viewLineGradient}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1.5, y: 0 }}
              colors={[colorsClient.green, colorsClient.lightGreen]}
              style={styles.gradientLine}
            />
          </View>

          <Text style={styles.textTitlePipeline}>Proposal Type</Text>

          <View style={styles.inputView}>
            <Icon
              name="info"
              color="#ccc"
              size={20}
              style={{ marginRight: 5 }}
            />
            <RNPickerSelect
              placeholder={{
                label: '- - - - - -',
                value: null,
                color: '#ccc',
              }}
              onValueChange={(value) => setSelectCountry(value)}
              style={{
                ...pickerSelectCountry,
                iconContainer: { top: 15, marginRight: 10 },
                placeholder: {
                  color: '#ccc',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              items={country}
              Icon={() => <IconAt name="down" color="#ccc" size={15} />}
            />
          </View>
          <View style={styles.viewLineGradient}>
            <LinearGradient
              start={{ x: 0, y: 0 }}
              end={{ x: 1.5, y: 0 }}
              colors={[colorsClient.green, colorsClient.lightGreen]}
              style={styles.gradientLine}
            />
          </View>

          <Text style={styles.textTitlePipeline}>Proposal Status</Text>

          <View style={styles.inputView}>
            <Icon
              name="info"
              color="#ccc"
              size={20}
              style={{ marginRight: 5 }}
            />
            <RNPickerSelect
              placeholder={{
                label: '- - - - - -',
                value: null,
                color: '#ccc',
              }}
              onValueChange={(value) => setSelectCountry(value)}
              style={{
                ...pickerSelectCountry,
                iconContainer: { top: 15, marginRight: 10 },
                placeholder: {
                  color: '#ccc',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              items={country}
              Icon={() => <IconAt name="down" color="#ccc" size={15} />}
            />
          </View>
          <View style={styles.inputView}>
            <Icon
              name="info"
              color="#ccc"
              size={20}
              style={{ marginRight: 5 }}
            />
            <RNPickerSelect
              placeholder={{
                label: '- - - - - -',
                value: null,
                color: '#ccc',
              }}
              onValueChange={(value) => setSelectCountry(value)}
              style={{
                ...pickerSelectCountry,
                iconContainer: { top: 15, marginRight: 10 },
                placeholder: {
                  color: '#ccc',
                  fontSize: 12,
                  fontWeight: 'bold',
                },
              }}
              items={country}
              Icon={() => <IconAt name="down" color="#ccc" size={15} />}
            />
          </View>
          <TouchableOpacity
            style={styles.btnSave}
            activeOpacity={1}
            onPress={() => {}}>
            <Text style={styles.btnSaveText}>SAVE</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const pickerSelectCountry = StyleSheet.create({
  inputIOS: {
    backgroundColor: 'transparent',
    height: 45,
    width: wp('85%'),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    fontSize: 16,
    color: '#000',
  },
  inputAndroid: {
    backgroundColor: 'transparent',
    height: 45,
    width: wp('73%'),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 5,
    fontSize: 16,
    color: '#ccc',
    borderWidth: 1,
    borderColor: '#000',
  },
});
