import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorsClient.green,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  header: {
    width: wp('100%'),
    height: 110,
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  gradient: {
    flex: 1,
    flexDirection: 'column',
    // justifyContent: 'space-between',
    // alignItems: 'center',
  },
  viewHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    width: '100%',
    marginTop: 20,
  },
  title: {
    fontSize: 30,
    color: colors.white,
    fontWeight: '600',
  },
  btnClose: {
    height: 30,
    width: 30,
    backgroundColor: 'rgba(255,255,255,0.9)',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    top: 0,
    left: '87%',
  },
  viewImage: {
    height: 70,
    width: 70,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colors.white,
    position: 'absolute',
    top: 15,
  },
  inputView: {
    backgroundColor: colors.white,
    height: 45,
    width: wp('90%'),
    borderRadius: 15,
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    paddingLeft: 8,
    fontSize: 16,
    borderWidth: 1,
    borderColor: colors.black,
  },
  input: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
    color: '#000',
  },
  btnSendInvite: {
    height: 48,
    width: '90%',
    borderRadius: 20,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: 10,
    shadowColor: '#222',
    shadowOffset: {
      height: Platform.OS === 'ios' ? 2 : 10,
      width: Platform.OS === 'ios' ? 2 : 10,
    },
    shadowOpacity: Platform.OS === 'ios' ? 2 : 10,
    shadowRadius: Platform.OS === 'ios' ? 2 : 10,
    elevation: Platform.OS === 'ios' ? 0 : 8,
  },
  btnSendInviteText: {
    fontSize: 16,
    color: colors.white,
    fontWeight: 'bold',
  },
  gradientLine: {
    flex: 1,
    // flexDirection: 'column',
    // borderBottomWidth: 1,
    // width: 100,
    // justifyContent: 'space-between',
    // alignItems: 'center',
  },
  viewLineGradient: {
    borderBottomWidth: 1,
    borderBottomColor: colorsClient.lightGreen,
    width: '70%',
    alignSelf: 'center',
    marginTop: 20,
  },
  textTitlePipeline: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 15,
    marginLeft: 25,
  },

  btnSave: {
    height: 48,
    width: '40%',
    borderRadius: 20,
    backgroundColor: '#ccc',
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'flex-end',
    marginTop: 20,
    shadowColor: '#000',
    shadowOffset: {
      height: Platform.OS === 'ios' ? 2 : 10,
      width: Platform.OS === 'ios' ? 2 : 10,
    },
    shadowOpacity: Platform.OS === 'ios' ? 2 : 10,
    shadowRadius: Platform.OS === 'ios' ? 2 : 10,
    elevation: Platform.OS === 'ios' ? 0 : 8,
    marginBottom: 50,
    marginRight: 20,
  },
  btnSaveText: {
    fontSize: 16,
    color: colors.white,
    fontWeight: 'bold',
  },
});

export default styles;
