import React from 'react';
import { View, StyleSheet } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import Graph from './Graph';

export default function Rainbow() {
  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <Graph onPressGoBack={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
});
