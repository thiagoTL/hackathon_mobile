import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { colors } from '../../../../constants/colors';

Icon.loadFont();

export default function Header({ onPress }) {
  return (
    <View style={styles.headerBit}>
      <View>
        <Text style={styles.textMoeda}>Martha Brown</Text>
        <Text style={styles.textValue}>
          <Text style={styles.textMoeda}>Portifolio: </Text>$6.564,30
        </Text>
      </View>
      <TouchableOpacity
        activeOpacity={0.8}
        style={styles.btnClose}
        {...{ onPress }}>
        <Icon name="close" size={18} color={colors.black} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    ...StyleSheet.absoluteFill,
  },
  headerBit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginLeft: 25,
    marginRight: 15,
  },
  textMoeda: {
    color: colors.white,
    fontSize: Platform.OS === 'ios' ? 22 : 28,
    fontWeight: '600',
    marginTop: 8,
  },
  textValue: {
    color: colors.black,
    fontSize: Platform.OS === 'ios' ? 16 : 20,
    fontWeight: '600',
  },
  btnClose: {
    height: 25,
    width: 25,
    borderRadius: 13,
    backgroundColor: 'rgba(255,255,255,0.9)',
    justifyContent: 'center',
    marginTop: 15,
    alignItems: 'center',
  },
  icon: {
    marginLeft: 5,
    marginTop: 3,
  },
});
