import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';

import { colors } from '../../../../constants/colors';
import { colorsClient } from '../../../../constants/colorsClient';

export default function ListDays({ onPress, label, selected }) {
  return (
    <TouchableOpacity
      {...{ onPress }}
      style={[
        styles.viewDay,
        {
          backgroundColor: selected ? colorsClient.purple : 'transparent',
        },
      ]}>
      <Text
        style={[
          styles.viewDayText,
          { color: selected ? colors.white : colors.blackweak },
        ]}>
        {label}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  viewDayText: {
    fontSize: 12,
    // marginLeft: 20,
    fontWeight: '600',
    color: colors.blackweak,
  },

  viewToday: {
    flexDirection: 'row',
    alignSelf: 'center',
    // marginTop: 25,
  },
  viewDay: {
    height: 25,
    width: 35,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? 5 : 12,
    backgroundColor: 'transparent',
  },
});
