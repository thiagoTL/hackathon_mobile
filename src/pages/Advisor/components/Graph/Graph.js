import React, { useState } from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import * as shape from 'd3-shape';
import Svg, { Path } from 'react-native-svg';
import { scaleLinear } from 'd3-scale';
import Animated, {
  useAnimatedProps,
  useAnimatedStyle,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';
import { TouchableNativeFeedback } from 'react-native-gesture-handler';
import { parse, mixPath, useVector } from 'react-native-redash';
import LinearGradient from 'react-native-linear-gradient';

import { Prices, DataPoints, SIZE } from './Model';
import data from '../../../../services/data.json';
// import Cursor from './Cursor';
import Header from './Header';
import ListDay from './ListDays';
import Footer from './Footer';
import { colors } from '../../../../constants/colors';
import { colorsClient } from '../../../../constants/colorsClient';

const { width } = Dimensions.get('window');
const AnimatedPath = Animated.createAnimatedComponent(Path);

const values = data.data.prices;
const POINTS = 60;

export default function Graph({ onPressGoBack }) {
  const [selectedGraph, setSelectedGraph] = useState('');

  function buildGraph(datapoints, label) {
    const priceList = datapoints.prices.slice(0, POINTS);
    const formattedValues = priceList.map((price) => [
      parseFloat(price[0]),
      price[1],
    ]);
    const prices = formattedValues.map((value) => value[0]);
    const dates = formattedValues.map((value) => value[1]);
    const scaleX = scaleLinear()
      .domain([Math.min(...dates), Math.max(...dates)])
      .range([0, SIZE]);
    const minPrice = Math.min(...prices);
    const maxPrice = Math.max(...prices);
    const scaleY = scaleLinear().domain([minPrice, maxPrice]).range([SIZE, 0]);

    return {
      label,
      minPrice,
      maxPrice,
      parcentChange: datapoints.percent_change,
      path: parse(
        shape
          .line()
          .x(([, x]) => scaleX(x))
          .y(([y]) => scaleY(y))
          .curve(shape.curveBasis)(formattedValues),
      ),
    };
  }

  function handleSelected(item) {
    if (item === 0) {
      console.log('ITEM 0');
    } else if (item === 1) {
      console.log('ITEM');
    }
  }

  const graphs = [
    {
      label: 'Today',
      value: 0,
      data: buildGraph(values.hour, 'Last Hour'),
      selected: false,
    },
    {
      label: '1W',
      value: 1,
      data: buildGraph(values.day, 'Today'),
      selected: false,
    },
    {
      label: '1M',
      value: 2,
      data: buildGraph(values.month, 'Last Month'),
      selected: true,
    },
    {
      label: '3M',
      value: 3,
      data: buildGraph(values.year, 'This Year'),
      selected: false,
    },
    {
      label: '6M',
      value: 4,
      data: buildGraph(values.all, 'All time'),
      selected: false,
    },
    {
      label: '1Y',
      value: 5,
      data: buildGraph(values.all, 'All time'),
      selected: false,
    },
    {
      label: 'ALL',
      value: 6,
      data: buildGraph(values.all, 'All time'),
      selected: false,
    },
  ];

  const SELECTION_WIDTH = width - 32;
  const BUTTON_WIDTH = (width - 32) / graphs.length;

  const translation = useVector();
  const transition = useSharedValue(0);
  const selected = useSharedValue(0);
  const previous = useSharedValue(graphs[0].data);
  const current = useSharedValue(graphs[1].data);
  const animatedProps = useAnimatedProps(() => {
    return {
      d: mixPath(transition.value, previous.value.path, current.value.path),
    };
  });

  const style = useAnimatedStyle(() => ({
    transform: [{ translateX: withTiming(BUTTON_WIDTH * selected.value) }],
  }));

  return (
    <LinearGradient
      style={styles.container}
      start={{ x: 0, y: 0 }}
      end={{ x: 1.5, y: 0 }}
      colors={[colorsClient.green, colorsClient.lightGreen]}>
      <Header onPress={onPressGoBack} />
      <View>
        <Svg width={SIZE} height={SIZE}>
          <AnimatedPath
            animatedProps={animatedProps}
            fill="transparent"
            stroke="white"
            strokeWidth={2}
          />
        </Svg>
      </View>

      <Footer />

      <LinearGradient
        start={{ x: 0, y: 0 }}
        end={{ x: 1, y: 1 }}
        colors={[colorsClient.green, colorsClient.lightGreen]}
        style={styles.viewDaysBack}>
        {graphs.map((item) => {
          return (
            <ListDay
              onPress={() => {
                previous.value = current.value;
                transition.value = 0;
                current.value = item.data;
                transition.value = withTiming(1);
                selected.value = item.value;
              }}
              label={item.label}
              selected={item.selected}
            />
          );
        })}
      </LinearGradient>
    </LinearGradient>
  );
}

const styles = StyleSheet.create({
  viewDaysBack: {
    width: '100%',
    height: 60,
    // flex: 1,
    backgroundColor: colorsClient.lightGreen,
    marginTop: 12,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  container: {
    flex: 1,
    backgroundColor: '#000',
    // height: '90%',
  },
  backgroundSelection: {
    // backgroundColor: '#f3f3f3',
    ...StyleSheet.absoluteFillObject,
    // width: BUTTON_WIDTH,
    // borderRadius: 8,
  },
  selection: {
    flexDirection: 'row',
    // width: SELECTION_WIDTH,
    alignSelf: 'center',
  },
  labelContainer: {
    padding: 10,
    // backgroundColor: 'rgba(0,0,0,0.5)',
    // width: BUTTON_WIDTH,
    borderRadius: 20,
  },
  label: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  btn: {
    height: 20,
    width: 40,
    backgroundColor: '#ccc',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnText: {
    fontSize: 10,
    color: '#000',
    fontWeight: '600',
  },
});
