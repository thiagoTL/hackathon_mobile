import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { scale } from 'react-native-size-matters';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconF from 'react-native-vector-icons/AntDesign';

import { colors } from '../../../../constants/colors';

Icon.loadFont();
IconF.loadFont();

export default function Footer() {
  return (
    <TouchableOpacity style={styles.btnPerc}>
      <Text style={styles.btnPercText}>7.12%</Text>
      <IconF name="caretup" size={10} style={styles.icon} color="#fff" />
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  btnPerc: {
    height: 25,
    width: 70,
    backgroundColor: 'rgb(179, 121, 247)',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 15,
    position: 'absolute',
    bottom: 20,
  },
  btnPercText: {
    color: colors.white,
    fontSize: 12,
    fontWeight: 'bold',
  },
});
