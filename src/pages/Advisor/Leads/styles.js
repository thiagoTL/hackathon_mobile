import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { ifIphoneX } from 'react-native-iphone-x-helper';

import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorsClient.green,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  headerContent: {
    ...ifIphoneX(
      {
        height: hp('35%'),
      },
      {
        height: Platform.OS === 'ios' ? hp('50%') : 500,
      },
    ),
    width: '100%',
    // flex: 1,
    backgroundColor: colors.grayPrimary,
    borderBottomLeftRadius: 40,
    borderBottomEndRadius: 40,
    borderBottomRightRadius: 40,
    borderBottomStartRadius: 40,
  },
  gradient: {
    flex: 1,
  },
  headerBit: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    marginLeft: 25,
    marginRight: 15,
  },
  textMoeda: {
    color: colors.white,
    fontSize: Platform.OS === 'ios' ? 22 : 28,
    fontWeight: '600',
    marginTop: 8,
  },
  textValue: {
    color: colors.black,
    fontSize: Platform.OS === 'ios' ? 16 : 20,
    fontWeight: '600',
  },
  btnClose: {
    height: 25,
    width: 25,
    borderRadius: 13,
    backgroundColor: 'rgba(255,255,255,0.9)',
    justifyContent: 'center',
    marginTop: 15,
    alignItems: 'center',
  },
  icon: {
    marginLeft: 5,
    marginTop: 3,
  },

  btnPerc: {
    height: 25,
    width: 70,
    backgroundColor: colors.purple,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginLeft: 30,
  },
  btnPercText: {
    color: colors.white,
    fontSize: 12,
    fontWeight: 'bold',
  },
  viewToday: {
    flexDirection: 'row',
    alignSelf: 'center',
    // marginTop: 25,
  },
  viewDay: {
    height: 25,
    width: 35,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: Platform.OS === 'ios' ? 5 : 12,
  },
  viewDayText: {
    fontSize: 12,
    // marginLeft: 20,
    fontWeight: '600',
  },
  viewDaysBack: {
    width: '100%',
    height: 60,
    backgroundColor: colorsClient.lightGreen,
    marginTop: 12,
    justifyContent: 'center',
    alignItems: 'center',
  },
  viewUserInfo: {
    flexDirection: 'row',
    marginTop: 70,
    marginLeft: 20,
  },
  imageView: {
    height: Platform.OS === 'ios' ? 50 : 70,
    width: Platform.OS === 'ios' ? 50 : 70,
    borderRadius: 10,
    borderWidth: 1,
    borderColor: colorsClient.green,
    marginTop: 10,
  },
  viewTextInfo: {
    flexDirection: 'column',
    marginLeft: 10,
  },
  textTypeInfo: {
    color: colors.grayPrimary,
    fontWeight: '600',
    fontSize: Platform.OS === 'ios' ? 12 : 14,
  },
  textUserInfo: {
    color: colors.black,
    fontSize: Platform.OS === 'ios' ? 12 : 14,
    fontWeight: '600',
    paddingLeft: 5,
  },
  textTags: {
    color: colors.grayPrimary,
    fontSize: Platform.OS === 'ios' ? 13 : 15,
    fontWeight: '600',
    marginTop: 10,
    marginLeft: 20,
  },
  btnChat: {
    height: 48,
    width: wp('90%'),
    backgroundColor: colorsClient.purple,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    borderRadius: 30,
    marginTop: Platform.OS === 'ios' ? 10 : 20,
    shadowColor: '#222',
    shadowOffset: {
      height: Platform.OS === 'ios' ? 5 : 10,
      width: Platform.OS === 'ios' ? 5 : 10,
    },
    shadowOpacity: Platform.OS === 'ios' ? 2 : 10,
    shadowRadius: Platform.OS === 'ios' ? 3 : 10,
    elevation: Platform.OS === 'ios' ? 0 : 10,
  },
  btnChatText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: 'bold',
  },
  viewLine: {
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
    marginTop: 20,
    width: wp('90%'),
    alignSelf: 'center',
  },
  viewActions: {
    height: 60,
    // backgroundColor: '#ccc',
    marginTop: 10,
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  viewActionsHeader: {
    flexDirection: 'row',
    marginLeft: 10,
  },
  viewActionsImage: {
    height: 30,
    width: 30,
    borderRadius: 5,
    // backgroundColor: '#222',
    marginRight: 5,
    marginTop: 4,
  },
  viewActionsText: {
    flexDirection: 'column',
    marginLeft: 5,
  },
  textActionsTitle: {
    fontSize: 16,
    fontWeight: '500',
  },
  textActionsSunTitle: {
    fontSize: 14,
    color: '#ccc',
  },
  viewValues: {
    flexDirection: 'column',
    marginRight: 10,
  },
  textValuer: {
    fontSize: 16,
    fontWeight: '500',
  },
  textValuePor: {
    fontSize: 12,
    alignSelf: 'flex-end',
    marginTop: 3,
    // color: '#2ecc71',
  },
  lineBottomView: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    width: '95%',
    alignSelf: 'center',
    marginTop: 5,
  },
  viewVirante: {
    flexDirection: 'row',
    alignSelf: 'flex-end',
    alignItems: 'center',
  },
  icons: {
    marginLeft: 5,
    marginTop: 3,
  },
});

export default styles;
