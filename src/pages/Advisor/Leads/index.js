import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Platform,
  FlatList,
  Image,
  SafeAreaView,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconF from 'react-native-vector-icons/AntDesign';
import { useNavigation } from '@react-navigation/native';
import { ScrollView } from 'react-native-gesture-handler';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';
import { Graph } from '../components';

// images
import bitcoin from '../../../assets/images/bitcoinIcon.png';
import ethereum from '../../../assets/images/ethe.png';
import bitcoincash from '../../../assets/images/bitcoincash.png';
import ripple from '../../../assets/images/ripple.png';
import litecoin from '../../../assets/images/litecoin.png';

Icon.loadFont();
IconF.loadFont();

export default function Leads() {
  const navigation = useNavigation();

  const data = [
    { id: 1, semana: 'Today', status: false },
    { id: 2, semana: '1W', status: false },
    { id: 3, semana: '1M', status: true },
    { id: 4, semana: '3M', status: false },
    { id: 5, semana: '6M', status: false },
    { id: 6, semana: '1Y', status: false },
    { id: 7, semana: 'ALL', status: false },
  ];

  const dataValor = [
    {
      id: 1,
      name: 'Bitcoin',
      subTitle: 'RTC',
      value: '5750,70',
      porc: '7,12',
      status: false,
      image: bitcoin,
    },
    {
      id: 2,
      name: 'Ethereum',
      subTitle: 'ETH',
      value: '315,86',
      porc: '4,05',
      status: true,
      image: ethereum,
    },
    {
      id: 3,
      name: 'Bitcoin Cash',
      subTitle: 'BCH',
      value: '1181,54',
      porc: '21,12',
      status: true,
      image: bitcoincash,
    },
    {
      id: 4,
      name: 'Ripple',
      subTitle: 'XRP',
      value: '0,20034',
      porc: '14,92',
      status: false,
      image: ripple,
    },
    {
      id: 5,
      name: 'Litecoin',
      subTitle: 'LTC',
      value: '61,13',
      porc: '1,69',
      status: true,
      image: litecoin,
    },
  ];

  function handleGoBack() {
    navigation.goBack();
  }

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.content}>
          <View style={styles.headerContent}>
            <Graph onPressGoBack={() => {}} />
            {/* <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1.5, y: 0}}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <View style={styles.headerBit}>
              <View>
                <Text style={styles.textMoeda}>Martha Brown</Text>
                <Text style={styles.textValue}>
                  <Text style={styles.textMoeda}>Portifolio: </Text>$6.564,30
                </Text>
              </View>
              <TouchableOpacity
                activeOpacity={0.8}
                style={styles.btnClose}
                onPress={handleGoBack}>
                <Icon name="close" size={18} color={colors.black} />
              </TouchableOpacity>
            </View>
            <View
              style={{
                height: Platform.OS === 'ios' ? 115 : 130,
                width: '100%',
              }}
            />
            <TouchableOpacity style={styles.btnPerc}>
              <Text style={styles.btnPercText}>7.12%</Text>
              <IconF
                name="caretup"
                size={10}
                style={styles.icon}
                color="#fff"
              />
            </TouchableOpacity>
            <LinearGradient
              start={{x: 0, y: 0}}
              end={{x: 1, y: 1}}
              colors={[colorsClient.green, colorsClient.lightGreen]}
              style={styles.viewDaysBack}>
              <View style={styles.viewToday}>
                {data.map((item) => (
                  <View
                    style={[
                      styles.viewDay,
                      {
                        backgroundColor:
                          item.status === true
                            ? colorsClient.purple
                            : 'transparent',
                      },
                    ]}>
                    <Text
                      key={item.id}
                      style={[
                        styles.viewDayText,
                        {
                          color:
                            item.status === true
                              ? colors.white
                              : colors.blackweak,
                        },
                      ]}>
                      {item.semana}
                    </Text>
                  </View>
                ))}
              </View>
            </LinearGradient>
          </LinearGradient> */}
          </View>
          <View style={styles.viewUserInfo}>
            <View style={styles.imageView} />
            <View style={styles.viewTextInfo}>
              <Text style={styles.textTypeInfo}>
                Full Name:
                <Text style={styles.textUserInfo}>Martha Brown</Text>
              </Text>
              <Text style={styles.textTypeInfo}>
                Subscription:
                <Text style={styles.textUserInfo}>Basic</Text>
              </Text>
              <Text style={styles.textTypeInfo}>
                Country:
                <Text style={styles.textUserInfo}>Canada</Text>
              </Text>
              <Text style={styles.textTypeInfo}>
                Email:
                <Text style={styles.textUserInfo}>martha.brown@gmail.com</Text>
              </Text>
              <Text style={styles.textTypeInfo}>
                Mobile:
                <Text style={styles.textUserInfo}>+1(604) 555-55555</Text>
              </Text>
            </View>
          </View>
          <Text style={styles.textTags}>Tags: </Text>
          <TouchableOpacity style={styles.btnChat} activeOpacity={0.6}>
            <Text style={styles.btnChatText}>CHAT WITH ME</Text>
          </TouchableOpacity>

          <View style={styles.viewLine} />

          <FlatList
            data={dataValor}
            keyExtractor={(item) => String(item)}
            showsVerticalScrollIndicator={false}
            renderItem={({ item }) => (
              <>
                <TouchableOpacity
                  onPress={() => {}}
                  style={styles.viewActions}
                  key={item.id}
                  activeOpacity={0.7}>
                  <View style={styles.viewActionsHeader}>
                    <Image
                      source={item.image}
                      style={styles.viewActionsImage}
                    />
                    <View style={styles.viewActionsText}>
                      <Text style={styles.textActionsTitle}>{item.name}</Text>
                      <Text style={styles.textActionsSunTitle}>
                        {item.subTitle}
                      </Text>
                    </View>
                  </View>

                  <View style={styles.viewValues}>
                    <Text style={styles.textValuer}>${item.value}</Text>
                    <View style={styles.viewVirante}>
                      <Text
                        style={[
                          styles.textValuePor,
                          { color: item.status ? 'red' : '#2ecc71' },
                        ]}>
                        {item.porc}%
                      </Text>
                      <IconF
                        name={item.status ? 'caretdown' : 'caretup'}
                        size={10}
                        style={styles.icons}
                        color={item.status ? 'red' : '#2ecc71'}
                      />
                    </View>
                  </View>
                </TouchableOpacity>
                <View style={styles.lineBottomView} />
              </>
            )}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
}
