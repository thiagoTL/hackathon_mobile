import { StyleSheet, Platform } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import { colors } from '../../../constants/colors';
import { colorsClient } from '../../../constants/colorsClient';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colorsClient.green,
  },
  content: {
    flex: 1,
    backgroundColor: colors.white,
  },
  header: {
    height: 95,
    width: wp('100%'),
  },
  gradient: {
    flex: 1,
  },
  viewInputSearch: {
    height: 35,
    width: wp('90%'),
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: 10,
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 10,
    flexDirection: 'row',
    // justifyContent: 'fle',
    alignItems: 'center',
    paddingLeft: 8,
  },
  inputSearch: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
  },
  headerTitle: {
    fontSize: 30,
    color: colors.white,
    fontWeight: 'bold',
    marginLeft: 10,
  },
  viewListChatRoom: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    height: 40,
    marginTop: 10,
    alignItems: 'center',
    paddingBottom: 10,
  },
  viewListChatRoomImage: {
    height: 35,
    width: 35,
    borderRadius: 30,
    backgroundColor: '#ccc',
    marginLeft: 10,
  },
  viewTextListChatRoomName: {
    marginLeft: 10,
    fontWeight: 'bold',
  },
});

export default styles;
