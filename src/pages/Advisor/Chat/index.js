import React, { useEffect } from 'react';
import {
  View,
  Text,
  TextInput,
  FlatList,
  TouchableOpacity,
  SafeAreaView,
  Image,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import { useSelector, useDispatch } from 'react-redux';

import styles from './styles';
import { colorsClient } from '../../../constants/colorsClient';
import { colors } from '../../../constants/colors';

import { ChatRoomListRequest } from '../../../store/modules/chatroom/actions';

Icon.loadFont();

export default function ChatList() {
  const data = [1, 2, 3, 4];

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const userAuth = useSelector((state) => state.auth.user);
  const chatRoom = useSelector((state) => state.chatroom.chatRoom);

  useEffect(() => {
    dispatch(ChatRoomListRequest(userAuth._id));
  }, []);

  function handleMessageChatRoom(itemMessage) {
    navigation.navigate('ChatMessageAdvisor', { itemMessage });
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <View style={styles.header}>
          <LinearGradient
            start={{ x: 0, y: 0 }}
            end={{ x: 1.5, y: 0 }}
            colors={[colorsClient.green, colorsClient.lightGreen]}
            style={styles.gradient}>
            <View style={styles.viewInputSearch}>
              <Icon name="ios-search" size={25} color={colors.black} />
              <TextInput style={styles.inputSearch} placeholder="Search" />
            </View>
            <Text style={styles.headerTitle}>Chats</Text>
          </LinearGradient>
        </View>
        {chatRoom.length > 0 ? (
          <FlatList
            data={chatRoom}
            renderItem={({ item }) => (
              <TouchableOpacity
                style={styles.viewListChatRoom}
                activeOpacity={0.7}
                onPress={() => handleMessageChatRoom(item)}>
                <Image
                  style={styles.viewListChatRoomImage}
                  source={{
                    uri:
                      userAuth._id === item.nameUserAuth
                        ? item.photoPrimary
                        : item.photoSecondary,
                  }}
                />
                <Text style={styles.viewTextListChatRoomName}>
                  {userAuth._id === item.nameUserAuth
                    ? item.name
                    : item.subName}
                </Text>
              </TouchableOpacity>
            )}
          />
        ) : (
          <View />
        )}
      </View>
    </SafeAreaView>
  );
}
