import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 35,
    textAlign: 'center',
    color: colors.white,
    fontWeight: 'bold',
  },
  btnLogin: {
    height: 30,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: '20%',
    position: 'absolute',
    bottom: 0,
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
