import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styles from './styles';

export default function VerificationConfirm() {
  const navigation = useNavigation();

  function handleNavigateProfileAdvisor() {
    navigation.navigate('ProfileComplete');
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Verification Complete</Text>
      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.6}
        onPress={handleNavigateProfileAdvisor}>
        <Text style={styles.btnLoginText}>Done</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
