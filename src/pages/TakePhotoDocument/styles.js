import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  header: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: 'blue',
    width: 200,
    marginLeft: 20,
  },
  viewText: {
    marginTop: 20,
    marginLeft: 20,
    flexDirection: 'column',
  },
  headerTitle: {
    fontSize: 20,
    width: 280,
    color: colors.white,
    fontWeight: 'bold',
  },
  headerSubTitle: {
    fontSize: 15,
    width: 280,
    marginTop: 5,
    color: colors.white,
  },
  viewUpload: {
    marginLeft: 20,
    marginTop: 20,
  },
  viewUploadLabel: {
    fontSize: 16,
    color: colors.white,
    fontWeight: 'bold',
  },
  viewLine: {
    borderBottomColor: colors.white,
    borderBottomWidth: 1,
    width: '90%',
    marginTop: 5,
  },
  btnAddUpload: {
    height: 45,
    width: 45,
    marginTop: 10,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.grayPrimary,
  },
  btnLogin: {
    height: 38,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '12%',
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
