import React, {useRef, useState, useEffect} from 'react';
import {View, SafeAreaView, Text, TouchableOpacity, Image} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

import ImagePicker from 'react-native-image-picker';

import {colors} from '../../constants/colors';
import styles from './styles';

Icon.loadFont();

export default function TakePhotoDocument() {
  const [imageFront, setImageFront] = useState('');
  const [sourceBack, setSourceBack] = useState('');
  const [sourceSelf, setSourceSelf] = useState('');

  const navigation = useNavigation();
  const cameraRef = useRef(null);
  const routes = useRoute();

  useEffect(() => {
    console.log('routes', routes);
    if (routes.params === undefined) {
      console.log('OK');
    } else {
      const imageFront = routes.params.imageUri;
      const sourceBack = routes.params.sourceBack;
      const sourceSelf = routes.params.sourceSelf;

      setImageFront(imageFront);
      setSourceBack(sourceBack);
      setSourceSelf(sourceSelf);
    }
  }, [routes]);

  const options = {
    title: 'Select Avatar',
    customButtons: [{name: 'fb', title: 'Choose Photo from Facebook'}],
    storageOptions: {
      skipBackup: true,
      path: 'images',
    },
  };

  function handleNavigate() {
    navigation.goBack();
  }

  async function handletakePicture() {
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const source = {uri: response.uri};

        navigation.navigate('ConfirmImage', {source});
        // console.log('SROUCE', source);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //   avatarSource: source,
        // });
      }
    });
  }

  async function handletakePictureBack() {
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const sourceBack = {uri: response.uri};

        navigation.navigate('ConfirmImage', {sourceBack});
        // console.log('SROUCE', source);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //   avatarSource: source,
        // });
      }
    });
  }
  async function handletakePictureSelf() {
    ImagePicker.showImagePicker(options, (response) => {
      // console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        const sourceSelf = {uri: response.uri};

        navigation.navigate('ConfirmImage', {sourceSelf});
        // console.log('SROUCE', source);
        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };

        // this.setState({
        //   avatarSource: source,
        // });
      }
    });
  }

  function handleUploadFiles() {
    navigation.navigate('VerificationConfirm');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity activeOpacity={0.7} onPress={handleNavigate}>
          <Icon name="left" color={colors.white} size={20} />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
      <View style={styles.viewText}>
        <Text style={styles.headerTitle}>
          Take a photo of an IDs and confirmations
        </Text>
        <Text style={styles.headerSubTitle}>
          Upload pictures of documents, selfie and confirmations
        </Text>
      </View>

      <View style={styles.viewUpload}>
        <Text style={styles.viewUploadLabel}>Front</Text>
        <View style={styles.viewLine} />
        {imageFront ? (
          <Image source={imageFront} style={styles.btnAddUpload} />
        ) : (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnAddUpload}
            onPress={handletakePicture}>
            <Icon name="plus" size={25} color={colors.black} />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.viewUpload}>
        <Text style={styles.viewUploadLabel}>Back</Text>
        <View style={styles.viewLine} />
        {sourceBack ? (
          <Image source={sourceBack} style={styles.btnAddUpload} />
        ) : (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnAddUpload}
            onPress={handletakePictureBack}>
            <Icon name="plus" size={25} color={colors.black} />
          </TouchableOpacity>
        )}
      </View>
      <View style={styles.viewUpload}>
        <Text style={styles.viewUploadLabel}>Self</Text>
        <View style={styles.viewLine} />
        {sourceSelf ? (
          <Image source={sourceSelf} style={styles.btnAddUpload} />
        ) : (
          <TouchableOpacity
            activeOpacity={0.8}
            style={styles.btnAddUpload}
            onPress={handletakePictureSelf}>
            <Icon name="plus" size={25} color={colors.black} />
          </TouchableOpacity>
        )}
      </View>

      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.6}
        onPress={handleUploadFiles}>
        <Text style={styles.btnLoginText}>Upload files</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
