import React from 'react';
import {View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import {
  Container,
  Title,
  Logo,
  SubTitle,
  InputView,
  Input,
  TextInputLabel,
  ButtonAdicionarComprovante,
  ButtonAddComprovanteText,
  TextInputLabelPhoto,
  ButtonAddPhotoProfile,
  ButtonAddPhotoProfileText,
  ViewProfile,
  ButtonLogin,
  ButtonLoginText,
} from './styles';
import mascote from '../../assets/images/mascote2.png';
import defaultProfile from '../../assets/images/imageDefault.jpg';

export default function RegisterDoc() {
  const navigation = useNavigation();

  function handleRegisterEndereco() {
    navigation.navigate('RegisterEndereco');
  }

  return (
    <Container>
      <Logo source={mascote} />
      <Title>Invest Expert</Title>
      <SubTitle>Cadastro Documentos</SubTitle>

      <TextInputLabel>Documento de Identificação</TextInputLabel>
      <InputView>
        <Input placeholder="CPF" />
      </InputView>

      <ButtonAdicionarComprovante>
        <ButtonAddComprovanteText>
          adicionar comprovante
        </ButtonAddComprovanteText>
      </ButtonAdicionarComprovante>

      <TextInputLabelPhoto>Foto do Perfil</TextInputLabelPhoto>
      <ButtonAddPhotoProfile>
        <ButtonAddPhotoProfileText>
          adicionar foto perfil
        </ButtonAddPhotoProfileText>
      </ButtonAddPhotoProfile>

      <ViewProfile source={defaultProfile} />

      <ButtonLogin onPress={handleRegisterEndereco}>
        <ButtonLoginText>continuar</ButtonLoginText>
      </ButtonLogin>
    </Container>
  );
}
