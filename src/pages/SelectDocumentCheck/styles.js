import {StyleSheet} from 'react-native';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  header: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: 'blue',
    width: 200,
    marginLeft: 20,
  },
  viewText: {
    marginTop: 20,
    marginLeft: 20,
    flexDirection: 'column',
  },
  headerTitle: {
    fontSize: 20,
    width: 200,
    color: colors.white,
    fontWeight: 'bold',
  },
  headerSubTitle: {
    fontSize: 15,
    marginTop: 5,
    color: colors.white,
  },

  viewDocumentSelect: {
    // position: 'absolute',
    // bottom: 0,
    width: '100%',
    marginTop: '50%',
  },
  btnSelectDocument: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: '100%',
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: '5%',
  },
  viewImage: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 20,
  },
  image: {
    height: 30,
    width: 30,
  },
  iconE: {
    marginRight: 20,
  },
  viewLineAdd: {
    borderBottomWidth: 1,
    borderBottomColor: colors.white,
    alignSelf: 'center',
    width: '88%',
    marginTop: 10,
  },
  textImage: {
    color: colors.white,
    fontSize: 14,
    marginLeft: 10,
  },
});

export default styles;
