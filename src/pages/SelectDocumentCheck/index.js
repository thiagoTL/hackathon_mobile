import React from 'react';
import {SafeAreaView, View, TouchableOpacity, Text, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

import {colors} from '../../constants/colors';
import styles from './styles';

import passaport from '../../assets/images/passport.png';
import idimage from '../../assets/images/license.png';
import cnh from '../../assets/images/cnh.png';

Icon.loadFont();

export default function SelectDocumentCheck() {
  const navigation = useNavigation();

  function handleNavigate() {
    navigation.goBack();
  }

  function handleNaviateTakePhoto() {
    navigation.navigate('TakePhotoDocument');
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity activeOpacity={0.7} onPress={handleNavigate}>
          <Icon name="left" color={colors.white} size={20} />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>
      <View style={styles.viewText}>
        <Text style={styles.headerTitle}>Take a photo of an ID document</Text>
        <Text style={styles.headerSubTitle}>Choose a document</Text>
      </View>

      <View style={styles.viewDocumentSelect}>
        <TouchableOpacity style={styles.btnSelectDocument} activeOpacity={0.7}>
          <View style={styles.viewImage}>
            <Image source={passaport} style={styles.image} />
            <Text style={styles.textImage}>Passport</Text>
          </View>
          <Icon
            name="right"
            color={colors.white}
            size={18}
            style={styles.iconE}
          />
        </TouchableOpacity>
        <View style={styles.viewLineAdd} />

        <TouchableOpacity style={styles.btnSelectDocument} activeOpacity={0.7}>
          <View style={styles.viewImage}>
            <Image source={idimage} style={styles.image} />
            <Text style={styles.textImage}>ID</Text>
          </View>
          <Icon
            name="right"
            color={colors.white}
            size={18}
            style={styles.iconE}
          />
        </TouchableOpacity>
        <View style={styles.viewLineAdd} />
        <TouchableOpacity
          style={styles.btnSelectDocument}
          activeOpacity={0.7}
          onPress={handleNaviateTakePhoto}>
          <View style={styles.viewImage}>
            <Image source={idimage} style={styles.image} />
            <Text style={styles.textImage}>Driver's License</Text>
          </View>

          <Icon
            name="right"
            color={colors.white}
            size={18}
            style={styles.iconE}
          />
        </TouchableOpacity>
        <View style={styles.viewLineAdd} />
      </View>
    </SafeAreaView>
  );
}
