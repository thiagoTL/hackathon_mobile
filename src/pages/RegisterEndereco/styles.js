import styled from 'styled-components/native';
import {RectButton} from 'react-native-gesture-handler';

import {colors} from '../../constants/colors';

export const Container = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${colors.black};
`;

export const Logo = styled.Image`
  height: 70px;
  width: 70px;
`;

export const Title = styled.Text`
  color: ${colors.white};
  font-size: 20px;
  font-weight: bold;
  margin-top: 10px;
`;

export const SubTitle = styled.Text`
  color: ${colors.gray};
  font-size: 16px;
  font-weight: bold;
  margin-top: 10px;
`;

export const InputView = styled.View`
  background-color: rgba(255, 255, 255, 0.14);
  height: 40px;
  width: 80%;
  border-radius: 5px;
  margin-top: 5px;
  margin-bottom: 10px;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  padding-left: 8px;
  font-size: 16px;
`;

export const Input = styled.TextInput.attrs({
  placeholderTextColor: colors.white,
})`
  flex: 1;
  width: 100%;
  margin-left: 5px;
  color: ${colors.white};
`;

export const TextInputLabel = styled.Text`
  font-size: 14px;
  color: ${colors.white};
  font-weight: bold;
  margin-top: 30px;
  align-self: flex-start;
  margin-left: 32px;
`;

export const ButtonLogin = styled(RectButton)`
  height: 35px;
  width: 80%;
  background-color: ${colors.white};
  border-radius: 20px;
  justify-content: center;
  align-items: center;
  margin-top: 50px;
  margin-bottom: 20px;
`;

export const ButtonLoginText = styled.Text`
  font-size: 14px;
  color: ${colors.black};
  font-weight: bold;
`;
