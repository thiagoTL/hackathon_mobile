import React from 'react';
import {View, Text} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {showMessage} from 'react-native-flash-message';
import Icon from 'react-native-vector-icons/SimpleLineIcons';

import {
  Container,
  Logo,
  Title,
  InputView,
  Input,
  SubTitle,
  TextInputLabel,
  ButtonLogin,
  ButtonLoginText,
} from './styles';
import mascote from '../../assets/images/mascote2.png';

Icon.loadFont();

export default function RegisterEndereco() {
  const navigation = useNavigation();

  function handleConcluirCadastro() {
    setTimeout(() => {
      showMessage({
        message: 'Cadastro Realizado com Sucesso',
        type: 'success',
      });
      navigation.navigate('Login');
    }, 2000);
  }

  return (
    <Container>
      <Logo source={mascote} />
      <Title>Invest Expert</Title>
      <SubTitle>Cadastro Endereço</SubTitle>

      {/* Cadastro Endereço */}
      <TextInputLabel>Endereço Completo</TextInputLabel>
      <InputView>
        <Input placeholder="Estado" />
      </InputView>
      <InputView>
        <Input placeholder="Cidade" />
      </InputView>
      <InputView>
        <Input placeholder="Rua" />
      </InputView>
      <InputView>
        <Input placeholder="Numero" />
      </InputView>

      <ButtonLogin onPress={handleConcluirCadastro}>
        <ButtonLoginText>concluir cadastro</ButtonLoginText>
      </ButtonLogin>
    </Container>
  );
}
