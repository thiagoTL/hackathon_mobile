import React, {useState, useEffect} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity, Image} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import {colors} from '../../constants/colors';

export default function ConfirmImage() {
  const [imagePicker, setImagePicker] = useState('');

  const navigation = useNavigation();
  const routes = useRoute();

  const imageUri = routes.params.source;
  const sourceBack = routes.params.sourceBack;
  const sourceSelf = routes.params.sourceSelf;

  useEffect(() => {
    if (imageUri) {
      setImagePicker(imageUri);
    } else if (sourceBack) {
      setImagePicker(sourceBack);
    } else if (sourceSelf) {
      setImagePicker(sourceSelf);
    }
  }, [imageUri, sourceSelf, sourceBack]);

  function handleNavigate() {
    navigation.goBack();
  }

  function handleUploadDocument() {
    if (imageUri) {
      navigation.navigate('TakePhotoDocument', {imageUri});
    } else if (sourceBack) {
      navigation.navigate('TakePhotoDocument', {sourceBack});
    } else if (sourceSelf) {
      navigation.navigate('TakePhotoDocument', {sourceSelf});
    }
    // navigation.navigate('TakePhotoDocument', {imageUri});
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity activeOpacity={0.7} onPress={handleNavigate}>
          <Icon name="left" color={colors.white} size={20} />
        </TouchableOpacity>
        <View style={styles.line} />
      </View>

      <View style={styles.viewText}>
        <Text style={styles.headerTitle}>Check the picture</Text>
        <Text style={styles.headerSubTitle}>
          Confirm if the document is clear and all edges are fully visable.
        </Text>
      </View>

      <View style={styles.viewQuadro}>
        <Image source={imagePicker} style={styles.viewImage} />
        <TouchableOpacity activeOpacity={0.7} style={styles.btnRetake}>
          <Text style={styles.btnRetakeText}>Retake the Picture</Text>
        </TouchableOpacity>
      </View>

      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.6}
        onPress={handleUploadDocument}>
        <Text style={styles.btnLoginText}>Upload Documents</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
