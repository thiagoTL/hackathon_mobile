import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
  },
  header: {
    flexDirection: 'row',
    marginLeft: 10,
    marginTop: 10,
    alignItems: 'center',
  },
  line: {
    borderBottomWidth: 1,
    borderBottomColor: 'blue',
    width: 200,
    marginLeft: 20,
  },
  viewText: {
    marginTop: 20,
    marginLeft: 20,
    flexDirection: 'column',
  },
  headerTitle: {
    fontSize: 20,
    width: 280,
    color: colors.white,
    fontWeight: 'bold',
  },
  headerSubTitle: {
    fontSize: 15,
    width: 280,
    marginTop: 5,
    color: colors.white,
  },
  viewQuadro: {
    height: 200,
    width: '87%',
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: '#ccc',
    marginTop: 20,
  },
  viewImage: {
    height: 160,
    width: '100%',
    backgroundColor: '#ccc',
  },
  btnRetake: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 40,
    width: '100%',
  },
  btnRetakeText: {
    color: colors.white,
    fontWeight: 'bold',
    fontSize: 12,
  },
  btnLogin: {
    height: 38,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: '35%',
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
