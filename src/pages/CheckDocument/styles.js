import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../constants/colors';

const styles = StyleSheet.create({
  conatiner: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  title: {
    color: colors.white,
    fontSize: 30,
    fontWeight: 'bold',
  },
  textDesc: {
    fontSize: 14,
    color: colors.white,
    textAlign: 'center',
    width: 240,
    marginTop: '22%',
  },
  btnLogin: {
    height: 38,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '30%',
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
