import React from 'react';
import {View, SafeAreaView, Text, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styles from './styles';

export default function CheckDocument() {
  const navigation = useNavigation();

  function handleNavigateCheckUser() {
    navigation.navigate('CheckInfoUser');
  }

  return (
    <SafeAreaView style={styles.conatiner}>
      <Text style={styles.title}>Check your ID</Text>
      <Text style={styles.textDesc}>
        To access the advisors'full profile and send messages, you need to
        verify your identity
      </Text>
      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.7}
        onPress={handleNavigateCheckUser}>
        <Text style={styles.btnLoginText}>Start</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
