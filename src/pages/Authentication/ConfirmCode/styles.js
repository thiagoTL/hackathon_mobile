import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  imageCheck: {
    height: 80,
    width: 80,
    marginBottom: 20,
  },
  textAccount: {
    fontSize: 40,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
    width: 180,
  },
  btnLogin: {
    height: 40,
    width: wp('70%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 80,
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
