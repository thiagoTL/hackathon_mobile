import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import check from '../../../assets/images/check.png';
import {checkCodeRequest} from '../../../store/modules/codeVerification/actions';

export default function ConfirmCode() {
  const navigation = useNavigation();
  const routes = useRoute();
  const dispatch = useDispatch();

  const {numero, code} = routes.params.obj;

  function handleCheckConfirm() {
    dispatch(checkCodeRequest(numero, code, navigation));
  }

  return (
    <View style={styles.container}>
      <Image source={check} style={styles.imageCheck} />
      <Text style={styles.textAccount}>Account Verify</Text>
      <TouchableOpacity
        style={styles.btnLogin}
        activeOpacity={0.7}
        onPress={handleCheckConfirm}>
        <Text style={styles.btnLoginText}>Welcome</Text>
      </TouchableOpacity>
    </View>
  );
}
