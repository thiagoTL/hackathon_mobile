import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';

import {colors} from '../../../../constants/colors';

Icon.loadFont();

export default function CheckBoxComponent({checked, onPress}) {
  return (
    <>
      <TouchableOpacity
        style={[
          styles.circle,
          {
            backgroundColor: checked ? colors.black : '#fff',
          },
        ]}
        onPress={onPress}
        activeOpacity={0.6}
        hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
        {checked ? <Icon name="check" size={10} color="#fff" /> : <View />}
      </TouchableOpacity>
    </>
  );
}

const styles = StyleSheet.create({
  circle: {
    height: 15,
    width: 15,
    borderRadius: 3,
    borderWidth: 2,
    borderColor: '#ccc',
    alignItems: 'center',
    justifyContent: 'center',
    alignSelf: 'flex-start',
    marginTop: 6,
    // marginHorizontal: 10,
  },
  checkedCircle: {
    width: 14,
    height: 14,
    borderRadius: 3,
    backgroundColor: colors.blue,
  },
});
