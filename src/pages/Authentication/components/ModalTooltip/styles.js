import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  content: {
    height: 60,
    width: 200,
    backgroundColor: 'rgba(255,255,255,0.4)',
    borderRadius: 10,
    padding: 5,
    ...StyleSheet.absoluteFill,
    top: 240,
    left: 150,
    borderWidth: 1,
    borderColor: 'red',
  },
  contentText: {
    fontSize: 12,
    color: '#212121',
    fontWeight: '600',
    textAlign: 'center',
  },
});

export default styles;
