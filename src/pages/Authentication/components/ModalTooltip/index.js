import React from 'react';
import { View, Text, Modal, TouchableOpacity } from 'react-native';

import styles from './styles';

export default function ModalTooltip({ visible, onRequestClose }) {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => onRequestClose()}
      transparent={true}>
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.content}
          onPress={() => onRequestClose()}
          activeOpacity={1}>
          <Text style={styles.contentText}>
            The password must contain letters and numbers, with a maximum of 6
            characters.
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}
