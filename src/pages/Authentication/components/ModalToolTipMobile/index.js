import React from 'react';
import { Modal, Text, View, TouchableOpacity } from 'react-native';

import styles from './styles';

export default function ModalToolTipMobile({ visible, onRequestClose }) {
  return (
    <Modal
      visible={visible}
      onRequestClose={() => onRequestClose()}
      transparent={true}>
      <View style={styles.container}>
        <TouchableOpacity
          style={styles.content}
          onPress={() => onRequestClose()}
          activeOpacity={1}>
          <Text style={styles.contentText}>
            The phone number must be up to 14 characters long.
          </Text>
        </TouchableOpacity>
      </View>
    </Modal>
  );
}
