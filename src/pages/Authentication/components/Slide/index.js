import React, { useState, useRef } from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import Animated, {
  interpolateColor,
  useAnimatedStyle,
  useAnimatedScrollHandler,
  useSharedValue,
  useDerivedValue,
} from 'react-native-reanimated';
import { useNavigation } from '@react-navigation/native';
import { scale } from 'react-native-size-matters';

import Intro01 from '../../../../assets/images/metro.config.png';
import Intro02 from '../../../../assets/images/iphone-intro02.png';
import Intro03 from '../../../../assets/images/iphone-intro03.png';
import Intro04 from '../../../../assets/images/iphone-intro04.png';
import Intro05 from '../../../../assets/images/iphone-intro05.png';
import Slide, { SLIDE_HEIGHT } from './Slide';
import SubSlide from './SubSlide';
import Line from './Line';

const { width } = Dimensions.get('window');

export default function Intro() {
  const [slide, setSlide] = useState([
    {
      id: 1,
      title: 'Welcome to Invest Expert',
      description:
        ' Have you ever imagined being able to consolidate all of your investments in one place ? ',
      newYou: 'now you can!',
      buttonText: 'NEXT',
      image: Intro01,
      backgroundColor: '#000000',
    },
    {
      id: 2,
      title: 'Recommendations',
      description:
        'Have you ever imagined being able to consolidate all of your investments in one place? Now you can do that.',
      newYou: '',
      buttonText: 'NEXT',
      image: Intro02,
      backgroundColor: '#212121',
    },
    {
      id: 3,
      title: 'Private Message',
      description: ' Chat with experts inside our app, privately and securely',
      newYou: '',
      buttonText: 'NEXT',
      image: Intro03,
      backgroundColor: '#cccccc',
    },
    {
      id: 4,
      title: 'Portfolio Management',
      description:
        ' Control your portfolio securely and share only the investment you want with your advisor',
      newYou: '',
      buttonText: 'NEXT',
      image: Intro04,
      backgroundColor: '#aaaaaa',
    },
    {
      id: 5,
      title: 'Badges',
      description:
        ' Have you ever imagined being able to consolidate all of your investments in one plac ? Now you can do that',
      newYou: '',
      buttonText: 'WELCOME',
      image: Intro05,
      backgroundColor: '#eeeeee',
    },
  ]);

  const navigation = useNavigation();
  const scroll = useRef(null);
  const x = useSharedValue(0);
  const onScroll = useAnimatedScrollHandler({
    onScroll: ({ contentOffset }) => {
      x.value = contentOffset.x;
    },
  });
  const slider = useAnimatedStyle(() => ({
    backgroundColor: interpolateColor(
      x.value,
      slide.map((_, i) => i * width),
      slide.map((s) => s.backgroundColor),
    ),
  }));

  const currentIndex = useDerivedValue(() => x.value / width);

  const footerStyle = useAnimatedStyle(() => ({
    transform: [{ translateX: -x.value }],
  }));

  return (
    <View style={styles.container}>
      <Animated.View style={[styles.slider, slider]}>
        <Animated.ScrollView
          ref={scroll}
          horizontal={true}
          snapToInterval={width}
          decelerationRate="fast"
          showsHorizontalScrollIndicator={false}
          bounces={false}
          onScroll={onScroll}
          scrollEventThrottle={16}>
          {slide.map(({ image }, index) => (
            <Slide key={index} {...{ image }} />
          ))}
        </Animated.ScrollView>
      </Animated.View>
      <View style={styles.footer}>
        <View style={styles.footerContent}>
          <View style={styles.pagination}>
            {slide.map((_, index) => (
              // divide(x, width)
              <Line key={index} currentIndex={currentIndex} {...{ index }} />
            ))}
          </View>
          <Animated.View
            style={[
              {
                width: width * slide.length,
                flex: 1,
                flexDirection: 'row',
              },
              footerStyle,
            ]}>
            {slide.map(({ description, title, newYou }, index) => (
              <SubSlide
                key={index}
                onPress={() => {
                  if (scroll.current) {
                    scroll.current
                      ?.getNode()
                      .scrollTo({ x: width * (index + 1), animated: true });
                  }
                  if (index === slide.length - 1) {
                    navigation.navigate('Login');
                  }
                }}
                last={index === slide.length - 1}
                {...{ description, title, newYou }}
              />
            ))}
          </Animated.View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  slider: {
    height: SLIDE_HEIGHT,
  },

  footer: {
    flex: 1,
  },
  footerContent: {
    flex: 1,
    backgroundColor: '#000',
  },
  pagination: {
    height: scale(30),
    position: 'absolute',
    top: scale(125),
    // top: Platform.OS === 'ios' ? 120 : 140,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    width,
  },
});
