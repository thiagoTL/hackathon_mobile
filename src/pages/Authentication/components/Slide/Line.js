import React from 'react';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
} from 'react-native-reanimated';

const Line = ({currentIndex, index}) => {
  const style = useAnimatedStyle(() => {
    const opacity = interpolate(
      currentIndex.value,
      [index - 1, index, index + 1],
      [0.5, 1, 0.5],
      Extrapolate.CLAMP,
    );

    const scale = interpolate(
      currentIndex.value,
      [index - 1, index, index + 1],
      [1, 1.25, 0.5],
      Extrapolate.CLAMP,
    );

    return {
      borderBottomWidth: 3,
      width: 30,
      marginLeft: 7,
      borderBottomColor: '#FFC15F',
      opacity,
      transform: [{scale}],
    };
  });

  return <Animated.View style={style} />;
};

export default Line;
