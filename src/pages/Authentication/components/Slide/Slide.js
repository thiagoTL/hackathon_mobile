import React from 'react';
import {View, Dimensions, StyleSheet, ImageBackground} from 'react-native';

const {width, height} = Dimensions.get('window');

export const SLIDE_HEIGHT = 0.59 * height;

const styles = StyleSheet.create({
  container: {
    width,
  },
  titleContainer: {
    height: height + 60,
    justifyContent: 'center',
    width,
  },
  title: {
    fontSize: 40,
    color: 'white',
    textAlign: 'center',
    lineHeight: 80,
  },
});

const Slide = ({title, image}) => {
  return (
    <View style={styles.container}>
      <ImageBackground style={styles.titleContainer} source={image} />
    </View>
  );
};

export default Slide;
