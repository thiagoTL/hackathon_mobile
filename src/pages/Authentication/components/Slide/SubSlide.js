import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const SubSlide = ({title, description, newYou, last, onPress}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <Text style={styles.description}>{description}</Text>
      <Text style={styles.newYou}>{newYou}</Text>
      <TouchableOpacity style={styles.btn} {...{onPress}} activeOpacity={0.7}>
        <LinearGradient
          colors={[colors.yellowT, colors.yellowDark]}
          style={styles.btnGradient}>
          <Text style={styles.btnText}>{last ? 'WELCOME' : 'NEXT'}</Text>
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    padding: 12,
  },
  title: {
    fontSize: Platform.OS === 'ios' ? 16 : 18,
    color: '#fff',
    fontWeight: 'bold',
    marginTop: Platform.OS === 'ios' ? 5 : 20,
  },
  description: {
    fontSize: Platform.OS === 'ios' ? 13 : 15,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
    marginTop: Platform.OS === 'ios' ? 8 : 15,
    marginBottom: 10,
  },
  newYou: {
    fontSize: Platform.OS === 'ios' ? 13 : 15,
    color: '#fff',
    fontWeight: '600',
    textAlign: 'center',
  },
  btn: {
    width: scale(280),
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: scale(50),
    height: Platform.OS === 'ios' ? 60 : 50,
  },
  btnText: {
    fontSize: 18,
    color: '#fff',
    fontWeight: 'bold',
  },

  btnGradient: {
    flex: 1,
    height: Platform.OS === 'ios' ? 50 : 50,
    width: '100%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
});

export default SubSlide;
