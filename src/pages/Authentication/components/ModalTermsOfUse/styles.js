import { StyleSheet, Dimensions, Platform } from 'react-native';
import { scale, verticalScale } from 'react-native-size-matters';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';

import { colors } from '../../../../constants/colors';

const { height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgba(255,255,255,0.5)',
    flex: 1,
  },
  content: {
    backgroundColor: colors.black,
    // height: height - 20,
    height: '100%',
    width: '100%',
    position: 'absolute',
    bottom: 0,
    marginTop: 20,
    // borderTopLeftRadius: 15,
    // borderTopRightRadius: 15,
  },
  contentHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: '100%',
    marginTop: Platform.OS === 'ios' ? 40 : 20,
  },
  textTitle: {
    fontSize: 16,
    color: colors.white,
    fontWeight: 'bold',
    // textAlign: 'center',
    marginTop: 10,
    marginLeft: 10,
    // alignSelf: 'center',
    marginBottom: 10,
  },
  textSubTitle: {
    fontSize: 10,
    color: colors.white,
    fontWeight: '600',
    marginTop: 10,
    marginLeft: 10,
  },
  contentDescription: {
    marginTop: 10,
    marginLeft: 10,
  },
  contentDescriptionText: {
    fontSize: 14,
    color: colors.white,
    fontWeight: '700',
  },
  contentDescriptionValue: {
    fontSize: 12,
    color: colors.white,
    fontWeight: '500',
    marginTop: 5,
  },
  viewValorAs: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 5,
    width: scale(280),
  },
  viewBollon: {
    height: scale(10),
    width: scale(10),
    borderRadius: scale(5),
    backgroundColor: colors.white,
    marginRight: 10,
  },

  buttonLogin: {
    height: scale(45),
    width: wp('90%'),
    backgroundColor: colors.white,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
    marginBottom: 10,
    alignSelf: 'center',
  },
  buttonLoginText: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
