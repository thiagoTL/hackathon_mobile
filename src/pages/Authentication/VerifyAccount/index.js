import React from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch} from 'react-redux';

import {resendCodeRequest} from '../../../store/modules/signUp/actions';
import styles from './styles';

export default function VerifyAccount() {
  const navigation = useNavigation();
  const routes = useRoute();
  const dispatch = useDispatch();

  const numero = routes.params.numero;

  function handleSendNumero() {
    dispatch(resendCodeRequest(numero, navigation));
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Verify your account</Text>
      <Text style={styles.subTitle}>Check your SMS for the code</Text>
      <TouchableOpacity
        style={styles.btnLogin}
        onPress={handleSendNumero}
        activeOpacity={0.7}>
        <Text style={styles.btnLoginText}>send again</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
}
