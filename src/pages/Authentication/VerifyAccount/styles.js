import {StyleSheet, Platform} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 35,
    textAlign: 'center',
    color: colors.white,
    fontWeight: 'bold',
    maxWidth: 200,
  },
  subTitle: {
    fontSize: 18,
    color: colors.white,
    fontWeight: '600',
    marginTop: 30,
    maxWidth: 160,
    textAlign: 'center',
  },
  btnLogin: {
    height: 45,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    alignSelf: 'center',
    marginBottom: '20%',
    marginTop: 50,
    // position: 'absolute',
    // bottom: 0,
  },
  btnLoginText: {
    fontSize: 14,
    color: colors.black,
    fontWeight: 'bold',
  },
});

export default styles;
