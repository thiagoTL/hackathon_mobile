import React, { useState, useRef } from 'react';
import {
  View,
  Text,
  ActivityIndicator,
  TouchableOpacity,
  SafeAreaView,
  Image,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import Icons from 'react-native-vector-icons/MaterialIcons';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';

// actions
import { signInRequest } from '../../../store/modules/auth/actions';
import styles from './styles';
import { colors } from '../../../constants/colors';
import mascote from '../../../assets/images/mascote2.png';
// import IntroApp from '../../components/IntroApp';

Icon.loadFont();
Icons.loadFont();

export default function Login() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.auth.loading);

  const passwordRef = useRef(null);

  function handleRegister() {
    navigation.navigate('SelectProfileRegister');
  }

  async function handleHome() {
    dispatch(signInRequest(email, password));
  }

  return (
    // <>
    //   {firstLogin === false ? (
    //     <Slide firstLogin={(value) => setFirstLogin(value)} />
    //   ) : (
    <SafeAreaView style={styles.container}>
      <Image style={styles.logo} source={mascote} />
      <Text style={styles.title}>Invest Expert</Text>
      <View style={styles.headerInput}>
        <Text style={styles.label}>login</Text>
        <View style={styles.inputView}>
          <Icon
            name="email"
            size={12}
            style={styles.icon}
            color={colors.white}
          />
          <TextInput
            style={styles.input}
            placeholder="email"
            placeholderTextColor={colors.white}
            value={email}
            onChangeText={setEmail}
            autoCapitalize="none"
            returnKeyType="next"
            returnKeyLabel="Go"
            onSubmitEditing={() => passwordRef.current.focus()}
          />
        </View>
        <Text style={styles.label}>senha</Text>

        <View style={styles.inputView}>
          <Icons
            name="lock"
            size={15}
            style={styles.icon}
            color={colors.white}
          />
          <TextInput
            ref={passwordRef}
            style={styles.input}
            placeholder="********"
            placeholderTextColor={colors.white}
            secureTextEntry={true}
            value={password}
            onChangeText={setPassword}
            autoCapitalize="none"
            onSubmitEditing={handleHome}
          />
        </View>
      </View>
      <TouchableOpacity
        activeOpacity={0.7}
        style={styles.textForgotPass}
        onPress={() => navigation.navigate('ForgotPassword')}>
        <Text style={styles.textForgot}>forgot my password</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.buttonLogin}
        activeOpacity={0.7}
        onPress={handleHome}>
        <LinearGradient
          colors={[colors.yellowT, colors.yellowDark]}
          style={styles.btnGradient}>
          {loading ? (
            <ActivityIndicator size="small" color={colors.white} />
          ) : (
            <Text style={styles.buttonLoginText}>login</Text>
          )}
        </LinearGradient>
      </TouchableOpacity>

      <View style={styles.viewRegister}>
        <Text style={styles.textOu}>Don't have an account?</Text>
        <Text style={styles.textRegister} onPress={handleRegister}>
          Sign Up
        </Text>
      </View>
    </SafeAreaView>
    //   )}
    // </>
  );
}
