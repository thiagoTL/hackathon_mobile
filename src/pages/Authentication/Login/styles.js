import { StyleSheet } from 'react-native';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import { scale } from 'react-native-size-matters';

import { colors } from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  logo: {
    height: scale(120),
    width: scale(120),
  },
  title: {
    color: colors.white,
    fontSize: scale(30),
    fontWeight: 'bold',
  },
  inputView: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: wp('80%'),
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
    fontSize: 16,
  },
  input: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
    color: colors.white,
  },
  label: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  textForgotPass: {
    alignSelf: 'flex-end',
    marginRight: 45,
  },
  textForgot: {
    color: colors.white,
    fontSize: scale(12),
    fontWeight: '600',
  },
  buttonLogin: {
    height: scale(45),
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  buttonLoginText: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  viewRegister: {
    flexDirection: 'column',
    marginTop: 10,
    alignItems: 'center',
  },
  textOu: {
    color: colors.white,
    fontSize: scale(14),
    marginRight: 5,
  },
  textRegister: {
    color: colors.white,
    fontSize: scale(14),
    fontWeight: 'bold',
  },
  icon: {
    marginTop: 3,
  },
  headerInput: {
    flexDirection: 'column',
    marginTop: 20,
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default styles;
