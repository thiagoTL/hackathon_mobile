import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import Login from './Login';
import Register from './Register';
import RegisterSelectType from './RegisterSelectType';
import CheckEmail from './CheckEmail';
import ConfirmCode from './ConfirmCode';
import SelectProfileRegister from './SelectProfileRegister';
import IntroApp from './components/Slide';
import ForgotPassword from './Forgot/ForgotPassword';
import CheckEmailForgot from './Forgot/CheckEmail';
import VerifyAccount from './VerifyAccount';

const AuthenticationStack = createStackNavigator();

export default function Authentication() {
  return (
    <AuthenticationStack.Navigator>
      <AuthenticationStack.Screen
        name="IntroApp"
        component={IntroApp}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="Login"
        component={Login}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="ForgotPassword"
        component={ForgotPassword}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="CheckEmailForgot"
        component={CheckEmailForgot}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="SelectProfileRegister"
        component={SelectProfileRegister}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="RegisterSelectType"
        component={RegisterSelectType}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="Register"
        component={Register}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="CheckEmail"
        component={CheckEmail}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="ConfirmCode"
        component={ConfirmCode}
        options={{ headerShown: false }}
      />
      <AuthenticationStack.Screen
        name="VerifyAccount"
        component={VerifyAccount}
        options={{ headerShown: false }}
      />
    </AuthenticationStack.Navigator>
  );
}
