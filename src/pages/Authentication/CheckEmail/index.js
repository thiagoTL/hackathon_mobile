import React, {useState, useRef} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';

import styles from './styles';
import {checkCodeRequest} from '../../../store/modules/codeVerification/actions';
import {colors} from '../../../constants/colors';

export default function CheckEmail() {
  const [numCod, setNumCod] = useState('');
  const [numCodTwo, setNumCodTwo] = useState('');
  const [numCodThree, setNumCodThree] = useState('');
  const [numCodFour, setNumCodFour] = useState('');
  const [numCodFive, setNumCodFive] = useState('');
  const [numCodSix, setNumCodFix] = useState('');

  const loading = useSelector((state) => state.codeVerification.loading);
  const navigation = useNavigation();
  const routes = useRoute();
  const dispatch = useDispatch();

  const numero = routes.params.numero;
  // refs
  const numCodRef = useRef(null);
  const numCodTwoRef = useRef(null);
  const numCodThreeRef = useRef(null);
  const numCodFourRef = useRef(null);
  const numCodFiveRef = useRef(null);
  const numCodSixRef = useRef(null);

  function handleConfirm() {
    const checkCode =
      numCod + numCodTwo + numCodThree + numCodFour + numCodFive + numCodSix;

    dispatch(checkCodeRequest(numero, checkCode, navigation));
  }

  function handleResetSms() {
    navigation.navigate('VerifyAccount', {numero});
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={{flexDirection: 'row'}}>
        <TextInput
          style={styles.viewInputCode}
          value={numCod}
          maxLength={1}
          onChangeText={(value) => {
            setNumCod(value);
            value ? numCodTwoRef.current.focus() : null;
          }}
          keyboardType="numeric"
        />
        <TextInput
          ref={numCodTwoRef}
          style={styles.viewInputCode}
          value={numCodTwo}
          maxLength={1}
          onChangeText={(value) => {
            setNumCodTwo(value);
            value ? numCodThreeRef.current.focus() : null;
          }}
          keyboardType="numeric"
        />
        <TextInput
          ref={numCodThreeRef}
          style={styles.viewInputCode}
          value={numCodThree}
          maxLength={1}
          onChangeText={(value) => {
            setNumCodThree(value);
            value ? numCodFourRef.current.focus() : null;
          }}
          keyboardType="numeric"
        />
        <TextInput
          ref={numCodFourRef}
          style={styles.viewInputCode}
          value={numCodFour}
          maxLength={1}
          onChangeText={(value) => {
            setNumCodFour(value);
            value ? numCodFiveRef.current.focus() : null;
          }}
          keyboardType="numeric"
        />
        <TextInput
          ref={numCodFiveRef}
          style={styles.viewInputCode}
          value={numCodFive}
          maxLength={1}
          onChangeText={(value) => {
            setNumCodFive(value);
            value ? numCodSixRef.current.focus() : null;
          }}
          keyboardType="numeric"
        />
        <TextInput
          ref={numCodSixRef}
          style={styles.viewInputCode}
          value={numCodSix}
          maxLength={1}
          onChangeText={(value) => {
            setNumCodFix(value);
          }}
          keyboardType="numeric"
        />
      </View>

      <Text style={styles.textInputCode}>Input the code </Text>
      <Text style={[styles.textInputCode, {fontSize: 12}]}>Checked email</Text>
      <TouchableOpacity
        style={styles.buttonConfim}
        onPress={handleConfirm}
        activeOpacity={0.7}>
        {loading ? (
          <ActivityIndicator size="small" color={colors.yellowDark} />
        ) : (
          <Text style={styles.buttonConfimText}>Send again</Text>
        )}
      </TouchableOpacity>

      <Text style={styles.textDidNot} onPress={handleResetSms}>
        Didn't receive the code ?
      </Text>
    </SafeAreaView>
  );
}
