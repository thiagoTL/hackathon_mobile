import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {ifIphoneX} from 'react-native-iphone-x-helper';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  viewInputCode: {
    height: hp('11%'),
    width: wp('12%'),
    backgroundColor: '#ccc',
    borderRadius: 2,
    marginLeft: 8,
    fontSize: 60,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 2,
    borderColor: colors.white,
  },
  buttonConfim: {
    height: 40,
    width: wp('80%'),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '35%',
  },
  buttonConfimText: {
    fontWeight: 'bold',
    fontSize: 14,
    color: colors.black,
  },
  textInputCode: {
    fontSize: 20,
    color: colors.white,
    fontWeight: '600',
    marginTop: 30,
  },
  textDidNot: {
    fontSize: 20,
    color: colors.white,
    fontWeight: '600',
    marginTop: 90,
    bottom: 0,
    ...ifIphoneX(
      {
        marginBottom: 100,
      },
      {marginBottom: 50},
    ),
    position: 'absolute',
    maxWidth: 150,
    width: 200,
    textAlign: 'center',
  },
  btnSendCode: {
    // position: 'absolute',
    maxWidth: 150,
    width: 200,
    textAlign: 'center',
    bottom: 0,
    ...ifIphoneX(
      {
        marginBottom: 100,
      },
      {marginBottom: 50},
    ),
  },
});

export default styles;
