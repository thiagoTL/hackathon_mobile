import React, { useRef, useEffect, useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  SafeAreaView,
} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useNavigation, useRoute } from '@react-navigation/native';
import RNPickerSelect from 'react-native-picker-select';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { scale } from 'react-native-size-matters';

//icons
import Icon from 'react-native-vector-icons/Entypo';
import IconMa from 'react-native-vector-icons/MaterialIcons';
import Icons from 'react-native-vector-icons/Ionicons';
import IconF from 'react-native-vector-icons/FontAwesome';
import IconM from 'react-native-vector-icons/MaterialIcons';
import IconFO from 'react-native-vector-icons/Fontisto';
import IconAt from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import { colors } from '../../../constants/colors';
import { signUpRequest } from '../../../store/modules/signUp/actions';

import ModalTermsOfUse from '../components/ModalTermsOfUse';
import CheckComponent from '../components/CheckComponent';
import ModalTooltip from '../components/ModalTooltip';
import ModalToolTipMobile from '../components/ModalToolTipMobile';

Icon.loadFont();
Icons.loadFont();
IconF.loadFont();
IconM.loadFont();
IconFO.loadFont();
IconAt.loadFont();

export default function Register() {
  const [selectDay, setSelectDay] = useState('');
  const [country, setCountry] = useState([]);
  const [year, setYear] = useState([]);
  const [selectCountry, setSelectCountry] = useState();
  const [selectMonth, setSelectMonth] = useState('');
  const [selectYear, setSelectYear] = useState('');
  const [days, setDays] = useState([]);
  const [months, setMonths] = useState([]);
  const [check, setCheck] = useState(false);
  const [modalCheck, setModalCheck] = useState(false);
  const [modalToolTip, setModalToolTip] = useState(false);
  const [modalToolTipMobile, setModalToolTipMobile] = useState(false);

  // refs
  const lastNameRef = useRef(null);
  const emailRef = useRef(null);
  const emailConfirmRef = useRef(null);
  const passwordRef = useRef(null);
  const phoneRef = useRef(null);

  const navigation = useNavigation();
  const dispatch = useDispatch();
  const routes = useRoute();

  const loading = useSelector((state) => state.signUp.loading);

  const userType = routes.params.typeUser;

  // FORMIK - VALIDAÇÕES
  const FormSchema = Yup.object().shape({
    firstName: Yup.string().required('Required'),
    lastName: Yup.string().required('Required'),
    email: Yup.string().email('Invalid email').required('Required'),
    confirmEmail: Yup.string()
      .oneOf([Yup.ref('email'), null], 'The Email must match')
      .required('Required'),
    password: Yup.string()
      .min(6)
      .max(15)
      .required('Required')
      .matches(
        /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/,
        'Pelo menos uma letra e um numero',
      ),
    phone: Yup.number().required('Required'),
  });

  useEffect(() => {
    async function loadCountry() {
      await axios
        .get('https://restcountries.eu/rest/v2/all')
        .then((res) => {
          let countryList = res.data.map((item, index) => {
            return { label: item.name, value: item };
          });
          setCountry(countryList);
        })
        .catch((err) => console.log('ERROR', err));
    }

    loadCountry();
    handleGenerateYear();
  }, []);

  async function handleGenerateYear() {
    // years
    let currentYear = new Date().getFullYear();
    let years = [];
    let startYear = 1960;
    for (let i = startYear; i <= currentYear; i++) {
      years.push(startYear++);
    }

    years.sort(function (a, b) {
      return b - a;
    });

    const yearArray = years.map((item, index) => {
      return { label: item.toString(), value: item.toString() };
    });

    setYear(yearArray);

    // days
    let daysFull = [];

    for (let day = 1; day < 32; day++) {
      daysFull.push(day.toString().padStart(2, '0'));
    }

    const daysArray = daysFull.map((item, index) => {
      return { label: item, value: item };
    });

    setDays(daysArray);

    // months
    let monthsFull = [];

    for (let month = 1; month < 13; month++) {
      monthsFull.push(month.toString().padStart(2, '0'));
    }

    const monthsArray = monthsFull.map((item, index) => {
      return { label: item, value: item };
    });

    setMonths(monthsArray);
  }

  function handleSubmit(values) {
    const {
      firstName,
      lastName,
      email,
      confirmEmail,
      password,
      phone,
    } = values;
    const { name, callingCodes } = selectCountry;
    const [codPhone] = callingCodes;
    const birthdate = selectYear + '-' + selectMonth + '-' + selectDay;

    dispatch(
      signUpRequest(
        userType,
        firstName,
        lastName,
        email,
        confirmEmail,
        phone,
        password,
        birthdate,
        name,
        codPhone,
        check,
        navigation,
      ),
    );
  }

  function handleLogin() {
    navigation.navigate('Login');
  }

  function handleChecked() {
    setCheck(!check);
  }

  return (
    <ScrollView style={{ backgroundColor: '#000' }}>
      <SafeAreaView style={styles.container}>
        <Text style={styles.title}>Sign Up</Text>

        <Formik
          initialValues={{
            firstName: '',
            lastName: '',
            email: '',
            confirmEmail: '',
            password: '',
            phone: '',
          }}
          validationSchema={FormSchema}
          onSubmit={(values) => handleSubmit(values)}>
          {({
            handleChange,
            handleBlur,
            handleSubmit,
            values,
            errors,
            touched,
          }) => (
            <>
              <View
                style={[
                  styles.inputView,
                  {
                    borderColor:
                      errors.firstName && touched.firstName ? 'red' : null,
                    borderWidth:
                      errors.firstName && touched.firstName ? 0.5 : 0,
                  },
                ]}>
                <IconM
                  name="person"
                  size={15}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="first name"
                  value={values.firstName}
                  onBlur={handleBlur('firstName')}
                  onChangeText={handleChange('firstName')}
                  style={styles.input}
                  placeholderTextColor={colors.white}
                  onSubmitEditing={() => {
                    lastNameRef.current.focus();
                  }}
                />
              </View>

              <View
                style={[
                  styles.inputView,
                  {
                    borderColor:
                      errors.lastName && touched.lastName ? 'red' : null,
                    borderWidth: errors.lastName && touched.lastName ? 0.5 : 0,
                  },
                ]}>
                <IconM
                  name="person"
                  size={15}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="last name"
                  ref={lastNameRef}
                  style={styles.input}
                  value={values.lastName}
                  onBlur={handleBlur('lastName')}
                  onChangeText={handleChange('lastName')}
                  placeholderTextColor={colors.white}
                  onSubmitEditing={() => {
                    emailRef.current.focus();
                  }}
                />
              </View>
              <View
                style={[
                  styles.inputView,
                  {
                    borderColor: errors.email && touched.email ? 'red' : null,
                    borderWidth: errors.email && touched.email ? 0.5 : 0,
                  },
                ]}>
                <Icon
                  name="email"
                  size={12}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="email"
                  ref={emailRef}
                  autoCapitalize="none"
                  value={values.email}
                  onBlur={handleBlur('email')}
                  onChangeText={handleChange('email')}
                  style={styles.input}
                  placeholderTextColor={colors.white}
                  keyboardType="email-address"
                  onSubmitEditing={() => {
                    emailConfirmRef.current.focus();
                  }}
                />
              </View>
              <View
                style={[
                  styles.inputView,
                  {
                    borderColor:
                      errors.confirmEmail && touched.confirmEmail
                        ? 'red'
                        : null,
                    borderWidth:
                      errors.confirmEmail && touched.confirmEmail ? 0.5 : 0,
                  },
                ]}>
                <Icon
                  name="email"
                  size={12}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="repeat email"
                  ref={emailConfirmRef}
                  autoCapitalize="none"
                  value={values.confirmEmail}
                  onBlur={handleBlur('confirmEmail')}
                  onChangeText={handleChange('confirmEmail')}
                  style={styles.input}
                  placeholderTextColor={colors.white}
                  keyboardType="email-address"
                  onSubmitEditing={() => {
                    passwordRef.current.focus();
                  }}
                />
              </View>
              <View
                style={[
                  styles.inputView,
                  {
                    borderColor:
                      errors.password && touched.password ? 'red' : null,
                    borderWidth: errors.password && touched.password ? 0.5 : 0,
                  },
                ]}>
                <IconMa
                  name="lock"
                  size={15}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="password"
                  ref={passwordRef}
                  style={styles.input}
                  secureTextEntry
                  placeholderTextColor={colors.white}
                  value={values.password}
                  onBlur={handleBlur('password')}
                  onChangeText={handleChange('password')}
                  onSubmitEditing={() => {
                    phoneRef.current.focus();
                  }}
                />
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => setModalToolTip(true)}
                  disabled={errors.password && touched.password ? false : true}>
                  <IconM
                    name="info"
                    color={
                      errors.password && touched.password ? 'red' : colors.white
                    }
                    size={15}
                    style={{ marginRight: 10 }}
                  />
                </TouchableOpacity>
              </View>
              <View
                style={[
                  styles.inputView,
                  {
                    borderColor: errors.phone && touched.phone ? 'red' : null,
                    borderWidth: errors.phone && touched.phone ? 0.5 : 0,
                  },
                ]}>
                <IconM
                  name="phone"
                  size={15}
                  style={{ marginTop: 3 }}
                  color={colors.white}
                />
                <TextInput
                  placeholder="celular"
                  ref={phoneRef}
                  style={styles.input}
                  placeholderTextColor={colors.white}
                  value={values.phone}
                  onBlur={handleBlur('phone')}
                  onChangeText={handleChange('phone')}
                  keyboardType="phone-pad"
                />
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => setModalToolTipMobile(true)}
                  disabled={errors.password && touched.password ? false : true}>
                  <IconM
                    name="info"
                    color={
                      errors.password && touched.password ? 'red' : colors.white
                    }
                    size={15}
                    style={{ marginRight: 10 }}
                  />
                </TouchableOpacity>
              </View>

              <View style={styles.inputView}>
                <IconFO
                  name="world-o"
                  size={15}
                  color={colors.white}
                  style={{ marginTop: 3, marginLeft: scale(15) }}
                />
                <RNPickerSelect
                  placeholder={{
                    label: 'country',
                    value: null,
                    color: '#fff',
                  }}
                  onValueChange={(value) => setSelectCountry(value)}
                  style={{
                    ...pickerSelectCountry,
                    iconContainer: { top: 15, marginRight: scale(15) },
                    placeholder: {
                      color: '#fff',
                      fontSize: scale(12),
                      fontWeight: 'bold',
                    },
                  }}
                  items={country}
                  Icon={() => (
                    <IconAt
                      name="down"
                      color="#fff"
                      size={15}
                      style={{ marginRight: 10 }}
                    />
                  )}
                />
              </View>
              <View style={styles.viewDateRow}>
                <Text style={styles.textLabelDate}>birthday </Text>
                <IconM
                  name="info"
                  color={colors.white}
                  size={15}
                  style={{ marginRight: 15, marginTop: 15 }}
                />
              </View>
              <View style={styles.viewRowDate}>
                {/* <View style={styles.dateView} /> */}
                <View style={styles.viewDay}>
                  <RNPickerSelect
                    placeholder={{
                      label: 'day',
                      value: null,
                      color: '#fff',
                    }}
                    onValueChange={(value) => setSelectDay(value)}
                    style={{
                      ...pickerSelectStyles,
                      iconContainer: { top: 17, marginRight: 10 },
                      placeholder: {
                        color: '#fff',
                        fontSize: scale(12),
                        fontWeight: 'bold',
                      },
                    }}
                    items={days}
                    Icon={() => <IconAt name="down" color="#fff" size={15} />}
                  />
                </View>

                <View style={styles.viewMonth}>
                  <RNPickerSelect
                    placeholder={{
                      label: 'month',
                      value: null,
                      color: '#fff',
                    }}
                    onValueChange={(value) => setSelectMonth(value)}
                    style={{
                      ...pickerSelectStylesMonth,
                      iconContainer: { top: 17, marginRight: 10 },
                      placeholder: {
                        color: '#fff',
                        fontSize: scale(12),
                        fontWeight: 'bold',
                      },
                    }}
                    items={months}
                    Icon={() => <IconAt name="down" color="#fff" size={15} />}
                  />
                </View>

                <View style={styles.viewYear}>
                  <RNPickerSelect
                    placeholder={{
                      label: 'year',
                      value: null,
                      color: '#fff',
                    }}
                    onValueChange={(value) => setSelectYear(value)}
                    style={{
                      ...pickerSelectStyles,
                      iconContainer: { top: 25, marginRight: 10 },
                      placeholder: {
                        color: '#fff',
                        fontSize: scale(12),
                        fontWeight: 'bold',
                      },
                    }}
                    items={year}
                    Icon={() => <IconAt name="down" color="#fff" size={15} />}
                  />
                </View>
              </View>

              <View style={styles.viewTermsOfUse}>
                <CheckComponent
                  onPress={() => handleChecked()}
                  checked={check}
                />
                <Text
                  onPress={() => setModalCheck(true)}
                  style={styles.viewTermsOfUseText}>
                  By signing up you agree to InvestExpert Terms of Service and
                  Privacy Policy
                </Text>
              </View>

              <TouchableOpacity
                style={styles.btnLogin}
                onPress={handleSubmit}
                activeOpacity={0.6}>
                {loading ? (
                  <ActivityIndicator size="small" color={colors.yellowDark} />
                ) : (
                  <Text style={styles.btnLoginText}>create your account</Text>
                )}
              </TouchableOpacity>
            </>
          )}
        </Formik>

        <View style={styles.viewRegister}>
          <Text style={styles.textOu}>I already have an account</Text>
          <Text style={styles.textRegister} onPress={handleLogin}>
            login
          </Text>
        </View>
      </SafeAreaView>
      <ModalTermsOfUse
        visible={modalCheck}
        onRequestClose={() => setModalCheck(false)}
        setCheckTerms={(value) => setCheck(value)}
      />

      <ModalTooltip
        visible={modalToolTip}
        onRequestClose={() => setModalToolTip(false)}
      />

      <ModalToolTipMobile
        visible={modalToolTipMobile}
        onRequestClose={() => setModalToolTipMobile(false)}
      />
    </ScrollView>
  );
}

const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(90),
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 30,
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    color: '#fff',
  },
  inputAndroid: {
    // backgroundColor: 'rgba(255,255,255,0.14)',
    // height: scale(45),
    // width: scale(90),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 8,
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    color: '#fff',
  },
});

const pickerSelectStylesMonth = StyleSheet.create({
  inputIOS: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(90),
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 20,
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    color: '#fff',
  },
  inputAndroid: {
    // backgroundColor: 'rgba(255,255,255,0.14)',
    // height: scale(45),
    // width: scale(90),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 8,
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
    color: '#fff',
  },
});

const pickerSelectCountry = StyleSheet.create({
  inputIOS: {
    backgroundColor: 'transparent',
    height: scale(45),
    width: wp('73%'),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    fontSize: 16,
    color: '#fff',
  },
  inputAndroid: {
    backgroundColor: 'transparent',
    height: scale(45),
    width: scale(300),
    borderRadius: 15,
    // marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 10,
    fontSize: 16,
    color: '#fff',
  },
});
