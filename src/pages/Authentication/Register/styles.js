import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.black,
  },
  inputView: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(300),
    borderRadius: 15,
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
    fontSize: scale(16),
  },
  input: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
    color: '#fff',
  },
  logo: {
    height: scale(50),
    width: scale(50),
  },
  title: {
    color: colors.white,
    fontSize: scale(25),
    fontWeight: 'bold',
  },
  subTitle: {
    color: colors.gray,
    fontSize: scale(16),
    fontWeight: 'bold',
    marginTop: 10,
  },
  dateView: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(20),
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingLeft: 8,
    fontSize: 16,
    marginLeft: 5,
    marginRight: 5,
  },
  viewRowDate: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  textLabelDate: {
    fontSize: scale(14),
    color: colors.white,
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginLeft: 40,
    marginTop: 15,
  },
  viewDateRow: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnLogin: {
    height: scale(40),
    width: scale(250),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
  },
  btnLoginText: {
    fontSize: scale(14),
    color: colors.black,
    fontWeight: 'bold',
  },
  viewRegister: {
    marginTop: scale(15),
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
  },
  textOu: {
    color: colors.white,
    fontSize: scale(14),
    marginRight: 5,
  },
  textRegister: {
    color: colors.white,
    fontSize: scale(14),
    fontWeight: 'bold',
  },
  viewDay: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(90),
    borderRadius: 15,
    marginTop: 10,
    marginRight: 6,
  },
  viewMonth: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(90),
    borderRadius: 15,
    marginTop: 10,
  },
  viewYear: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(90),
    borderRadius: 15,
    marginTop: 10,
    marginLeft: 6,
  },
  viewTermsOfUse: {
    flexDirection: 'row',
    width: scale(250),
    alignItems: 'center',
    marginTop: 15,
    marginLeft: 6,
  },
  viewTermsOfUseText: {
    color: '#fff',
    fontSize: scale(12),
    marginLeft: 8,
  },
});

export default styles;
