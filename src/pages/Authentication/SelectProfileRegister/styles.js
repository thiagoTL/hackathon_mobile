import {StyleSheet} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.black,
    justifyContent: 'space-evenly',
  },
  cardItem: {
    height: scale(120),
    width: scale(120),
    backgroundColor: colors.white,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
    marginTop: 20,
  },
  title: {
    fontSize: scale(30),
    color: '#fff',
    fontWeight: 'bold',
  },
  imageAvatar: {
    height: scale(90),
    width: scale(100),
  },
  btnText: {
    marginTop: 5,
    fontWeight: '500',
  },
  viewCard: {
    flexDirection: 'row',
  },
  viewTextLogin: {
    alignItems: 'center',
  },
  textNormal: {
    fontSize: scale(12),
    color: '#fff',
  },
  textBold: {
    fontSize: scale(12),
    color: '#fff',
    fontWeight: 'bold',
  },
});

export default styles;
