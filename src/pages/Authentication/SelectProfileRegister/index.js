import React from 'react';
import {View, Text, SafeAreaView, Image, TouchableOpacity} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styles from './styles';
import client from '../../../assets/images/client.png';
import Advisor from '../../../assets/images/advisior.png';

export default function SelectProfileRegister() {
  const navigation = useNavigation();

  function handleGoBack() {
    navigation.goBack();
  }

  function handleRegisterContinue(type) {
    navigation.navigate('RegisterSelectType', {type});
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Sign Up</Text>
      <View style={styles.viewCard}>
        <TouchableOpacity
          style={styles.cardItem}
          activeOpacity={0.7}
          onPress={() => handleRegisterContinue('investor')}>
          <Image source={client} style={styles.imageAvatar} />
          <Text style={styles.btnText}>I'm an Investor</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.cardItem}
          activeOpacity={0.7}
          onPress={() => handleRegisterContinue('advisor')}>
          <Image source={Advisor} style={styles.imageAvatar} />
          <Text style={styles.btnText}>I'm an Advisor</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.viewTextLogin}>
        <Text style={styles.textNormal}>I already have an account </Text>
        <Text style={styles.textBold} onPress={handleGoBack}>
          login
        </Text>
      </View>
    </SafeAreaView>
  );
}
