import React, { useState } from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Image,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/Entypo';
import LinearGradient from 'react-native-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import Check from '../../../../assets/images/checkForgot.png';
import { forgotPasswordRequest } from '../../../../store/modules/auth/actions';
import styles from './styles';
import { colors } from '../../../../constants/colors';

Icon.loadFont();

export default function ForgotPassword() {
  const [email, setEmail] = useState('');

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const loading = useSelector((state) => state.auth.loadingForgot);

  function handleCheckEmail() {
    dispatch(forgotPasswordRequest(email, navigation));
  }

  return (
    <View style={styles.container}>
      <Image source={Check} style={styles.imgLogo} />
      <Text style={styles.title}>Reset your password !</Text>
      <View>
        <Text style={styles.label}>email</Text>
        <View style={styles.inputView}>
          <Icon
            name="email"
            size={12}
            style={styles.icon}
            color={colors.white}
          />
          <TextInput
            style={styles.input}
            placeholder="email"
            placeholderTextColor={colors.white}
            value={email}
            onChangeText={setEmail}
            autoCapitalize="none"
          />
        </View>
      </View>

      <TouchableOpacity
        style={styles.buttonLogin}
        activeOpacity={0.7}
        onPress={handleCheckEmail}>
        <LinearGradient
          colors={[colors.yellow, colors.yellowDark]}
          style={styles.btnGradient}>
          {loading ? (
            <ActivityIndicator size="small" color={colors.white} />
          ) : (
            <Text style={styles.buttonLoginText}>Reset Password</Text>
          )}
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}
