import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: scale(35),
    color: colors.white,
    fontWeight: 'bold',
    marginBottom: 50,
    width: scale(250),
    textAlign: 'center',
  },
  inputView: {
    backgroundColor: 'rgba(255,255,255,0.14)',
    height: scale(45),
    width: scale(300),
    borderRadius: 15,
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 8,
    fontSize: scale(16),
  },
  input: {
    flex: 1,
    width: wp('100%'),
    marginLeft: 5,
    color: colors.white,
  },
  label: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  icon: {
    marginTop: 3,
  },
  buttonLogin: {
    height: scale(45),
    width: scale(260),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: scale(50),
  },
  buttonLoginText: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    height: scale(80),
    width: scale(80),
    marginBottom: 50,
  },
});

export default styles;
