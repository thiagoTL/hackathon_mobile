import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import {useNavigation} from '@react-navigation/native';
import Svg, {Text as TextSvg, TSpan} from 'react-native-svg';

import {colors} from '../../../../constants/colors';
import styles from './styles';
import Email from '../../../../assets/images/emailForgot.png';

const EmailCheck = () => {
  return (
    <Svg width={81} height={81} viewBox="0 0 81 81">
      <TextSvg
        transform="translate(-62 -3)"
        fill="#FFA602"
        fillRule="evenodd"
        fontFamily="SFProText-Regular, SF Pro Text"
        fontSize={72}>
        <TSpan x={56} y={69}>
          {'\uDBC0\uDDFD'}
        </TSpan>
      </TextSvg>
    </Svg>
  );
};

export default function CheckEmail() {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Image source={Email} style={styles.imgLogo} />
      {/* <View style={{transform: [{rotate: 40}], backgroundColor: colors.yellowDark}}>
        <EmailCheck />
      </View> */}
      <Text style={styles.title}>Check your inbox email !</Text>
      <TouchableOpacity
        style={styles.buttonLogin}
        activeOpacity={0.7}
        onPress={() => navigation.navigate('Login')}>
        <LinearGradient
          colors={[colors.yellow, colors.yellowDark]}
          style={styles.btnGradient}>
          {/* {loading ? (
                <ActivityIndicator size="small" color={colors.white} />
              ) : ( */}
          <Text style={styles.buttonLoginText}>Done</Text>
          {/* )} */}
        </LinearGradient>
      </TouchableOpacity>
    </View>
  );
}
