import {StyleSheet} from 'react-native';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {scale} from 'react-native-size-matters';

import {colors} from '../../../../constants/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.black,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: scale(35),
    color: colors.white,
    fontWeight: 'bold',
    marginBottom: 50,
    width: scale(250),
    textAlign: 'center',
  },
  buttonLogin: {
    height: scale(45),
    width: scale(280),
    backgroundColor: colors.white,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 50,
  },
  buttonLoginText: {
    fontSize: scale(16),
    color: colors.white,
    fontWeight: 'bold',
  },
  btnGradient: {
    flex: 1,
    width: '100%',
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgLogo: {
    height: scale(80),
    width: scale(80),
    marginBottom: 50,
  },
});

export default styles;
