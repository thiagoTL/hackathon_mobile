import styled from 'styled-components/native';

import {colors} from '../../../constants/colors';

export const Container = styled.SafeAreaView`
  flex: 1;
  justify-content: center;
  align-items: center;
  background-color: ${colors.black};
`;

export const Logo = styled.Image`
  height: 50px;
  width: 50px;
`;

export const Title = styled.Text`
  color: ${colors.white};
  font-size: 25px;
  font-weight: bold;
  margin-bottom: 50px;
`;

export const SubTitle = styled.Text`
  color: ${colors.gray};
  font-size: 16px;
  font-weight: bold;
  margin-top: 10px;
`;

export const ButtonSocialAuth = styled.TouchableOpacity.attrs({
  activeOpacity: 0.4,
})`
  height: 40px;
  width: 80%;
  border-radius: 20px;
  background-color: ${colors.white};
  justify-content: center;
  align-items: center;
  margin-top: 25px;
  flex-direction: row;
`;

export const ButtonSocialAuthText = styled.Text`
  font-size: 14px;
  color: ${colors.black};
  font-weight: bold;
`;

export const ViewRegister = styled.View`
  /* flex-direction: row; */
  /* width: 100px; */
  margin-top: 10px;
  justify-content: center;
  align-items: center;
`;

export const TextOu = styled.Text`
  color: ${colors.white};
  font-size: 14px;
  margin-right: 5px;
`;

export const TextRegister = styled.Text`
  color: ${colors.white};
  font-size: 14px;
  font-weight: bold;
`;
