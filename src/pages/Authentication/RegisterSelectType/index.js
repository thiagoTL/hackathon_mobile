import React from 'react';
import { View, Image, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Foundation';
import { useNavigation, useRoute } from '@react-navigation/native';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-community/google-signin';

import {
  Container,
  Logo,
  Title,
  SubTitle,
  ButtonSocialAuth,
  ButtonSocialAuthText,
  ViewRegister,
  TextOu,
  TextRegister,
} from './styles';

// images
import mascote from '../../../assets/images/mascote2.png';
import googleIcon from '../../../assets/images/google-icon.png';
import facebookIcon from '../../../assets/images/facebook.png';
import linkedin from '../../../assets/images/linkedin.png';
import apple from '../../../assets/images/apple.png';

Icon.loadFont();

export default function RegisterSelectType() {
  const navigation = useNavigation();
  const routes = useRoute();

  const typeUser = routes.params.type;

  GoogleSignin.configure({
    scopes: ['https://www.googleapis.com/auth/drive.readonly'],
  });

  function handleLogin() {
    navigation.navigate('Login');
  }

  function handleRegisterEmail() {
    navigation.navigate('Register', { typeUser });
  }

  async function handleGoogleSignIn() {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();

      Alert.alert('USER => ', userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        console.log('User cancelled the login flow');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        console.log('Operation (e.g sign in) is in progress already');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        console.log('PLAY SERVICES NOT AVAILABLE OR OUTDATED');
      } else {
        console.log('ERROR => ', error);
      }
    }
  }

  return (
    <Container>
      {/* <Logo source={mascote} /> */}
      <Title>Sign Up</Title>
      {/* <SubTitle>Sign </SubTitle> */}

      <ButtonSocialAuth>
        <Image
          source={apple}
          style={{ height: 30, width: 30, marginRight: 37 }}
        />
        <ButtonSocialAuthText>Sign up with Apple</ButtonSocialAuthText>
      </ButtonSocialAuth>
      <ButtonSocialAuth>
        <Image
          source={linkedin}
          style={{ height: 30, width: 30, marginRight: 20 }}
        />
        <ButtonSocialAuthText>Sign up with Linkedln</ButtonSocialAuthText>
      </ButtonSocialAuth>
      {typeUser === 'advisor' ? null : (
        <ButtonSocialAuth onPress={handleGoogleSignIn}>
          <Image
            source={googleIcon}
            style={{ height: 30, width: 30, marginRight: 30 }}
          />
          <ButtonSocialAuthText>Sign up with Google</ButtonSocialAuthText>
        </ButtonSocialAuth>
      )}
      {typeUser === 'advisor' ? null : (
        <ButtonSocialAuth>
          <Image
            source={facebookIcon}
            style={{ height: 30, width: 30, marginRight: 10 }}
          />
          <ButtonSocialAuthText>Sign up with Facebook</ButtonSocialAuthText>
        </ButtonSocialAuth>
      )}

      <ButtonSocialAuth onPress={handleRegisterEmail}>
        <ButtonSocialAuthText>Sign up with Email</ButtonSocialAuthText>
      </ButtonSocialAuth>

      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          maxWidth: 250,
          marginTop: 5,
        }}>
        <ViewRegister>
          <TextOu>I already have an account</TextOu>
          <TextRegister onPress={handleLogin}> login</TextRegister>
        </ViewRegister>
      </View>
    </Container>
  );
}
