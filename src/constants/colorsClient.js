export const colorsClient = {
  purple: '#5748CD',
  blue: '#2999AD',
  lightGreen: '#41E975',
  mediumGreen: '#74C948',
  green: '#18A777',
  red: '#EA5A5F',
  orange: '#DE791E',
  yellow: '#FCCF3A',
};
