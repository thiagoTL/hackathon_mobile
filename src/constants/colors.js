export const colors = {
  black: '#000000',
  blackweak: '#212121',
  yellow: '#FCC15F',
  burntyellow: '#F9A826',
  gray: '#DADFEA',
  white: '#ffffff',
  yellowDark: '#E69423',
  grayPrimary: '#c7c7c7',
  purple: '#6C63FF',
  green: '#56BE19',
  red: '#C7410E',
  yellowT: '#FFC15F',
};
