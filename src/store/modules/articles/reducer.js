import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  articleList: [],
  articleTag: [],
  articleDetails: null,
};

export default function ArticleList(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@article/ARTICLE_LIST_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@article/ARTICLE_LIST_SUCCESS': {
        draft.articleList = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@article/ARTICLE_LIST_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@article/ARTICLE_TAGS_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@article/ARTICLE_TAGS_SUCCESS': {
        draft.articleTag = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@article/ARTICLE_TAGS_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@article/ARTICLE_DETAILS_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@article/ARTICLE_DETAILS_SUCCESS': {
        draft.loading = false;
        draft.articleDetails = action.payload.data;
        break;
      }

      case '@article/ARTICLE_DETAILS_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
        break;
    }
  });
}
