import { call, put, takeLatest, all } from 'redux-saga/effects';

import {
  articleListSuccess,
  articleListFailure,
  articleTagsSuccess,
  articleDetailsSuccess,
} from './actions';
import api from '../../../services/api';

export function* articleList({ payload }) {
  try {
    const response = yield call(api.get, `article/${payload.id}`);

    yield put(articleListSuccess(response.data.article));
  } catch (error) {
    console.log('ERROR', error);
  }
}

export function* articleTags({ payload }) {
  try {
    const response = yield call(
      api.get,
      `articles?tag=${payload.tag.toLowerCase()}`,
    );

    yield put(articleTagsSuccess(response.data.data));
  } catch (error) {
    yield put(articleTagsSuccess());

    console.log('ERROR => ', error);
  }
}

export function* articleDetails({ payload }) {
  try {
    const response = yield call(api.get, `article/show/${payload.id}`);

    yield put(articleDetailsSuccess(response.data.data));
  } catch (error) {
    console.log('ERROR => ', error);
  }
}

export default all([
  takeLatest('@article/ARTICLE_LIST_REQUEST', articleList),
  takeLatest('@article/ARTICLE_TAGS_REQUEST', articleTags),
  takeLatest('@article/ARTICLE_DETAILS_REQUEST', articleDetails),
]);
