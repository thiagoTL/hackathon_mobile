export function articleListRequest(id) {
  return {
    type: '@article/ARTICLE_LIST_REQUEST',
    payload: { id },
  };
}
export function articleListSuccess(data) {
  return {
    type: '@article/ARTICLE_LIST_SUCCESS',
    payload: { data },
  };
}

export function articleListFailure() {
  return {
    type: '@article/ARTICLE_LIST_FAILURE',
  };
}

export function articleTagsRequest(tag) {
  return {
    type: '@article/ARTICLE_TAGS_REQUEST',
    payload: { tag },
  };
}

export function articleTagsSuccess(data) {
  return {
    type: '@article/ARTICLE_TAGS_SUCCESS',
    payload: { data },
  };
}

export function articleTagsFailure() {
  return {
    type: '@article/ARTICLE_TAGS_FAILURE',
  };
}

export function articleDetailsRequest(id) {
  return {
    type: '@article/ARTICLE_DETAILS_REQUEST',
    payload: { id },
  };
}

export function articleDetailsSuccess(data) {
  return {
    type: '@article/ARTICLE_DETAILS_SUCCESS',
    payload: { data },
  };
}

export function articleDetailsFailure() {
  return {
    type: '@article/ARTICLE_DETAILS_FAILURE',
  };
}
