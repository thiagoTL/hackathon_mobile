import { enableES5 } from 'immer';
import { combineReducers } from 'redux';

enableES5();

import auth from './auth/reducer';
import user from './user/reducer';
import articles from './articles/reducer';
import evaluations from './evaluations/reducer';
import signUp from './signUp/reducer';
import chatroom from './chatroom/reducer';
import chatmessage from './chatmessage/reducer';
import codeVerification from './codeVerification/reducer';

export default combineReducers({
  auth,
  user,
  articles,
  evaluations,
  signUp,
  chatroom,
  chatmessage,
  codeVerification,
});
