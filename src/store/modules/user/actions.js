export function UserListRequest() {
  return {
    type: '@auth/USER_REQUEST',
  };
}

export function UserListSuccess(data) {
  return {
    type: '@auth/USER_SUCCESS',
    payload: { data },
  };
}

export function UserListFailure() {
  return {
    type: '@auth/USER_FAILURE',
  };
}

export function UserListIdRequest(id) {
  return {
    type: '@auth/USER_ID_REQUEST',
    payload: { id },
  };
}

export function UserListIdSuccess(data) {
  return {
    type: '@auth/USER_ID_SUCCESS',
    payload: { data },
  };
}

export function UserListIdFailure() {
  return {
    type: '@auth/USER_ID_FAILURE',
  };
}

export function UserUpdateRequest(
  imageUri,
  firstName,
  lastName,
  email,
  mobile,
  oldPassword,
  password,
  navigation,
) {
  return {
    type: '@auth/USER_UPDATE_REQUEST',
    payload: {
      imageUri,
      firstName,
      lastName,
      email,
      mobile,
      oldPassword,
      password,
      navigation,
    },
  };
}

export function UserUpdateSuccess() {
  return {
    type: '@auth/USER_UPDATE_SUCCESS',
  };
}

export function UserUpdateFailure() {
  return {
    type: '@auth/USER_UPDATE_FAILURE',
  };
}
