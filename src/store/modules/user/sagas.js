import { all, takeLatest, call, put } from 'redux-saga/effects';
import { showMessage } from 'react-native-flash-message';
import {
  UserListSuccess,
  UserListFailure,
  UserListIdSuccess,
  UserListIdFailure,
  UserUpdateSuccess,
  UserUpdateFailure,
} from './actions';
import { UserUpdate } from '../auth/actions';
import api from '../../../services/api';

export function* userList() {
  try {
    const response = yield call(api.get, 'advisors');

    const data = response.data.data;

    yield put(UserListSuccess(response.data.data));
  } catch (error) {
    yield put(UserListFailure(error));
  }
}

export function* userListId({ payload }) {
  try {
    const { id } = payload;

    const response = yield call(api.get, `advisors/${id}`);

    yield put(UserListIdSuccess(response.data));
  } catch (error) {
    yield put(UserListIdFailure(error));
  }
}

export function* userUpdate({ payload }) {
  try {
    const {
      imageUri,
      firstName,
      lastName,
      email,
      mobile,
      oldPassword,
      password,
      navigation,
    } = payload;

    const data = new FormData();

    const fileUrl = imageUri;
    const fileName = fileUrl.split('/').pop();

    data.append('file', {
      uri: fileUrl,
      name: fileName,
      type: 'image/jpeg',
    });
    data.append('firstName', firstName);
    data.append('lastName', lastName);
    data.append('email', email);
    data.append('mobile', mobile);
    data.append('oldPassword', oldPassword);
    data.append('password', password);

    const response = yield call(api.put, 'user', data);

    const user = response.data;

    yield put(UserUpdateSuccess());
    yield put(UserUpdate(user));

    showMessage({
      type: 'success',
      message: 'Profile Updated Successfully',
    });

    navigation.goBack();
  } catch (error) {
    yield put(UserUpdateFailure());
    console.log('ERRO', error);
  }
}

export default all([
  takeLatest('@auth/USER_REQUEST', userList),
  takeLatest('@auth/USER_ID_REQUEST', userListId),
  takeLatest('@auth/USER_UPDATE_REQUEST', userUpdate),
]);
