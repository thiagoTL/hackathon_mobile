import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  loadingEdit: false,
  userList: [],
  userListId: [],
  name: '',
  lastName: '',
  stars: 0,
  urlImage: '',
};

export default function user(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@auth/USER_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@auth/USER_SUCCESS': {
        draft.userList = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@auth/USER_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/USER_ID_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@auth/USER_ID_SUCCESS': {
        draft.name = action.payload.data.user.firstName;
        draft.lastName = action.payload.data.user.lastName;
        draft.stars = action.payload.data.avaliacao.rating;
        draft.urlImage = action.payload.data.user.url_image;
        draft.loading = false;
        break;
      }

      case '@auth/USER_ID_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@auth/USER_UPDATE_REQUEST': {
        draft.loadingEdit = true;
        break;
      }

      case '@auth/USER_UPDATE_SUCCESS': {
        draft.loadingEdit = false;
        break;
      }

      case '@auth/USER_UPDATE_FAILURE': {
        draft.loadingEdit = false;
        break;
      }
      default:
        break;
    }
  });
}
