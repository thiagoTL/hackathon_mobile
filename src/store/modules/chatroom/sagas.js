import { all, takeLatest, call, put } from 'redux-saga/effects';

import {
  ChatRoomFailure,
  ChatRoomSuccess,
  ChatRoomListSuccess,
  ChatRoomListFailure,
} from './actions';
import api from '../../../services/api';

export function* chatRoom({ payload }) {
  try {
    const { navigation } = payload;
    const obj = {
      name: payload.name,
      subName: payload.subName,
      photoPrimary: payload.photoPrimary,
      photoSecondary: payload.photoSecondary,
      nameUserAuth: payload.nameChatRoom,
      nameChatRoom: payload.nameUserAuth,
    };

    const response = yield call(api.post, 'chatroom', obj);

    yield put(ChatRoomSuccess(response.data));

    const itens = response.data;

    navigation.navigate('ChatMessage', { itens });
  } catch (error) {
    console.log('ERRO => ', error);
    yield put(ChatRoomFailure(error));
  }
}

export function* chatRoomList({ payload }) {
  try {
    const response = yield call(api.get, `chatroom/${payload.id}`);

    yield put(ChatRoomListSuccess(response.data.data));
  } catch (error) {
    console.log('ERROR', error);
    yield put(ChatRoomListFailure(error));
  }
}

export default all([
  takeLatest('@chatroom/CHAT_ROOM_REQUEST', chatRoom),
  takeLatest('@chatroom/CHAT_ROOM_LIST_REQUEST', chatRoomList),
]);
