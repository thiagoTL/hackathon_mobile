export function ChatRoomRequest(
  name,
  subName,
  photoPrimary,
  photoSecondary,
  nameUserAuth,
  nameChatRoom,
  navigation,
) {
  return {
    type: '@chatroom/CHAT_ROOM_REQUEST',
    payload: {
      name,
      subName,
      photoPrimary,
      photoSecondary,
      nameUserAuth,
      nameChatRoom,
      navigation,
    },
  };
}

export function ChatRoomSuccess(data) {
  return {
    type: '@chatroom/CHAT_ROOM_SUCCESS',
    payload: { data },
  };
}

export function ChatRoomFailure(error) {
  return {
    type: '@chatroom/CHAT_ROOM_FAILURE',
    payload: { error },
  };
}

export function ChatRoomListRequest(id) {
  return {
    type: '@chatroom/CHAT_ROOM_LIST_REQUEST',
    payload: { id },
  };
}

export function ChatRoomListSuccess(data) {
  return {
    type: '@chatroom/CHAT_ROOM_LIST_SUCCESS',
    payload: { data },
  };
}

export function ChatRoomListFailure(error) {
  return {
    type: '@chatroom/CHAT_ROOM_LIST_FAILURE',
    payload: { error },
  };
}
