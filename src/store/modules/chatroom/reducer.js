import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  error: null,
  chatRoom: [],
};

export default function chatRoom(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@chatroom/CHAT_ROOM_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@chatroom/CHAT_ROOM_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@chatroom/CHAT_ROOM_FAILURE': {
        draft.loading = false;
        draft.error = action.payload.error;
        break;
      }

      case '@chatroom/CHAT_ROOM_LIST_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@chatroom/CHAT_ROOM_LIST_SUCCESS': {
        draft.loading = false;
        draft.chatRoom = action.payload.data;
        break;
      }

      case '@chatroom/CHAT_ROOM_LIST_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
        break;
    }
  });
}
