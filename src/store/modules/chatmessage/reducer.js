import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  error: null,
  message: [],
};

export default function chatMessage(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@chatmessage/CHAT_MESSAGE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@chatmessage/CHAT_MESSAGE_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@chatmessage/CHAT_MESSAGE_FAILURE': {
        draft.loading = false;
        draft.error = action.payload.error;
        break;
      }

      default:
        break;
    }
  });
}
