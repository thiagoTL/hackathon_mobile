export function ChatMessageRequest(message, chatRoom, user) {
  return {
    type: '@chatmessage/CHAT_MESSAGE_REQUEST',
    payload: { message, chatRoom, user },
  };
}

export function ChatMessageSuccess(data) {
  return {
    type: '@chatmessage/CHAT_MESSAGE_SUCCESS',
    payload: { data },
  };
}

export function ChatMessageFailure(error) {
  return {
    type: '@chatmessage/CHAT_MESSAGE_FAILURE',
    payload: { error },
  };
}
