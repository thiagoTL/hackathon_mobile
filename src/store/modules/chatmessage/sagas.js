import { all, takeLatest, call, put } from 'redux-saga/effects';

import { ChatMessageFailure, ChatMessageSuccess } from './actions';
import api from '../../../services/api';

export function* chatMessage({ payload }) {
  try {
    const obj = {
      message: payload.message,
      chatroom: payload.chatRoom,
      user: payload.user,
    };

    const response = yield call(api.post, 'message', obj);

    console.log('RESPONSE', response.data);
    yield put(ChatMessageSuccess(response.data));
  } catch (error) {
    yield put(ChatMessageFailure(error));
  }
}

export default all([
  takeLatest('@chatmessage/CHAT_MESSAGE_REQUEST', chatMessage),
]);
