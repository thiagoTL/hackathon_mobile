export function signInRequest(email, password) {
  return {
    type: '@auth/SIGN_IN_REQUEST',
    payload: { email, password },
  };
}

export function signInSuccess(token, client) {
  console.log('CLIENT', client);
  return {
    type: '@auth/SIGN_IN_SUCCESS',
    payload: { token, client },
  };
}

export function signInToken(token) {
  return {
    type: '@auth/SIGN_IN_TOKEN',
    payload: { token },
  };
}

export function signInFailure() {
  return {
    type: '@auth/SIGN_IN_FAILURE',
  };
}

export function signInOut() {
  return {
    type: '@auth/SIGN_OUT',
  };
}

export function forgotPasswordRequest(email, navigation) {
  return {
    type: '@auth/FORGOT_PASSWORD_REQUEST',
    payload: { email, navigation },
  };
}

export function forgotPasswordSuccess() {
  return {
    type: '@auth/FORGOT_PASSWORD_SUCCESS',
  };
}

export function forgotPasswordFailure() {
  return {
    type: '@auth/FORGOT_PASSWORD_FAILURE',
  };
}

export function UserUpdate(data) {
  return {
    type: '@auth/USER_UPDATE',
    payload: { data },
  };
}
