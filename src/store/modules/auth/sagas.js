import { all, takeLatest, call, put } from 'redux-saga/effects';
import { showMessage } from 'react-native-flash-message';

import { signInSuccess, signInFailure, forgotPasswordSuccess } from './actions';
import api from '../../../services/api';

export function* signIn({ payload }) {
  try {
    const { email, password } = payload;

    const obj = {
      email: email,
      password: password,
    };

    console.log('OBJ => ', obj);
    const response = yield call(api.post, 'login', obj);

    console.log('RES =< ', response);

    const { token, user } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(signInSuccess(token, user));
  } catch (error) {
    console.log('ERROR -, ', error);
    showMessage({
      type: 'danger',
      message: 'Wrong email or password, please check.',
    });
    yield put(signInFailure(error));
  }
}

export function* forgotPassword({ payload }) {
  try {
    const data = {
      email: payload.email,
    };

    const response = yield call(api.post, 'forgot', data);

    showMessage({
      type: 'success',
      message: response.data.message,
    });

    payload.navigation.navigate('CheckEmailForgot');

    yield put(forgotPasswordSuccess());
  } catch (error) {
    console.log('ERRO => ', error);
  }
}

export function setToken({ payload }) {
  if (!payload) {
    return;
  }

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@auth/SIGN_IN_REQUEST', signIn),
  takeLatest('@auth/FORGOT_PASSWORD_REQUEST', forgotPassword),
]);
