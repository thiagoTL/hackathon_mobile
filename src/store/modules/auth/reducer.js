import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  loadingForgot: false,
  signed: false,
  user: null,
  token: null,
};

export default function auth(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@auth/SIGN_IN_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@auth/SIGN_IN_SUCCESS': {
        draft.user = action.payload.client;
        draft.token = action.payload.token;
        draft.loading = false;
        draft.signed = true;
        break;
      }

      case '@auth/SIGN_IN_TOKEN': {
        draft.token = action.payload.token;
        break;
      }
      case '@auth/SIGN_IN_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@auth/SIGN_OUT': {
        draft.token = null;
        draft.signed = false;
        break;
      }

      case '@auth/FORGOT_PASSWORD_REQUEST': {
        draft.loadingForgot = true;
        break;
      }

      case '@auth/FORGOT_PASSWORD_SUCCESS': {
        draft.loadingForgot = false;
        break;
      }

      case '@auth/FORGOT_PASSWORD_FAILURE': {
        draft.loadingForgot = false;
        break;
      }

      case '@auth/USER_UPDATE': {
        draft.user = action.payload.data;
      }

      default:
        draft.loading = false;
        break;
    }
  });
}
