import { all, takeLatest, call, put } from 'redux-saga/effects';

import {
  sendCodeMobileSuccess,
  sendCodeMobileFailure,
  checkCodeSuccess,
  checkCodeFailure,
  sendEmailSuccess,
  sendEmailFailure,
} from './actions';
import { signInSuccess } from '../auth/actions';
import api from '../../../services/api';

export function* sendCode({ payload }) {
  const headersParams = {
    'Content-Type': 'application/json',
  };

  try {
    const { mobile } = payload;

    const obj = {
      mobile: mobile,
    };

    const response = yield call(api.post, 'code/resend', obj, {
      headers: headersParams,
    });

    yield put(sendCodeMobileSuccess(response.data));
  } catch (error) {
    yield put(sendCodeMobileFailure(error));
    console.log('ERROR', error);
  }
}

export function* sendEmail({ payload }) {
  try {
    const { email } = payload;

    const obj = {
      email: email,
    };

    const response = yield call(api.post, 'send', obj);

    yield put(sendEmailSuccess(response.data));
  } catch (error) {
    console.log('ERRIR', error);
    //   yield put(resendCodeEmailFailure());
  }
}

export function* checkCodeMobile({ payload }) {
  try {
    const navigation = payload.navigation;

    const objeto = {
      code: payload.code,
      phone: payload.phone,
    };

    const response = yield call(api.post, 'code/confirm', objeto);

    const { token, user } = response.data;

    api.defaults.headers.Authorization = `Bearer ${token}`;

    yield put(signInSuccess(token, user));
    yield put(checkCodeSuccess(response.data));
    navigation.navigate('Home');
  } catch (error) {
    console.log('ERRO', error);
    yield put(checkCodeFailure(error));
  }
}

export default all([
  takeLatest('@sendcode/SEND_CODE_MOBILE_REQUEST', sendCode),
  takeLatest('@sendcode/SEND_EMAIL_REQUEST', sendEmail),
  takeLatest('@checkcode/CHECK_CODE_REQUEST', checkCodeMobile),
]);
