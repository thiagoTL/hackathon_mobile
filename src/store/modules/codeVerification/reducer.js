import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  checkCode: '',
  numero: '',
  message: '',
  modal: false,
};

export default function Evaluations(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@sendcode/SEND_CODE_MOBILE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@sendcode/SEND_CODE_MOBILE_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@sendcode/SEND_CODE_MOBILE_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@sendcode/SEND_EMAIL_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@sendcode/SEND_EMAIL_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@sendcode/SEND_EMAIL_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@checkcode/CHECK_CODE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@checkcode/CHECK_CODE_SUCCESS': {
        draft.loading = false;
        draft.modal = true;
        draft.message = action.payload.message;
        break;
      }

      case '@checkcode/CHECK_CODE_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@modal/MODAL_CLOSE': {
        draft.modal = false;
        break;
      }

      default:
        break;
    }
  });
}
