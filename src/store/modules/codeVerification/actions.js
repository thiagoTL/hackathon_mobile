export function sendCodeMobileRequest(mobile, navigation) {
  return {
    type: '@sendcode/SEND_CODE_MOBILE_REQUEST',
    payload: { mobile, navigation },
  };
}

export function sendCodeMobileSuccess() {
  return {
    type: '@sendcode/SEND_CODE_MOBILE_SUCCESS',
  };
}

export function sendCodeMobileFailure() {
  return {
    type: '@sendcode/SEND_CODE_MOBILE_FAILURE',
  };
}

export function sendEmailRequest(email) {
  return {
    type: '@sendcode/SEND_EMAIL_REQUEST',
    payload: { email },
  };
}

export function sendEmailSuccess() {
  return {
    type: '@sendcode/SEND_EMAIL_SUCCESS',
  };
}

export function sendEmailFailure() {
  return {
    type: '@sendcode/SEND_EMAIL_FAILURE',
  };
}

export function checkCodeRequest(phone, code, navigation) {
  return {
    type: '@checkcode/CHECK_CODE_REQUEST',
    payload: {
      phone,
      code,
      navigation,
    },
  };
}

export function checkCodeSuccess(data) {
  return {
    type: '@checkcode/CHECK_CODE_SUCCESS',
    payload: { message: data.message },
  };
}

export function checkCodeFailure() {
  return {
    type: '@checkcode/CHECK_CODE_FAILURE',
  };
}

export function modalClose() {
  return {
    type: '@modal/MODAL_CLOSE',
  };
}
