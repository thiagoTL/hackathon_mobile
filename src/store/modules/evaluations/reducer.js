import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  evaluations: [],
  modal: false,
};

export default function Evaluations(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@evaluations/EVALUATIONS_LIST_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@evaluations/EVALUATIONS_LIST_SUCCESS': {
        draft.evaluations = action.payload.data;
        draft.loading = false;
        break;
      }

      case '@evaluations/EVALUATIONS_LIST_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@evaluations/EVALUATIONS_STORE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@evaluations/EVALUATIONS_STORE_SUCCESS': {
        draft.loading = false;
        draft.modal = false;
        break;
      }

      case '@evaluations/EVALUATIONS_STORE_FAILURE': {
        draft.loading = false;
        break;
      }
      default:
        break;
    }
  });
}
