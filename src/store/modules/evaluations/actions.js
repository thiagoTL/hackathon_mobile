export function evaluationsListRequest() {
  return {
    type: '@evaluations/EVALUATIONS_LIST_REQUEST',
  };
}

export function evaluationsListSuccess(data) {
  return {
    type: '@evaluations/EVALUATIONS_LIST_SUCCESS',
    payload: { data },
  };
}

export function evaluationsListFailure() {
  return {
    type: '@evaluations/EVALUATIONS_LIST_FAILURE',
  };
}

export function evaluationsStoreRequest(content, stars, id) {
  return {
    type: '@evaluations/EVALUATIONS_STORE_REQUEST',
    payload: { content, stars, id },
  };
}

export function evaluationsStoreSuccess(data) {
  return {
    type: '@evaluations/EVALUATIONS_STORE_SUCCESS',
    payload: { data },
  };
}

export function evaluationsStoreFailure() {
  return {
    type: '@evaluations/EVALUATIONS_STORE_FAILURE',
  };
}
