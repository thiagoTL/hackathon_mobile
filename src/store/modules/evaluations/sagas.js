import { all, put, call, takeLatest } from 'redux-saga/effects';

import api from '../../../services/api';
import {
  evaluationsListFailure,
  evaluationsListSuccess,
  evaluationsStoreSuccess,
  evaluationsStoreFailure,
} from './actions';

export function* evaluations() {
  try {
    const response = yield call(api.get, 'recomms');

    yield put(evaluationsListSuccess(response.data.data));
  } catch (error) {
    console.log('ERROR', error);
    yield put(evaluationsListFailure(error));
  }
}

/** DEPOIS LEVANTAR ESSA QUESTÃO */
export function* evaluationsStore({ payload }) {
  try {
    const obj = {
      professional_id: payload.id,
      content: payload.content,
      stars: payload.stars,
    };

    // const id = payload.id;

    const response = yield call(api.post, 'recomms', obj);

    yield put(evaluationsStoreSuccess(response.data));
  } catch (error) {
    console.log('ERROR', error);
    yield put(evaluationsStoreFailure(error));
  }
}

export default all([
  takeLatest('@evaluations/EVALUATIONS_LIST_REQUEST', evaluations),
  takeLatest('@evaluations/EVALUATIONS_STORE_REQUEST', evaluationsStore),
]);
