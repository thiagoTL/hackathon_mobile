import produce from 'immer';

const INITIAL_STATE = {
  loading: false,
  checkCode: '',
  numero: '',
  message: '',
  modal: false,
};

export default function signUp(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case '@signUp/SIGN_UP_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@signUp/SIGN_UP_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@signUp/SIGN_UP_FAILURE': {
        draft.loading = false;
        break;
      }
      case '@signUp/CHECK_CODE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@signUp/CHECK_CODE_SUCCESS': {
        draft.loading = false;
        draft.message = action.payload.message;
        draft.modal = true;
        break;
      }

      case '@signUp/CHECK_CODE_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@resendCode/RESEND_CODE_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@resendCode/RESEND_CODE_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@resendCode/RESEND_CODE_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@resendCodeEmail/RESEND_CODE_EMAIL_REQUEST': {
        draft.loading = true;
        break;
      }

      case '@resendCodeEmail/RESEND_CODE_EMAIL_SUCCESS': {
        draft.loading = false;
        break;
      }

      case '@resendCodeEmail/RESEND_CODE_EMAIL_FAILURE': {
        draft.loading = false;
        break;
      }

      case '@modal/MODAL_CLOSE': {
        draft.modal = false;
        break;
      }

      default:
        break;
    }
  });
}
