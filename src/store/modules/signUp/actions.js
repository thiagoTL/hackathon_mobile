export function signUpRequest(
  userType,
  firstName,
  lastName,
  email,
  confirmEmail,
  phone,
  password,
  birthdate,
  name,
  codPhone,
  check,
  navigation,
) {
  return {
    type: '@signUp/SIGN_UP_REQUEST',
    payload: {
      userType,
      firstName,
      lastName,
      email,
      confirmEmail,
      phone,
      password,
      birthdate,
      name,
      codPhone,
      check,
      navigation,
    },
  };
}

export function signUpSuccess() {
  return {
    type: '@signUp/SIGN_UP_SUCCESS',
  };
}

export function signUpFailure() {
  return {
    type: '@signUp/SIGN_UP_FAILURE',
  };
}

export function checkCodeRequest(phone, code, navigation) {
  return {
    type: '@signUp/CHECK_CODE_REQUEST',
    payload: {
      phone,
      code,
      navigation,
    },
  };
}

export function checkCodeSuccess(data) {
  return {
    type: '@signUp/CHECK_CODE_SUCCESS',
    payload: { message: data.message },
  };
}

export function checkCodeFailure() {
  return {
    type: '@signUp/CHECK_CODE_FAILURE',
  };
}

export function resendCodeRequest(mobile, navigation) {
  return {
    type: '@resendCode/RESEND_CODE_REQUEST',
    payload: { mobile, navigation },
  };
}

export function resendCodeSuccess() {
  return {
    type: '@resendCode/RESEND_CODE_SUCCESS',
  };
}

export function resendCodeFailure(mobile) {
  return {
    type: '@resendCode/RESEND_CODE_FAILURE',
  };
}

export function resendCodeEmailRequest(email, navigation) {
  return {
    type: '@resendCodeEmail/RESEND_CODE_EMAIL_REQUEST',
    payload: { email, navigation },
  };
}

export function resendCodeEmailSuccess() {
  return {
    type: '@resendCodeEmail/RESEND_CODE_EMAIL_SUCCESS',
  };
}

export function resendCodeEmailFailure() {
  return {
    type: '@resendCodeEmail/RESEND_CODE_EMAIL_FAILURE',
  };
}

export function modalClose(modal) {
  return {
    type: '@modal/MODAL_CLOSE',
    payload: { modal },
  };
}
