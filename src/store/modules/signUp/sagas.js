import { all, takeLatest, call, put } from 'redux-saga/effects';
import { showMessage } from 'react-native-flash-message';

import { signUpSuccess, signUpFailure } from './actions';
import api from '../../../services/api';

export function* signUp({ payload }) {
  try {
    const {
      userType,
      firstName,
      lastName,
      email,
      confirmEmail,
      phone,
      password,
      birthdate,
      name,
      codPhone,
      check,
      navigation,
    } = payload;

    const numero = '+' + codPhone + phone;

    const object = {
      userType: userType,
      firstName: firstName,
      lastName: lastName,
      email: email,
      confirmedEmail: confirmEmail,
      mobile: numero,
      password: password,
      confirmedPassword: password,
      dob: birthdate,
      country: name,
      agreeTerms: check,
    };

    const response = yield call(api.post, 'register', object);

    yield put(signUpSuccess(response.data));

    navigation.navigate('CheckEmail', { numero });
  } catch (err) {
    console.log('ERROR => ', err);
    showMessage({
      type: 'danger',
      message: 'Email or phone has already been registered.',
    });
    yield put(signUpFailure(err));
  }
}

export function setToken({ payload }) {
  if (!payload) {
    return;
  }

  const { token } = payload.auth;

  if (token) {
    api.defaults.headers.Authorization = `Bearer ${token}`;
  }
}

export default all([
  takeLatest('persist/REHYDRATE', setToken),
  takeLatest('@signUp/SIGN_UP_REQUEST', signUp),
]);
