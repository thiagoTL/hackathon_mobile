import { all } from 'redux-saga/effects';

import authSaga from './auth/sagas';
import userSaga from './user/sagas';
import articleSaga from './articles/sagas';
import evaluationsSaga from './evaluations/sagas';
import chatroomSaga from './chatroom/sagas';
import chatmessageSaga from './chatmessage/sagas';
import signUpSaga from './signUp/sagas';
import codeVerificationSaga from './codeVerification/sagas';

export default function* rootSaga() {
  return yield all([
    authSaga,
    userSaga,
    articleSaga,
    evaluationsSaga,
    chatroomSaga,
    chatmessageSaga,
    signUpSaga,
    codeVerificationSaga,
  ]);
}
