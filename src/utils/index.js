export const validatePassword = (value) => {
  const regex = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]/;

  if (regex.test(value) === false) {
    return false;
  }

  return true;
};
