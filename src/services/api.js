import axios from 'axios';

const api = axios.create({
  baseURL: 'http://10.0.2.2:5000/',
});

// const api = axios.create({
//   baseURL: 'http://localhost:5000/',
// });

// const api = axios.create({
//   baseURL: 'https://app.investexpert.co/',
// });

export default api;
