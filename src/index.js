import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { useSelector } from 'react-redux';

import AuthenticationStack from './pages/Authentication/routes';
import InvestorStack from './pages/Investor/routesStack';
import AdvisorStack from './pages/Advisor';

const Stack = createStackNavigator();

export default function Index() {
  const token = useSelector((state) => state.auth.token);
  const typeUsers = useSelector((state) => state.auth.user);
  // const typeUsers = 'advisor';
  return (
    <Stack.Navigator>
      {token === null ? (
        <Stack.Screen
          name="Auth"
          component={AuthenticationStack}
          options={{ headerShown: false }}
        />
      ) : (
        <>
          {typeUsers.userType === 'advisor' ? (
            <Stack.Screen
              name="HomeAdvisor"
              component={AdvisorStack}
              options={{ headerShown: false }}
            />
          ) : (
            <Stack.Screen
              name="Home"
              component={InvestorStack}
              options={{ headerShown: false }}
            />
          )}
        </>
      )}
    </Stack.Navigator>
  );
}
