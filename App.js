import 'intl';
import 'intl/locale-data/jsonp/en';

import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import FlashMessage from 'react-native-flash-message';
import analytics from '@segment/analytics-react-native';

import Routes from './src';
import { store, persistor } from './src/store';
import { colors } from './src/constants/colors';

export default function App() {
  async function loadSegment() {
    await analytics.setup('thky5t3OOZ3qiDM9IPa4UDpuIsdaplgx', {
      // Record screen views automatically!
      recordScreenViews: true,
      // Record certain application events automatically!
      trackAppLifecycleEvents: true,
    });
  }

  useEffect(() => {
    loadSegment();
  }, []);

  return (
    <NavigationContainer>
      <Provider store={store}>
        <PersistGate persistor={persistor}>
          <Routes />
          <StatusBar backgroundColor={colors.black} barStyle="light-content" />
          <FlashMessage position="top" floating={true} />
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
}
